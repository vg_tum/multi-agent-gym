import numpy as np
from multi_agent_gym.envs.particles import FourRoomsParticles

nb_agents = 1
num_episodes = 10 # 10000 for issue 5 (
num_steps_per_episode = 64

env = FourRoomsParticles(nb_agents=nb_agents, sample_goal=True, sample_start=True)

for episode_no in range(num_episodes):
    print(episode_no)
    init_obs = env.reset()
    for i in range(num_steps_per_episode):
        action_n = 2 * np.random.random_sample((nb_agents, 2))- 1
        obs_n, reward_n, done, info_n = env.step(action_n)
    print(episode_no)

# extension for issue 6
nb_agents = 1
num_episodes = 10000
env = FourRoomsParticles(nb_agents=nb_agents, num_goals=nb_agents, sample_goal=True, sample_start=True)

for episode_no in range(num_episodes):
    print(episode_no)
    init_obs = env.reset()
    for i in range(num_steps_per_episode):
        action_n = 2 * np.random.random_sample((nb_agents, 2)) - 1
        obs_n, reward_n, done, info_n = env.step(action_n)
