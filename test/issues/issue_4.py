import numpy as np
from multi_agent_gym.envs.particles import FreeSpaceParticles
import time


nb_agents = 2
obs_dim = nb_agents * 6
start_states = np.array([[0.1, 0.1],[0.2, 0.2]])
goal_positions = np.array([[-0.1, -0.1], [-0.2, -0.2]])

env = FreeSpaceParticles(nb_agents=nb_agents,num_landmarks=nb_agents, goals=goal_positions, start_states=start_states, dt=0.1, sample_goal=False, sample_start=False)
_obs = env.reset()
env.render()

obs_n = np.zeros((nb_agents, obs_dim)) # I want to fill this array without world state
env._calc_agent_collisions() # this function should be called during initialization of environment to update env.agent_diff_pos_vectors

for n in range(nb_agents):
    init_obs, _ = env.get_agent_update(env.agents[n])
    obs_n[n] = init_obs
print("wrong initial obs_n \n",obs_n)

# This is what it should be like:
# it is just one step with zero force
action_n = np.zeros((nb_agents,2))
obs_n, _, _, _ = env.step(action_n)
cmp_obs = np.array(obs_n[0:nb_agents])
print("right initial obs_n \n", cmp_obs)
print("reset feedback == right init obs", cmp_obs == _obs)
print(_obs, _obs[0].shape)
print(cmp_obs)
time.sleep(10.0)
