import numpy as np
from multi_agent_gym.envs.particles import FreeSpaceParticles, FourRoomsParticles

nb_agents = 2
num_episodes = 10000
num_steps_per_episode = 100

# for illustration entity_collision_cost = 100

env = FreeSpaceParticles(nb_agents=nb_agents,
                         num_goals=nb_agents,
                         sample_goal=True,
                         sample_start=True,
                         entity_collision_cost=100.0,
                         agent_collision_cost=-42.0,
                         step_cost=0.0,
                         success_reward=0.0)

for episode_no in range(num_episodes):
    init_obs = env.reset()
    for i in range(num_steps_per_episode):
        action_n = np.ones((nb_agents, 2))
        obs_n, reward_n, done, info_n = env.step(action_n)
        print(info_n[0]['entity_collision'], info_n[1]['entity_collision'])
        env.render()
