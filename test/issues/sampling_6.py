import numpy as np
from multi_agent_gym.envs.particles import FourRoomsParticles as MahacFourRoomsParticles

env = MahacFourRoomsParticles(nb_agents=1,
                              num_goals=1,
                              sample_start=True,
                              sample_goal=True)
env.reset()
