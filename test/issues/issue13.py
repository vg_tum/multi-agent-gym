import numpy as np
from multi_agent_gym.envs.particles import FreeSpaceParticles

nb_agents = 1
num_episodes = 10000
num_steps_per_episode = 200

env = FreeSpaceParticles(nb_agents=nb_agents,
                         num_goals=nb_agents,
                         sample_goal=True,
                         sample_start=True,
                         entity_collision_cost=-5.0,
                         agent_collision_cost=0.0,
                         step_cost=-1.0,
                         success_reward=0.0)

sign = 1.0
for episode_no in range(num_episodes):
    init_obs = env.reset()
    for i in range(num_steps_per_episode):
        if i % 50 == 0:
            sign = sign * -1.0
        action_n = sign * np.array([[1.0, 1.0]])
        obs_n, reward_n, done, info_n = env.step(action_n)
        env.render()
        print("reward: ", reward_n[0])
        print("collision:", info_n[0]["entity_collision"])
