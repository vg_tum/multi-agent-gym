import argparse
import unittest
import sys
from pprint import pformat

sys.path.append('../')
from gym import error
from multi_agent_env_test import particles_test
from multi_agent_gym.utils import get_logger

lgr = get_logger('testing')


def get_args():
    parser = argparse.ArgumentParser(description="Multi Agent Gym Tester")
    g = parser.add_argument_group("General Test Parameters")

    # environment settings
    g.add_argument("-t", "--test", default=["all"],
                   help="tests to be run. Defaults to %(default)s", nargs="+",
                   dest="load_tests",
                   choices=["particles", "gym_checks", "robotics", "all"
                            ])
    g.add_argument("--render-robotics", action="store_true", dest="r_disp")
    g.add_argument("--render-particles", action="store_true", dest="p_disp")
    return parser.parse_args()


def load_tests_from(mod):
    lgr.info("\nTesting module {}\n{}\n".format(str(mod.__name__), "-" * 35))
    try:
        suite = unittest.TestLoader().loadTestsFromModule(mod)
    except:
        lgr.warn(" WARNING: failed to load {}".format(mod))
        return 1
    return unittest.TextTestRunner(verbosity=2).run(suite)


def run_tests(test_array):
    """Wraps standard test module to throw a system error for gitlab CI"""
    test_fail = False
    for o in list(filter(lambda x: x is not None, [load_tests_from(x) for x in test_array])):
        test_fail = o.errors or o.failures or test_fail
    return test_fail


if __name__ == "__main__":
    test = []
    test_args = get_args()

    lgr.info("Initiating Tests, set as:\n{}".format(pformat(vars(test_args))))

    # special awareness for mujoco dependent tests
    try:
        from multi_agent_env_test import gym_consistency_test, robotics_test

        if "gym_checks" in test_args.load_tests or "all" in test_args.load_tests:
            test.append(gym_consistency_test)
        if "robotics" in test_args.load_tests or "all" in test_args.load_tests:
            if test_args.r_disp:
                robotics_test.RENDER_FLAG = True
            test.append(robotics_test)
    except error.DependencyNotInstalled:
        lgr.error("Skipping multi-robot environment and gym consistency tests as mujoco is not installed")
    if "particles" in test_args.load_tests or "all" in test_args.load_tests:
        if test_args.p_disp:
            particles_test.RENDER_FLAG = True
        test.append(particles_test)

    if run_tests(test):
        lgr.error('Test failed')
        sys.exit(1)
    lgr.info("all Tests have been parsed successfully")
