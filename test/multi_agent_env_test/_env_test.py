import os
import sys
import numpy as np
from unittest import TestCase
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', '..'))
from typing import List
from multi_agent_gym.core import MultiAgentEnv


class EnvTest(TestCase):
    """
    Base  Test class

    Attributes:
        env([List[MultiAgentEnv]): list of environments to be tested
    """
    envs = []  # type: List[MultiAgentEnv]

    def fcn_env_spaces(self):
        for e in self.envs:
            self.assertIsNotNone(e)
            self.assertIsNotNone(e.observation_space)
            self.assertIsNotNone(e.action_space)
            self.assertIsNotNone(e.world_state_space)
            self.assertIsNotNone(e.world_state)
            self.assertIsInstance(e.reward_range, np.ndarray)
            self.assertTrue(e.reward_range.shape == (2,) or e.reward_range.shape == (e.nb_agents, 2))

    def fcn_centralized_checks(self):
        for e in self.envs:
            if e._decentral:
                e._decentral = False
                e.scalar_reward = True
                self.assertIsNotNone(e.observation_space)
                self.assertEqual(len(e.reward_range), 2)

    def fcn_agents_num(self):
        for e in self.envs:
            self.assertEqual(e.nb_agents, len(e.agents))
            self.assertIsInstance(e.nb_agents, int)
            self.assertGreater(e.nb_agents, 1)
            for ind, ag in enumerate(e.agents):
                self.assertEqual(int(ag), ind)
                self.assertEqual(hash(ag), hash(ind))
                self.assertIsInstance(ag._name, str)
                if ind > 0:
                    self.assertNotEqual(ag, e.agents[0])

    def fcn_agent_spaces(self):
        for e in self.envs:
            for a in e.agents:
                self.assertIsNotNone(a.action_space)
                self.assertIsNotNone(a.state_space)
                self.assertIsNotNone(a.obs_space)
                self.assertIsNotNone(a.state)

    def fcn_functions(self):
        for e in self.envs:
            obs = e.reset()
            self.assertIsNotNone(obs)
            self.assertIsNotNone(e.lgr)

    def stress_reset(self, env=None):
        if env is None:
            env = self.envs
        for _ in range(100):
            for e in env:
                self.assertIsNotNone(e.reset())
