import time
import os
import sys
import numpy as np


try:
    from multi_agent_gym.utils import error
except ModuleNotFoundError:
    sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', '..'))


try:
    from multi_agent_gym.envs.robotics.reaching.reach_goals import DyadicReach, TriadicReach
    from multi_agent_gym.envs.robotics.utils import simple_pos_ik, inverse_tf_mat

    FAKE_TEST = False
except Exception:
    import sys

    error("skipping tests with mujoco")
    FAKE_TEST = True
from ._env_test import EnvTest

RENDER_FLAG = False


def custom_reset(env, q_des):
    # type: (DyadicReach or DyadicReach, np.ndarray) -> bool
    """
    Allows to  reset system state by sampling for a finite time

    Args:
        env:
        q_des:
    """
    env.reset()
    s0 = env.sim.get_state()
    s0.qvel[:] = 0.0
    s0.qvel[:] = 0.0
    if len(q_des.shape) > 1:
        q_des = q_des[:env.nb_agents, :].copy()
    for a in env.agents:
        try:
            s0.qpos[a.joint_handles[a.controlled_joints]] = q_des[int(a)]
        except IndexError:
            s0.qpos[a.joint_handles[0]: a.joint_handles[0] + len(q_des[int(a)])] = q_des[int(a)]
        env.sim.set_state(s0)
        env.sim.forward()
        a.update(env.sim)
        q_cur = a.q[a.controlled_joints]
        if np.linalg.norm(q_cur - q_des[int(a)]) > 2e-2:
            print("Could not reach init value {}".format(q_des))
            return False
        env.sim.data.ctrl[a.joint_handles[a.controlled_joints]] = a.q[a.controlled_joints]
    return True


if not FAKE_TEST:
    class ReachTest(EnvTest):

        def setUp(self):
            self.envs = [
                DyadicReach(ctrl_joints=np.array([0, 1, 2, 3])),
                TriadicReach(ctrl_joints=np.array([0, 1, 2, 3]))
            ]
            if RENDER_FLAG:
                self._render = True
            else:
                self._render = False

        def test_env_spaces(self):
            super().fcn_env_spaces()

        def test_centralized(self):
            super().fcn_centralized_checks()

        def test_agents_num(self):
            super().fcn_agents_num()

        def test_agent_spaces(self):
            super().fcn_agent_spaces()

        def test_functions(self):
            super().fcn_functions()

        def test_issue5(self):
            # run stress test
            super().stress_reset()

        def test_object_collision(self):
            for e in self.envs:
                action = np.zeros(e.action_space.shape)
                custom_reset(e, action)
                object_collision = False
                for i in range(500):
                    action[2] -= 0.2
                    obs_n, reward_n, done, info_n = e.step(action)
                    self.assertFalse(info_n[0]["agent_collision"])
                    if info_n[0]["object_collision"]:
                        object_collision = True
                        break
                self.assertTrue(object_collision)

        def test_agent_collision(self):
            for e in self.envs[1:]:
                for act_agent in range(e.nb_agents):
                    agent_col_with = [set(), set(), set()]
                    if act_agent == 0:
                        self.assertTrue(custom_reset(e, np.array([[-0.5, 0.3, -1.2, 0.0],
                                                                  [-0.05, 0.0, -np.pi / 2.0, 0.0],
                                                                  [0.5, 0.0, -np.pi / 2.0, 0.0]])),
                                        "failed for {} agents at agent {}".format(2 + self.envs.index(e),
                                                                                  act_agent))
                    elif act_agent == 1:
                        self.assertTrue(custom_reset(e, np.array([[0.0, 0.3, -1.2, 0.0],
                                                                  [0.25, 0.0, -np.pi / 2.0, 0.0],
                                                                  [-0.5, 0.0, -np.pi / 2.0, 0.0]])),
                                        "failed for {} agents at agent {}".format(2 + self.envs.index(e),
                                                                                  act_agent))
                    elif act_agent == 2:
                        self.assertTrue(custom_reset(e, np.array([[-0.7, 0.0, -np.pi / 2.0, 0.0],
                                                                  [0.5, 0.0, -np.pi / 2.0, 0.0],
                                                                  [0.25, 0.0, -np.pi / 2.0, 0.0]])),
                                        "failed for {} agents at agent {}".format(2 + self.envs.index(e),
                                                                                  act_agent))
                    action = np.reshape(np.zeros(e.action_space.shape),
                                        (e.nb_agents, int(e.action_space.shape[0] / e.nb_agents)))
                    for i in range(500):
                        action[act_agent, 0] = -0.6 * np.sin(i / 50)
                        obs_n, reward_n, done, info_n = e.step(action)
                        for i in range(e.nb_agents):
                            if info_n[i]["agent_collision"]:
                                for c in filter(lambda x: x is not None, info_n[i]["coll_with_link"]):
                                    agent_col_with[i].add(c)
                        if self._render:
                            e.render()
                    for i in range(e.nb_agents):
                        self.assertTrue(any([x is not None for x in agent_col_with]))
                        self.assertFalse(any([e.agents[i]._name in x for x in agent_col_with[i]]))
                    if self._render:
                        e.close()

        def test_goals(self):
            check = [DyadicReach(sample_goal=False, ctrl_joints=None),
                     TriadicReach(sample_goal=False, ctrl_joints=None)]
            for e in check:
                e.sim.forward()
                idle_action = np.zeros(e.action_space.shape)
                self.assertEqual(len(e.goals), e._num_goals)
                for g_1, g_2 in zip(np.reshape(e.world_state, (len(e.goals), 3)),
                                    [e.sim.data.get_site_xpos(s) for s in e.sim.model.site_names]):
                    self.assertLess(np.linalg.norm(g_1 - g_2), 1e-7)
                e.reset()
                goal_state = e.sim.get_state()
                goal_state.qvel[:] = 0.0
                done = False
                while not done:
                    q_goal = np.zeros(e.action_space.shape)
                    for g in e.goals:
                        for a in e.agents:
                            if np.all(q_goal[int(a) * 6: (int(a) + 1) * 6] == 0.0):
                                tmp = np.eye(4)
                                tmp[0:3, 3] = g
                                legal, q = simple_pos_ik(a,
                                                         np.dot(a.world_base_tf[0:3, 0:3].T,
                                                                np.dot(tmp, inverse_tf_mat(a.world_base_tf))[0:3, 3]))
                                if legal:
                                    goal_state.qpos[int(a) * 6: int(a) * 6 + 6] = q
                                    e.sim.data.ctrl[a.joint_handles] = q
                                    a.q_prev = q
                                    break
                        e.sim.set_state(goal_state)
                        obs_n, reward_n, done, info_n = e.step(idle_action)
                        achieved = []
                        if self._render:
                            e.render()
                            time.sleep(0.2)
                            obs_n, reward_n, done, info_n = e.step(idle_action)
                            e.render()
                            time.sleep(1)
                        for a in range(e.nb_agents):
                            achieved = achieved + info_n[a]["achieved"]
                        self.assertGreater(len(achieved), 0)
                self.assertTrue(done)

        def test_a_joint_set_action(self):
            env = DyadicReach(ctrl_joints=None, incremental_action_space=False)
            for i in range(50):
                env.reset()
                q_tmp = np.array([a.q.copy() for a in env.agents]).ravel()
                could_reach = False
                action = env.action_space.sample()
                _ = env.step(action)
                for i in range(200):
                    env.sim.step()
                    env.sim.forward()
                    if np.linalg.norm(action - env.sim.get_state().qpos) < 2e-1:
                        could_reach = True
                        break
                _, _, _, info_n = env.step(action)
                try:
                    self.assertTrue(could_reach)
                except:
                    self.assertTrue(any([len(info_n[a]["coll_with_link"]) > 0 for a in range(env.nb_agents)]))

        def test_forward_kin(self):
            env = DyadicReach(ctrl_joints=None)
            env.reset()
            for i in range(50):
                action = env.action_space.sample()
                _ = env.step(action)
                if self._render:
                    env.render()
                    time.sleep(0.2)
                for a in env.agents:
                    T_tmp = np.dot(a.world_base_tf, a.forward_kin(a.q))
                    p = T_tmp[0:3, 3]
                    self.assertLessEqual(np.linalg.norm(a.x_pos - p), 1e-5)
