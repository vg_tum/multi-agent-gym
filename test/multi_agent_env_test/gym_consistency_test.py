import os
import sys
from unittest import TestCase
import gym
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', '..'))

from multi_agent_gym.utils import get_logger

lgr = get_logger('registration_tests')

class Registration(TestCase):

    def test_list(self):
        from gym import envs as g_envs
        from gym.error import DependencyNotInstalled
        try:
            from multi_agent_gym import envs as mg_envs
            gym_env_ids = [spec.id for spec in g_envs.registry.all()]
            multi_agent_gym_ids = [spec.id for spec in mg_envs.multi_agent_registry.all()]
            for g in gym_env_ids:
                self.assertIn(g, multi_agent_gym_ids)
            self.assertGreater(len(multi_agent_gym_ids), len(gym_env_ids))
            for g in filter(lambda x: x not in gym_env_ids, multi_agent_gym_ids):
                env = mg_envs.make(g)
                self.assertIsNotNone(env.reset())
        except (DependencyNotInstalled, ModuleNotFoundError):

            lgr.error("skipping tests with mujoco due to missing dependencies")
            return

