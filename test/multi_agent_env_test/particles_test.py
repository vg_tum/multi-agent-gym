import os
import sys
import gym
import numpy as np
from ._env_test import EnvTest

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', '..'))
from multi_agent_gym.envs.particles.scenarios.spread import ParticleSpread
from multi_agent_gym.utils import get_logger

lgr = get_logger("particles_tests")

RENDER_FLAG = False


def render(env: ParticleSpread or ParticleSpread):
    if not RENDER_FLAG:
        return
    try:
        import ctypes
    except ImportError:
        return
    try:
        env.render()
    except Exception:
        return


def steady_env_test(env):
    # type: (FreeParticlesTest) -> (np.ndarray, np.ndarray, float)
    """
    Includes testing against:
    - issue 1 --> reset return value broken
    - issue 4 --> initial observation broken

    Args:
        env:

    Returns:

    """
    obs_0 = env.reset()
    if isinstance(env.action_space, gym.spaces.Box):
        idle_action = np.zeros(env.action_space.shape)
    elif isinstance(env.action_space, gym.spaces.Dict):
        actions = []
        for ag in env.agents:
            if isinstance(env.action_space[ag._name], gym.spaces.Box):
                actions.append(np.zeros(env.action_space[ag._name].shape))
            elif isinstance(env.action_space[ag._name], gym.spaces.Discrete):
                actions.append(np.zeros(env.action_space[ag._name].n))
            else:
                raise NotImplementedError
        idle_action = np.array(actions)
    elif isinstance(env.action_space, gym.spaces.Discrete):
        idle_action = np.zeros(env.action_space.n)
    else:
        raise NotImplementedError
    obs_1, rew, _, _ = env.step(idle_action)
    return obs_0, obs_1, rew


def issue_7(env):
    # type: (ParticleSpread or ParticleSpread) -> bool
    """
    Testing environment against Issue 7 -> check if wall collision is encountered.

    Args:
        env:  environment to be tested

    Returns:
        bool: True if a wall collision is detected
    """
    from copy import copy
    env.reset()
    tmp = np.array([copy(x) for ag in env.agents for x in ag.p_pos])
    if np.any(np.abs(tmp) > 1.0):
        print(tmp)
    action_n = np.ones((env.nb_agents, 2))
    out = False
    for i in range(1000):
        obs_n, reward_n, done, info_n = env.step(action_n)
        if any([info_n[n]["entity_collision"] for n in range(env.nb_agents)]):
            out = True
            break
    return out


class FreeParticlesTest(EnvTest):

    def setUp(self):
        self.envs = []
        for nb_agents in range(2, 5):
            for num_goals in range(2, 5):
                self.envs.append(ParticleSpread(nb_agents, num_goals=num_goals,
                                                entity_collision_cost=100.0,
                                                agent_collision_cost=42.0,
                                                step_cost=0.0,
                                                success_reward=0.0))

    def issue9(self, env):
        for eps in range(10):
            sign = 1.0
            cnt = 0
            dummy_aciton = np.repeat([[1.0, 1.0]], env.nb_agents, axis=0)
            col_info_array = np.zeros(20, dtype=bool)
            rew_array = np.zeros(20)
            env.reset()
            for i in range(50):
                if i % 50 == 0:
                    cnt = 0
                    sign *= -1.0
                action_n = sign * dummy_aciton
                obs_n, reward_n, done, info_n = env.step(action_n)
                render(env)
                if 10 < cnt < 40:
                    col_info_array = np.zeros(20, dtype=bool)
                    rew_array = np.zeros(10)
                else:
                    rew_array = np.roll(rew_array, 1)
                    col_info_array = np.roll(col_info_array, 1)
                    try:
                        rew_array[0] = reward_n[0]
                    except KeyError:
                        rew_array[0] = reward_n[env.agents[0]._name]
                    col_info_array[0] = info_n[0]['entity_collision']
                cnt += 1
            self.assertFalse(np.all(rew_array == env._entity_collision_cost))
            self.assertFalse(np.all(col_info_array))

    def test_a_steady_step(self):
        for e in self.envs:
            e.sparse_rewards = True
            o0, o1, rew = steady_env_test(e)
            for x, y in zip(o0, o1):
                self.assertTrue(np.all(x == y))
            for r_l in rew:
                if isinstance(rew, dict):
                    r = rew[r_l]
                else:
                    r = r_l
                self.assertEqual(r, -e._step_cost)
            e.sparse_rewards = False
            o0, o1, rew = steady_env_test(e)
            for r_l in rew:
                if isinstance(rew, dict):
                    r = rew[r_l]
                else:
                    r = r_l
                self.assertLess(r, 0.0)
                self.assertNotEqual(r, -e._entity_collision_cost)
                self.assertNotEqual(r, -e._agent_collision_cost)
                self.assertNotEqual(r, -e._step_cost)

    def test_env_spaces(self):
        super().fcn_env_spaces()

    def test_centralized(self):
        super().fcn_centralized_checks()

    def test_agents_num(self):
        super().fcn_agents_num()

    def test_agent_spaces(self):
        super().fcn_agent_spaces()

    def test_functions(self):
        super().fcn_functions()

    def test_issue5(self):
        # run stress test
        super().stress_reset()

    def test_issue6(self):
        # tests single agent sampling
        super().stress_reset([ParticleSpread(nb_agents=1, num_goals=g,) for g in range(1, 4)])

    def test_issue7(self):
        for e in self.envs:
            e.reset()
            for ag in e.agents:
                self.assertTrue(e.legal_pos(ag))
            self.assertTrue(issue_7(e))
            if e._illegal_pos(ag):
                lgr.info("check this")
            self.assertFalse(e._illegal_pos(ag))

    def test_issue9(self):
        e = ParticleSpread(nb_agents=1,
                           num_goals=1,
                           entity_collision_cost=5.0,
                           agent_collision_cost=0.0,
                           step_cost=1.0,
                           success_reward=0.0)
        self.issue9(e)

    def test_issue9_extended(self):
        for e in self.envs:
            self.issue9(e)


class FourRoomTest(EnvTest):

    def setUp(self):
        self.envs = []
        for nb_agents in range(2, 4):
            for num_goals in range(2, 4):
                self.envs.append(ParticleSpread(nb_agents, num_goals=num_goals,
                                                entity_collision_cost=100.0,
                                                agent_collision_cost=42.0,
                                                step_cost=1.0,
                                                success_reward=0.0))

    def test_env_spaces(self):
        super().fcn_env_spaces()

    def test_agents_num(self):
        super().fcn_agents_num()

    def test_centralized(self):
        super().fcn_centralized_checks()

    def test_agent_spaces(self):
        super().fcn_agent_spaces()

    def test_functions(self):
        super().fcn_functions()

    def test_issue5(self):
        # run stress test
        super().stress_reset()

    def test_issue6(self):
        # tests single agent sampling
        super().stress_reset([ParticleSpread(nb_agents=1, num_goals=g) for g in range(1, 4)])

    def test_issue7(self):
        [self.assertTrue(issue_7(e)) for e in self.envs]
