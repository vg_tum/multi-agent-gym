# Multi Agent Gym Contribution Guidelines

## Getting started  
No matter which option you chose, please take a look at the [Git Best Practices](http://sethrobertson.github.io/GitBestPractices/) document. 

## Reporting problems
  * open an issue raising your feature request / concern in the isse setting [https://git.lsr.ei.tum.de/vg/all_access/multi_agent_gym/issues]
  * In case you actively want to add these features / work on these bugs, please refer to the making changes etc section. A git commit message which contains the word closes #123 will automatically close your issue 123.

## Making changes
  * Create a topic branch for your work. You should branch off the master branch. Name your branch by the type of contribution, source branch, and nature of the contribution, e.g., bug/master/my_contribution.
  * Generally, the type is bug, or feature, but you can use something else if they don't fit. 
  * To create a topic branch based on master: ```git checkout master && git pull && git checkout -b bug/master/my_contribution```
  * Don't work directly on the master branch, or any other core branch. 
  * Keep your commits distinct. A commit should do one thing, and only one thing.
  * Make sure your commit messages are in the proper format.
  * If your commit fixes an issue, close it with your commit message (by appending, e.g., fixes #1234, to the summary).
  * Merge changes on the master changes with your branch as often as possible.
  
## Submitting Changes
  * Push your changes to a topic branch in your fork of the repository.
  * Never try pushing to the master or core branches on the server before local merges are successful!
  * Track any (as minor as it may seem) problem in the issue tracking section.

# Commit Message Format
What should be included in a commit message? The three basic things to include are:
  * Summary or title.
  * Detailed description
  * Issue number (optional).

Here is a sample commit message with all that information:

```
Adds UTF-8 encoding to POM properties

Some POM's did not have the source encoding specified. This
caused unnecessary warning printouts during build. This commit
ensures that all POM's includes the correct declaration for
UTF-8.

Closes #1234
```

The summary should be kept short, 50 characters or less, and the lines in the detailed message should not exceed 72 characters. These limits are recommended to get the best output possible from the git log command and also to be able to view the commits in a terminal window with 80 character limit.
The issue number is optional and should only be included when the commit really closes an issue. The close will then occur when the pull request is merged.
