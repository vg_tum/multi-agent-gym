# Multi Agent Gym

A short extension of [OpenAi gym](https://github.com/openai/gym).

This package is utter alpha and requires further implementation and testing(!):

This package contains the following environments:

1. [Multi Agent Particles](./multi_agent_gym/envs/particles_wrapper)
2. [Robotics Environment](./multi_agent_gym/envs/robotics)

## Installation

Please follow / extend the description in the [installation instructions](./doc/INSTALL.rst)

## Documentation

Use [Sphinx](http://www.sphinx-doc.org/en/master/) by following the installation instructions and then running 

```sh
make html
```

such that you can access the build dodumentation at [./doc/build/html](./doc/build/html). This link will of course be dead in the actual Documentation.
For bugs / problems and comments, please check the [contribution-guideline](./CONTRIBUTING.md)

## Note

The robotics environment are not usable as provided due to the missing STL / CAD files.
Furthermore, the robot dynamics have been alternated as these information were provided by COMAU as part of a joint project.
Please contact the author in case you want to explicitly use this code base, or contact COMAU directly for the missing information.