References and Resources
---------------------------

.. [Loewe2017] Multi-Agent Actor-Critic for Mixed Cooperative-Competitive Environments (`pdf <https://arxiv.org/pdf/1706.02275.pdf>`__, `src <https://github.com/openai/maddpg>`__).

.. [Sutton1998] Sutton, R. S., & Sutton, R. S. (1998). Between MDPs and Semi-MDPs : Learning , Planning , and Representing Knowledge at Multiple Temporal Scales at Multiple Temporal Scales. 

.. [OpenAI2016] Brockman, G., Cheung, V., Pettersson, L., Schneider, J., Schulman, J., Tang, J., & Zaremba, W. (2016). OpenAI Gym. (`src <https://github.com/openai/gym>`__)

.. [Levy2018] Levy, A., Platt, R., & Saenko, K. (2018). Hierarchical Actor-Critic (`src <https://github.com/andrew-j-levy/Hierarchical-Actor-Critc-HAC->`__).


