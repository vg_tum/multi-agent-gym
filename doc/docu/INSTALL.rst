----------------
Basics and Setup
----------------



Installation Instruction
========================

In order to install all required packages, please follow the instructions below. Conda is recomended but pip can be used as well.

Sphinx / Docu installation
--------------------------

please run 

.. code-block:: sh

   pip3 install Sphinx==1.7.2 \
               sphinx-rtd-theme==0.3.0\
               sphinxcontrib-napoleon==0.6.1 \
               sphinxcontrib-websupport==1.0.1 \
               m2r


Tested on:

* Ubuntu 18.04 LTS


Common Problems and Solutions
===============================

Mujoco Problems 
-----------------

When running mujocp_py. the OpenGL library throws errors.

   ERROR: GLEW initalization error: Missing GL version

The solution is to add the libaray path to your python executable to be loaded upon runtime, i.e. the location of libGLEW.so and libGL.so.
In order to solve this issue, search the location of the libraries by calling 

.. code-block:: sh

   locate libGL.so 
      /opt/MatlabR2018a/sys/opengl/lib/glnxa64/libGL.so.1
      /opt/MatlabR2018a/sys/opengl/lib/glnxa64/libGL.so.1.6.0
      /usr/lib/i386-linux-gnu/libGL.so.1
      /usr/lib/i386-linux-gnu/libGL.so.1.0.0
      /usr/lib/i386-linux-gnu/libGL.so.1.distrib
      /usr/lib/x86_64-linux-gnu/libGL.so
      /usr/lib/x86_64-linux-gnu/libGL.so.1
      /usr/lib/x86_64-linux-gnu/libGL.so.1.0.0
      /usr/lib/x86_64-linux-gnu/libGL.so.1.distrib
      /usr/lib/x86_64-linux-gnu/libGL.so.distrib

The same holds for libGLEW.so accordingly. Then adjust the python call by adding the LD_PRELOAD command as shown below

.. code-block:: sh
   
   cd examples
   LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libGLEW.so:/usr/lib/x86_64-linux-gnu/libGL.so python mujoco_sim.py

For a permanent solution you can add a alias call to your zshrc / bashrc:

.. code-block:: sh
   
   echo "alias m_python='LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libGLEW.so:/usr/lib/x86_64-linux-gnu/libGL.so python'" | tee -a ~/.zshrc
   echo "alias m_jupyter='LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libGLEW.so:/usr/lib/x86_64-linux-gnu/libGL.so jupyter'" | tee -a ~/.zshrc

In order to further use these variables in pycharm without the need of starting pycharm from terminal every time, you can change the 
environment variables for each run individually as shown below

.. image:: mujoco_settings_pycharm.png
   :width: 780pt

Sources:
 * https://github.com/openai/mujoco-py/issues/44
 * https://github.com/openai/mujoco-py/pull/145

Code Test Coverage 
==================

After running 

.. code-block:: bash

    py.test --cov=multi_agent_gym --cov-report=html:doc/build/html/coverage

you can get an overview of the current test-coverage in `here <../coverage/index.html>`_.
