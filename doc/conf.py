# -*- coding: utf-8 -*-
import os
import sys

import sphinx_rtd_theme

sys.path.insert(0, os.path.abspath('../'))

from multi_agent_gym_info import (
    PACKAGE_NAME, PACKAGE_TITLE,
    COPYRIGHT,
    AUTHOR, RELEASE, VERSION as __version__
)

# -- Project information -----------------------------------------------------
project = PACKAGE_NAME
copyright = COPYRIGHT
author = AUTHOR
version = __version__
release = RELEASE

# -- General configuration ---------------------------------------------------
on_rtd = os.environ.get('READTHEDOCS', None) == 'True'
# When on read the docs stub out the modules that cannot be loaded
if on_rtd:
    try:
        from unittest.mock import MagicMock
    except ImportError:
        from mock import MagicMock


    class Mock(MagicMock):
        @classmethod
        def __getattr__(cls, name):
            return Mock()


    MOCK_MODULES = ['numpy', 'scipy', 'gym', 'pyglet', 'six', 'typing']
    sys.modules.update((mod_name, Mock()) for mod_name in MOCK_MODULES)
# If extensions (or modules to document with autodoc) are in another directory,
# If extensions (or modules to document with autodoc) are in another directory,
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.doctest',
    'sphinx.ext.todo',
    'sphinx.ext.coverage',
    'sphinx.ext.mathjax',
    'sphinxcontrib.napoleon',
    'sphinx.ext.mathjax',
    'sphinx.ext.ifconfig',
    'sphinx.ext.viewcode',
    'nbsphinx',
]
suppress_warnings = [
    'nbsphinx',
]
nbsphinx_custom_formats = {
    '.pct.py': ['jupytext.reads', {'fmt': 'py:percent'}],
    '.md': ['jupytext.reads', {'fmt': 'Rmd'}],
}
mathjax3_config = {
    'tex': {'tags': 'ams', 'useLabelIds': True},
}

## nb sphinx setup
nbsphinx_execute_arguments = [
    "--InlineBackend.figure_formats={'svg', 'pdf'}",
    "--InlineBackend.rc=figure.dpi=96",
]
autodoc_default_flags = ['members', 'undoc-members', 'private-members', 'special-members',
                         'undoc-members', 'inherited-members', 'show-inheritance']

# napoleon settings
napoleon_include_init_with_doc = True
napoleon_use_ivar = True
napoleon_use_param = False
napoleon_use_rtype = False
source_suffix = {'.rst': 'restructuredtext'}
master_doc = 'index'
language = None
exclude_patterns = []
pygments_style = 'sphinx'
add_module_names = False
show_authors = True

# -- Options for HTML output -------------------------------------------------
html_domain_indices = False
html_use_index = True
html_split_index = True
html_show_sourcelink = True
html_theme = 'sphinx_rtd_theme'
html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]
# html_static_path = ['.static']
htmlhelp_basename = '{}help'.format(PACKAGE_NAME)
html_theme_options = {
    'logo_only': False,
    'display_version': True,
    'prev_next_buttons_location': 'bottom',
    'style_external_links': True,
    'style_nav_header_background': '#0565BD',
    # Toc options
    'collapse_navigation': True,
    'sticky_navigation': True,
    'navigation_depth': 4,
    'includehidden': True,
    'titles_only': False
}

# -- Options for LaTeX output ------------------------------------------------
latex_elements = {
    'papersize': 'a4paper',
    'pointsize': '10pt',
    # 'preamble': '',
    # 'figure_align': 'htbp',
}
latex_documents = [
    (master_doc, '{}.tex'.format(PACKAGE_NAME), '{} Documentation'.format(PACKAGE_NAME),
     AUTHOR, 'manual'),
]
# -- Options for manual page output ------------------------------------------
man_pages = [
    (master_doc, PACKAGE_NAME, '{} Documentation'.format(PACKAGE_NAME),
     [AUTHOR], 1)
]
# -- Options for Texinfo output ----------------------------------------------
texinfo_documents = [
    (master_doc, PACKAGE_NAME, '{} Documentation'.format(PACKAGE_NAME),
     AUTHOR, PACKAGE_NAME, PACKAGE_TITLE,
     'Miscellaneous'),
]
todo_include_todos = True
