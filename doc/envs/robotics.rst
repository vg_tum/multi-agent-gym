Multi Agent Robot Manipulation
==============================

Registered Environments
------------------------

.. automodule:: multi_agent_gym.envs.robotics.reaching.reach_goals
    :members:
    :show-inheritance:


Basic Robotic Modules
---------------------

Due to various different agents in use, the agents have been shifted to a separate description

.. automodule:: multi_agent_gym.envs.robotics
    :members:
    :show-inheritance:

.. automodule:: multi_agent_gym.envs.robotics.robot_env
    :members:
    :private-members:
    :show-inheritance:


Joint Reaching
^^^^^^^^^^^^^^^

.. automodule:: multi_agent_gym.envs.robotics.reaching.reach_env
    :members:
    :show-inheritance:

Mujoco API helpers
---------------------------

.. automodule:: multi_agent_gym.envs.robotics.utils
    :members:
    :show-inheritance:


