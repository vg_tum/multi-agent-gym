Multi Agent Particles 
=========================

.. automodule:: multi_agent_gym.envs.particles_wrapper
    :members:
    :show-inheritance:

Core Modules
------------------

.. automodule:: multi_agent_gym.envs.particles_wrapper.entities
    :members:
    :show-inheritance:


.. automodule:: multi_agent_gym.envs.particles_wrapper.agents
    :members:
    :show-inheritance:


.. automodule:: multi_agent_gym.envs.particles_wrapper.render
    :members:
    :show-inheritance:


.. automodule:: multi_agent_gym.envs.particles_wrapper.multi_agent_particle_env
    :members:
    :show-inheritance:

Registered Environments
-------------------------


.. automodule:: multi_agent_gym.envs.particles_wrapper.environments
    :members:
    :show-inheritance:


