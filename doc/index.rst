===============================================================
Multi Agent Gym -> An extension for multi robot systems
===============================================================
This package contains an extension of the well established https://github.com/openai/gym.


Available environments
=======================

:mod:`~particles`
    Particle Environment as presented by [Loewe2017_] and [Iqbal2018_]
:mod:`~robotcs`
    Extension of the robotic OpenAi gym scenarios to joint robotic manipulation tasks


Welcome to Multi-Agent Gym's documentation!
=============================================


.. toctree::
   :maxdepth: 3
   :caption: Overview

   README
   docu/INSTALL
   multi_agent_gym
   envs/particles
   envs/robotics
   envs/robotic_agents
   docu/refs

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
