from setuptools import setup, find_packages

from multi_agent_gym_info import AUTHOR, REPO, EMAIL, LICENSE, PACKAGE_NAME, VERSION


setup_extras = dict(racecars=['pybullet'],
                    robotics=['mujoco_py>=1.50, <2.1', 'imageio', 'numpy-quaternion', 'sympy'
                                                                                      'bidict'])

# Meta dependency groups.
all_deps = []
for group_name in setup_extras:
    all_deps += setup_extras[group_name]
setup_extras['all'] = all_deps
print(setup_extras)

setup(
    name=PACKAGE_NAME,
    version=VERSION,
    url=REPO,
    author=AUTHOR,
    author_email=EMAIL,
    license=LICENSE,
    entry_points={"console_scripts": {
        "mag_list_environments = multi_agent_gym.commands:list_environments",
        "mag_generate_random_data = multi_agent_gym.commands:run_random_policy_multi_agent_env",
        "mag_env_to_cfg = multi_agent_gym.commands:env_id_to_cfg",
        "mag_print_episode_data = multi_agent_gym.commands:load_and_extract_agent_data"
    }},
    packages=[package for package in find_packages() if package.startswith(PACKAGE_NAME)],
    python_requires='>=3.8',
    classifiers=[
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
    ],
    package_data={'multi_agent_gym': [
        'envs/robotics/assets/*/*.xml',
        'envs/robotics/assets/stls/*.stl',
        'envs/robotics/assets/stls/*.STL',
        'envs/robotics/assets/textures/*.png']
    },
    zip_safe=False,
    tests_require=['pytest', 'mock'],
    extras_require=setup_extras,
    install_requires=['scipy', 'numpy',
                      'six', 'gym',
                      'seaborn', 'pygame',
                      'coloredlogs', 'typing'],
)
