from gym.wrappers import TimeLimit


class TimeLimitMultiAgent(TimeLimit):
    """
    Basic TimeLimit Wrapper, but will not return a done flag once the episode time limit is exceeded,
    which is likely to happen in multi-agent environments, even though the final state may be
    close to the actual episode end / success.
    Punishments are instead thought to be obtained via collected (negative) rewards instead
    """
    def __init__(self, env, max_episode_steps=None):
        super(TimeLimitMultiAgent, self).__init__(env, max_episode_steps)

    def step(self, action):
        assert self._elapsed_steps is not None, "Cannot call env.step() before calling reset()"
        observation, reward, done, info = self.env.step(action)
        self._elapsed_steps += 1
        if self._elapsed_steps >= self._max_episode_steps:
            info['TimeLimit.truncated'] = not done
        return observation, reward, done, info

    @property
    def agent_dict(self):
        return self.env.agent_dict

    @property
    def agent_list(self):
        return self.env.agent_list

    @property
    def timeout(self) -> bool:
        return self._elapsed_steps >= self._max_episode_steps
