from collections import OrderedDict
import gym.spaces
import typing
import numpy as np

__all__ = ["AgentObservation", "JointObservationSpace"]


class AgentObservation(gym.spaces.Dict):

    def __init__(self, state_space: gym.spaces.Space, env_observation_space: gym.spaces.Space,
                 interaction_observation_space: typing.Optional[gym.spaces.Dict] = None):
        if interaction_observation_space is None:
            super(AgentObservation, self).__init__(
                x=state_space,
                y_env=env_observation_space,
            )
        else:
            assert isinstance(interaction_observation_space, gym.spaces.Dict), \
                "agent observation is expected to be given as dictionaries"
            super(AgentObservation, self).__init__(
                x=state_space,
                y_env=env_observation_space,
                y_ag=interaction_observation_space
            )

    def get_state_space(self) -> gym.spaces.Space:
        return self.spaces['x']

    def get_env_observation_space(self) -> gym.spaces.Space:
        return self.spaces['y_env']

    def get_interation_observation_space(self) -> dict:
        try:
            return self.spaces['y_ag'].spaces
        except KeyError:
            return gym.spaces.Dict().spaces

    @property
    def flat_shape(self):
        fl = self.flatten(self.sample())
        if fl.dtype == object:
            return [sp.shape for sp in fl]
        return [fl.shape, ]

    def __str__(self):
        try:
            return super().__str__() + f" => flat: {' x '.join([str(x[-1]) for x in self.flat_shape])}"
        except TypeError:
            return super().__str__() + f" => flat: {self.flat_shape[-1]}"

    @staticmethod
    def flatten(state: OrderedDict) -> np.array:
        """
        Flatten observation space to be

        Args:
            state(OrderedDict): state within the observation space

        Note:
            state is assumed to return True ```self.contains(state)```, the check is omitted for brevity

        Returns:
            np.ndarray: either single flat array or object-array for e.g. integer and float array of (possibly) different size
        """
        def parse_space(sp):
            if isinstance(sp, np.ndarray):
                try:
                    arrays[sp.dtype].append(sp)
                except KeyError:
                    arrays[sp.dtype] = [sp]
            elif isinstance(sp, dict):
                for v in sp.values():
                    parse_space(v)
            else:
                raise NotImplementedError(f'unknown type {type(sp)}')
        # stack objects of same type
        arrays = OrderedDict()
        parse_space(state['x'])
        try:
            parse_space(state['y_ag'])
        except KeyError:
            pass
        parse_space(state['y_env'])
        return np.hstack([np.hstack(v) for v in arrays.values()]).flatten()


class JointObservationSpace(gym.spaces.Dict):

    def __init__(self,
                 agent_names: typing.Optional[typing.List[str]] = None,
                 agent_spaces: typing.Optional[typing.List[AgentObservation]] = None,
                 space_dict: typing.Optional[typing.Dict[str, AgentObservation]] = None):
        if agent_spaces is not None and agent_names is not None:
            assert len(agent_names) == len(
                agent_spaces), f"number of agents {len(agent_names)} different from agent spaces {len(agent_spaces)}"
            super(JointObservationSpace, self).__init__({ag: space for ag, space in zip(agent_names, agent_spaces)})
        else:
            super(JointObservationSpace, self).__init__(**space_dict)


def space_test(sp: AgentObservation):
    print(sp)
    print(sp.sample())
    assert sp.contains(sp.sample())
    print(f'flatten: space1: {AgentObservation.flatten(sp.sample())}')


if __name__ == "__main__":
    x = gym.spaces.Box(-np.ones(2), np.zeros(2), dtype=np.double)
    y_e = gym.spaces.Box(-np.ones(2), np.ones(2), dtype=np.double)
    y_ag = gym.spaces.Dict({'other_guy': x})
    space_test(AgentObservation(x, y_e))
    space_test(AgentObservation(x, y_e, y_ag))
