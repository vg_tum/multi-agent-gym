from gym.spaces import Space, Box
import torch
import numpy as np

__all__ = ["TorchBox", "tn_box"]


class TorchBox(Space):
    """
    A (possibly unbounded) batched box in R^n. Specifically, a TorchBox represents the
    Cartesian product of n closed intervals. Each interval has the form of one
    of [a, b], (-oo, b], [a, oo), or (-oo, oo).

    There are two common use cases:

    """

    def __init__(self, shape, low, high, dtype=torch.float32):
        def limit_helper(lim):
            assert lim.shape == self.shape or lim.shape == self.shape[1:], "dimension mismatch for limer bound"
            if self.shape[1:] == lim.shape:
                return torch.stack([lim] * self.shape[0])
            else:
                return lim

        assert dtype is not None, 'dtype must be explicitly provided. '
        self.shape = tuple(shape)
        assert len(shape) > 1, "Torch Box is intended for batched environment, i.e. non-vector spaces"
        super(TorchBox, self).__init__(self.shape)
        self.dtype = dtype
        tmp = dict(size=self.shape, dtype=torch.bool)
        if np.isscalar(low):
            self.low = low
            self.bounded_below = torch.ones(**tmp) if np.isfinite(low) else torch.zeros(**tmp)
        else:
            self.low = limit_helper(low)
            self.bounded_below = torch.isfinite(self.low)
        if np.isscalar(high):
            self.high = high
            self.bounded_above = torch.ones(**tmp) if np.isfinite(high) else torch.zeros(**tmp)
        else:
            self.high = limit_helper(high)
            self.bounded_above = torch.isfinite(self.high)

    def is_bounded(self, manner="both"):
        below = torch.all(self.bounded_below)
        above = torch.all(self.bounded_above)
        if manner == "both":
            return below and above
        elif manner == "below":
            return below
        elif manner == "above":
            return above
        else:
            raise ValueError("manner is not in {'below', 'above', 'both'}")

    def sample(self):
        """
        Generates a single random sample inside of the Box.

        In creating a sample of the box, each coordinate is sampled according to
        the form of the interval:

        * [a, b] : uniform distribution
        * [a, oo) : shifted exponential distribution
        * (-oo, b] : shifted negative exponential distribution
        * (-oo, oo) : normal distribution
        """
        sample = torch.empty(self.shape, dtype=self.dtype)

        # Masking arrays which classify the coordinates according to interval
        # type
        unbounded = ~self.bounded_below & ~self.bounded_above
        upp_bounded = ~self.bounded_below & self.bounded_above
        low_bounded = self.bounded_below & ~self.bounded_above
        bounded = self.bounded_below & self.bounded_above

        # Vectorized sampling by interval type
        sample[bounded] = (self.low[bounded] - self.high[bounded]) * torch.rand(bounded[bounded].shape) + \
                          self.high[bounded]
        sample[unbounded] = torch.normal(torch.zeros(unbounded[unbounded].shape),
                                         torch.ones(unbounded[unbounded].shape))
        # todo: shifted exponential distributions are missing
        return sample

    def contains(self, x):
        if isinstance(x, list):
            x = torch.Tensor(x, )  # Promote list to array for contains check
        return x.shape == self.shape and torch.all(x >= self.low).item() and torch.all(x <= self.high).item()

    def to_jsonable(self, sample_n):
        return torch.Tensor(sample_n).tolist()

    def from_jsonable(self, sample_n):
        return [torch.Tensor(sample) for sample in sample_n]

    def __repr__(self):
        return "TorchBox" + str(self.shape)

    def __eq__(self, other):
        if isinstance(other, TorchBox) and (self.shape == other.shape):
            try:
                return torch.allclose(self.low, other.low) and torch.allclose(self.high, other.high)
            except TypeError:
                return self.low == other.low and self.high == other.high
        return False


def tn_box(low, high):
    if len(low.shape) > 1:
        return TorchBox(low.shape, low, high, low.dtype)
    return Box(low, high)


if __name__ == "__main__":
    x = TorchBox((10, 2), low=-1.0, high=1.0)
    x.sample()
    print(x == TorchBox((10, 2), low=-1.0, high=1.0))
    print(x == TorchBox((11, 2), low=-1.0, high=1.0))
    print(x.contains(torch.zeros((10, 2))))
    print(x.contains(2.0 * torch.ones((10, 2))))
