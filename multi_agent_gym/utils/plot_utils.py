import numpy as np
import matplotlib.pylab as plt
import seaborn as sns
from .stream_io import in_ipynb

__all__ = ["get_axis", "set_2Dplot_properties", "set_ipy_plot", "set2D_axis",
           "get_colors", "get_rgba_colors"]


def shared_plot_rc_settings():
    return {
        "xtick.minor.visible": True,
        "ytick.minor.visible": True,
        "legend.framealpha": 0.5,
    }


def set_ipy_plot(ncols=1, nrows=1):
    """
    Set Ipython plot configt

    Args:
        ncols:
        nrows:
    """
    rc_params = shared_plot_rc_settings()
    rc_params["figure.figsize"] = [15 * ncols, 6 * nrows]
    rc_params["legend.markerscale"] = 2.0
    rc_params["font.size"] = 14.0
    rc_params["axes.labelsize"] = 10
    rc_params["axes.titlesize"] = 10
    rc_params["figure.titlesize"] = 24.0

    try:
        import seaborn as sns

        sns.set_theme(
            context="notebook",
            style="darkgrid",
            font="sans-serif",
            font_scale=2.0,
            color_codes=True,
            rc=rc_params,
        )
    except (ImportError, ModuleNotFoundError):
        print("could not set seaborn plot styles. Consider installing")
    plt.rc("legend", markerscale=1.0, fontsize=22.0)


def set_src_plot(ncols=1, nrows=1):
    """
    Set Ipython plot configt

    Args:
        ncols(int): number of columns
        nrows(int): number of rows

    """
    rc_params = shared_plot_rc_settings()
    rc_params["figure.figsize"] = [18 * ncols, 6 * nrows]
    rc_params["font.size"] = 12.0
    rc_params["axes.labelsize"] = 10
    rc_params["axes.titlesize"] = 10
    rc_params["figure.titlesize"] = 24.0
    rc_params["legend.markerscale"] = 1.0
    rc_params["legend.fontsize"] = 8.0
    try:
        import seaborn as sns

        sns.set_theme(
            context="paper",
            style="darkgrid",
            font="sans-serif",
            color_codes=True,
            rc=rc_params,
        )
    except (ImportError, ModuleNotFoundError):
        print("could not set seaborn plot styles. Consider installing")


def set_2Dplot_properties(ncols=1, nrows=1):
    if in_ipynb():
        set_ipy_plot(ncols, nrows)
    else:
        set_src_plot(ncols, nrows)


def get_axis(title, ncols=1, nrows=1, ax_handle=None):
    if ax_handle is None:
        set_2Dplot_properties()
        fig, ax = plt.subplots(ncols=ncols, nrows=nrows)
        fig.suptitle(title)
        plt.xticks()
        plt.yticks()
    else:
        ax = ax_handle
    return ax


def set2D_axis(ax, x_label=r"$x$", y_label=r"$y$",
               x_lim=(-1.0, 1.0), y_lim=(-1., 1.),
               show_grid=False,
               show_ticks=True
               ):
    ax.set_xlim(x_lim)
    ax.set_ylim(y_lim)
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label, rotation="horizontal")
    ax.set_aspect('equal', 'box')
    ax.grid(show_grid)
    if not show_ticks:
        ax.set_xticks([])
        ax.set_yticks([])
    else:
        plt.xticks()
        plt.yticks()


def get_colors(nb_cols):
    return np.array(sns.color_palette(n_colors=nb_cols))


def get_rgba_colors(nb_cols):
    return np.hstack((get_colors(nb_cols), np.ones((nb_cols, 1))))