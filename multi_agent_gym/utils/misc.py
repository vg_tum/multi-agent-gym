from .stream_io import in_ipynb

try:
    import tqdm

    _tq_available = True
except (ImportError, ModuleNotFoundError):
    _tq_available = False

try:
    from colorama import Fore

except (ImportError, ModuleNotFoundError):
    Fore = None

__all__ = ["get_range", "get_loop"]


def get_tqdm_script_kwargs(desc: str, color: str,
                           ncols: int = 80, position: int = 0,
                           leave=False, **tqkwargs) -> dict:
    """

    Args:
        desc:
        color:
        ncols:
        position:
        leave:
        **tqkwargs:

    Returns:

    """
    if in_ipynb():
        return dict(position=position, desc=desc, leave=leave, **tqkwargs)
    try:
        color = getattr(Fore, color.upper())
        return dict(position=position, desc=desc, leave=leave,
                    bar_format="{l_bar}%s{bar}%s{r_bar}" % (color, Fore.RESET),
                    ncols=ncols, **tqkwargs)
    except AttributeError:
        return dict(position=position, desc=desc, leave=leave, ncols=ncols, **tqkwargs)


def get_range(N, *args, **kwargs):
    """

    Args:
        N:
        *args,
       **kwargs: arguments to parsed in :py:meth:`~get_tqdm_script_kwargs`

    Returns:

    """
    if not _tq_available:
        return range(N)
    if in_ipynb():
        from tqdm.notebook import trange
    else:
        from tqdm.auto import trange
    return trange(N, *args, **get_tqdm_script_kwargs(**kwargs))


def get_loop(iter_elem, *args, **kwargs):
    """

    Args:
        iter_elem: iterable
        *args:     additoinal aru
        **kwargs

    Returns:

    """
    if not _tq_available:
        return iter_elem
    if in_ipynb():
        from tqdm.notebook import tqdm
    else:
        from tqdm.auto import tqdm
    return tqdm(iter_elem, *args, **get_tqdm_script_kwargs(**kwargs))
