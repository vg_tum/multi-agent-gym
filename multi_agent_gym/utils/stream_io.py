import logging
import pathlib
import sys
from logging.handlers import RotatingFileHandler

_LOG_LEVEL = logging.INFO
_DEBUG = '\033[90m'
_OK_GREEN = '\033[92m'
_WARNING = '\033[93m'
_FAIL = '\033[91m'
_END = '\033[0m'
_VALUE_SIZE = 16
_KEY_MAX_SIZE = 30

__all__ = ["info", "debug", "error", "warning",
           "in_ipynb", "key_value_string",
           "clear_line", "get_logger", "framed_str"]


def assert_path(p: str or pathlib.Path) -> pathlib.Path:
    Pp = pathlib.Path(p).expanduser()
    if not Pp.exists():
        Pp.mkdir(parents=True)
    return Pp


def in_ipynb():
    """Checks if current script is executed in ipython notebook

    Returns:
        bool: True if currently in ipython mode
    """
    try:
        from IPython import get_ipython, InteractiveShell
        return isinstance(get_ipython(), InteractiveShell)
    except (NameError, ImportError, ModuleNotFoundError):
        return False


def truncate_str(x: str, max_len: int = _KEY_MAX_SIZE) -> str:
    return x[:max_len - 3] + '...' if len(x) > max_len else x


def _gen_value_str(x, value_size=_VALUE_SIZE):
    val_str = "{:.6g}".format(x) if isinstance(x, (float, int)) else str(x)
    return truncate_str(val_str, value_size)


def key_value_string(key, value, indent='', key_size=_KEY_MAX_SIZE, value_size=_VALUE_SIZE):
    base_str = f"| {indent}{{:<{key_size - len(indent)}}} | {{:>{value_size}}} |"
    return base_str.format(truncate_str(f"{key}", key_size), _gen_value_str(value))


def _color_print(color, msg, *args, sys_out=False, **kwargs):
    if sys_out:
        sys.stdout.write(color + msg + "".join(args) + _END)
    else:
        print(color, msg, *args, _END, **kwargs)


def _print_black(msg, *args, **kwargs):
    _color_print(_DEBUG, msg, *args, **kwargs)


def _print_green(msg, *args, **kwargs):
    _color_print(_OK_GREEN, msg, *args, **kwargs)


def _print_yellow(msg, *args, **kwargs):
    _color_print(_WARNING, msg, *args, **kwargs)


def _print_red(msg, *args, **kwargs):
    _color_print(_FAIL, msg, *args, **kwargs)


def debug(msg, *args, **kwargs):
    if _LOG_LEVEL <= logging.DEBUG:
        _print_black(msg, *args, **kwargs)


def info(msg, *args, **kwargs):
    if _LOG_LEVEL <= logging.INFO:
        _print_green(msg, *args, **kwargs)


def warning(msg, *args, **kwargs):
    if _LOG_LEVEL <= logging.WARNING:
        _color_print(msg, _WARNING, *args, **kwargs)


def warn(msg, *args, **kwargs):
    return warning(msg, *args, **kwargs)


def exception(msg, *args, **kwargs):
    if _LOG_LEVEL <= logging.WARNING:
        _color_print(msg, _WARNING, *args, **kwargs)


def error(msg, *args, **kwargs):
    if _LOG_LEVEL <= logging.ERROR:
        _color_print(msg, _FAIL, *args, **kwargs)


def critical(msg, *args, **kwargs):
    return error(msg, *args, **kwargs)


def clear_line():
    sys.stdout.write("\033[F")  # back to previous line
    sys.stdout.write("\033[K")  # clear line


class FakeLogger():

    def __init__(self, level, *args, **kwargs):
        global _LOG_LEVEL
        _LOG_LEVEL = int(level)

    @staticmethod
    def debug(msg, *args, **kwargs):
        debug(msg, *args, **kwargs)

    @staticmethod
    def info(msg, *args, **kwargs):
        info(msg, *args, **kwargs)

    @staticmethod
    def warn(msg, *args, **kwargs):
        warn(msg, *args, **kwargs)

    @staticmethod
    def warning(msg, *args, **kwargs):
        warn(msg, *args, **kwargs)

    @staticmethod
    def critical(msg, *args, **kwargs):
        critical(msg, *args, **kwargs)

    @staticmethod
    def error(msg, *args, **kwargs):
        error(msg, *args, **kwargs)

    @staticmethod
    def exception(msg, *args, **kwargs):
        exception(msg, *args, **kwargs)


def init_logger(log_name, stream_log_level=logging.INFO,
                file_log_level=logging.INFO, log_dir=None, log_filename=None,
                propagate=False):
    """
    Initialize Logging utility for


    Returns:
        my_logger(logging.Logger): logger handle which can be used throughout package
    Note:
        * Saves all debug output by default in log file Info >  is put in terminal using ``coloredlogs`` module
    Raises:
        AssertionError: In case the logging directory does not exist
    """
    global _LOG_ID
    format_str = f"[{log_name:^10s}]%(module)10s->%(funcName)10s#%(lineno)d\t%(message)s"

    my_logger = logging.getLogger(log_name)
    fmt = logging.Formatter(format_str)
    # set maximum level allowed
    my_logger.setLevel(min(file_log_level, stream_log_level))
    # optionally add file-handler
    if log_dir is not None:
        log_dir = assert_path(log_dir)
        log_file = log_dir / 'current.log' if log_filename is None else log_dir / log_filename
        file_handler = RotatingFileHandler(filename=log_file, maxBytes=10000000, backupCount=4)
        file_handler.setFormatter(fmt)
        file_handler.setLevel(file_log_level)
        my_logger.addHandler(file_handler)
    # set terminal output logger
    try:
        import coloredlogs
        coloredlogs.install(
            level=stream_log_level,
            logger=my_logger,
            fmt=format_str,
            datefmt="%H:%M:%S",
            level_styles=dict(
                debug=dict(color='green'),
                info=dict(color='cyan'),
                verbose=dict(color='blue'),
                warning=dict(color='yellow'),
                error=dict(color='red'),
                critical=dict(color='red', bold=True)
            ),
            field_styles=dict(
                asctime=dict(color='green'),
                hostname=dict(color='magenta'),
                levelname=dict(color='green', bold=True),
                filename=dict(color='magenta'),
                name=dict(color='blue'),
                threadName=dict(color='green')
            )
        )
    except ModuleNotFoundError:
        stream_handler = logging.StreamHandler()
        stream_handler.setFormatter(fmt)
        stream_handler.setLevel(stream_log_level)
        my_logger.addHandler(stream_handler)
    my_logger.propagate = propagate
    return my_logger


def get_logger(name, level=logging.INFO, check_ipy=False, **logger_kwargs):
    if check_ipy and in_ipynb():
        return FakeLogger(level)
    if name is None:
        return logging.getLogger(_LOG_ID)
    return init_logger(name, stream_log_level=level, **logger_kwargs)


def framed_str(msg, frame_sign='-'):
    L = max(list(map(len, msg.split('\n'))))
    return f'\n{frame_sign * L}\n{msg:<}\n{frame_sign * L}'
