"""
Utilities and Helpers
-----------------------

This module contains various helpers and functions that are for example used to map
the decentralized structure of the multi-agent gym to the functionality of default gym ([OpenAI2016]_).
"""
from typing import Sequence

import gym
import numpy as np

import multi_agent_gym.core as _mag_core
from .stream_io import get_logger

logger = get_logger("helpers")

__all__ = ["set_action_space", "set_observation_space",
           "parse_action", "filter_kwargs",
           "fuse_state_spaces", "fuse_discrete_spaces",
           "fuse_obs_spaces", "fuse_action_spaces",
           "fuse_box_spaces"]


def fuse_discrete_spaces(space1, space2):
    # type: (gym.spaces.Discrete, gym.spaces.Discrete) -> gym.spaces.Discrete
    """
    Fuse 2 discrete spaces

    Args:
         space1(gym.spaces.Discrete): space which space2 will be fused to
         space2(gym.spaces.Discrete): space to append to space 1
    Returns:
        space.Discrete: combined space of space1 and space2
    """
    try:
        return gym.spaces.Discrete(space1.n + space2.n)
    except Exception:
        raise NotImplementedError


def fuse_box_spaces(space1, space2):
    # type: (gym.spaces.Box, gym.spaces.Box) -> gym.spaces.Box
    """
    Fuse 2 spaces of type gym.spaces.Box

    Args:
         space1(gym.spaces.Box): space which space2 will be fused to
         space2(gym.spaces.Box): space to append to space 1
    Returns:
        space.Box: combined space of space1 and space2
    """
    space1.low = np.append(space1.low, space2.low)
    space1.high = np.append(space1.high, space2.high)
    try:
        space1.bounded_below = np.append(space1.bounded_below, space2.bounded_below)
        space1.bounded_above = np.append(space1.bounded_above, space2.bounded_above)
    except AttributeError:
        pass
    tmp_list = []
    for s1, s2 in zip(space1.shape, space2.shape):
        tmp_list.append(s1 + s2)
    space1._shape = tuple(tmp_list)
    return space1


def _get_agent_action_space(ag):
    return ag.action_space


def _get_agent_state_space(ag):
    return ag.state_space


def _get_agent_obs_space(ag):
    return ag.obs_space


def _fuse_spaces(agents, get_space_handle):
    # type: (Sequence[object], callable) -> gym.spaces.Space
    """
    Fuse all action spaces of individual agents to a central joint action space

    Args:
         agents(Sequence[_mag_core.BaseAgent]):   List of agents to be fused (modularized)
    Note:
        This implementation onl supports fusing actions of identical types
    """
    assert len(agents) > 0, "fusing only works with at least one agent"
    a0_space = get_space_handle(agents[0])
    if isinstance(a0_space, gym.spaces.Box):
        fused_space = gym.spaces.Box(low=a0_space.low,
                                     high=a0_space.high,
                                     dtype=a0_space.dtype)
        for a in agents[1:]:
            fused_space = fuse_box_spaces(fused_space, get_space_handle(a))
    elif isinstance(a0_space, gym.spaces.Discrete):
        fused_space = gym.spaces.Discrete(n=a0_space.n)
        for a in agents[1:]:
            fused_space = fuse_box_spaces(fused_space, get_space_handle(a))
    elif a0_space is None:
        raise RuntimeError("could not fuse action-spaces")
    else:
        raise NotImplementedError("implement this")
    return fused_space


def fuse_action_spaces(agents):
    # type: (Sequence[_mag_core.BaseAgent]) -> gym.spaces.Space
    """
    Fuse all action spaces of individual agents to a central joint action space

    Args:
         agents(Sequence[_mag_core.BaseAgent]):   List of agents to be fused (modularized)
    Note:
        This implementation onl supports fusing actions of identical types
    """
    return _fuse_spaces(agents, _get_agent_action_space)


def fuse_state_spaces(agents):
    # type: (Sequence[_mag_core.BaseAgent]) -> gym.spaces.Space
    """
    Fuse all action spaces of individual agents to a central joint action space

    Args:
         agents(Sequence[_mag_core.BaseAgent]):   List of agents to be fused (modularized)
    Note:
        This implementation onl supports fusing actions of identical types
    """
    return _fuse_spaces(agents, _get_agent_state_space)


def fuse_obs_spaces(agents):
    # type: (Sequence[_mag_core.BaseAgent]) -> gym.spaces.Space
    """
    Fuse all action spaces of individual agents to a central joint action space

    Args:
         agents(Sequence[_mag_core.BaseAgent]):   List of agents to be fused (modularized)
    Note:
        This implementation onl supports fusing actions of identical types
    """
    return _fuse_spaces(agents, _get_agent_obs_space)


def set_action_space(env: _mag_core.MultiAgentEnv, agents, use_dict_spaces=True):
    """
    Fuse the action space of all agents to a joint action space
    See :py:mod:`~fuse_action_spaces` for further insights

    Args:
        env(MultiAgentEnv): current environment
        agents(list)
        use_dict_spaces(bool, optional): flag to use dictionary based observation spaces. Defaults to True.
    """
    assert agents is not None, "environment contains no agents"
    if use_dict_spaces:
        env.action_space = gym.spaces.Dict({a.name: a.action_space for a in agents})
    else:
        env.action_space = fuse_action_spaces(agents)


def set_observation_space(env: _mag_core.MultiAgentEnv, agents, use_dict_spaces=True, decentral=True) -> None:
    """
    Map the individual observation spaces of all agent to a joint observation space of the environment.
    There are two options implemented:

    * In the decentralized setting the :py:mod:`~fuse_obs_spaces` is concatenating all agent observations
      which contain the agent specific perception of the environment.
    * In the centralized setting, the   :py:mod:`~fuse_state_spaces` is concatenating all agent internal states
      with adding the state of the world at the end, returning a fully observable state of the centralized
      environment.

    Todo:
        Extend this to all state spaces

    Args:
        env(MultiAgentEnv): current environment
        agents(list): agents in the environment as a list
        use_dict_spaces(bool, optional): flag to use dictionary based observation spaces. Defaults to True.
        decentral(bool, optional): flag to enable autonomous agents with individual observation. Defaults to True
    """
    assert agents is not None, "environment contains no agents"
    if decentral:
        if use_dict_spaces:
            env.observation_space = gym.spaces.Dict({a.name: a.obs_space for a in agents})
        env.observation_space = fuse_obs_spaces(agents)
    else:
        assert env.world_state_space is not None, \
            "environment contains no world state"
        all_state_spaces = fuse_state_spaces(agents)
        if isinstance(env.world_state_space, gym.spaces.Box) and isinstance(all_state_spaces, gym.spaces.Box):
            env.observation_space = fuse_box_spaces(all_state_spaces, env.world_state_space)
        raise NotImplementedError


def parse_action(env, action):
    """Parse action to agents. Deprecated"""
    if isinstance(action, np.ndarray) and len(action.shape) == 1:
        action = np.reshape(action, (env.nb_agents, *env.agents[0].action_space.shape))
    elif isinstance(action, dict):
        for agent in filter(lambda x: x.action_space is not None, env.agents):
            agent.set_action(action[agent.name])
        return
    for n, agent in enumerate(env.agents):
        if agent.action_space is None:
            continue
        agent.set_action(action[n])


def filter_kwargs(env, **kwargs):
    for k, v in kwargs.items():
        if hasattr(env, k):
            setattr(env, k, v)
        elif hasattr(env, "_{}".format(k)):
            setattr(env, "_{}".format(k), v)
        logger.warning(f"Unknown attribute {k} with value: {v} provided")


def get_reward_range(agents, scalar_reward: bool = False):
    if any([x is None for x in agents]):
        r_min, r_max = -np.inf, np.inf
    elif scalar_reward:
        r_min = np.sum(np.array([x.reward_range[0] for x in agents]))
        r_max = np.sum(np.array([x.reward_range[1] for x in agents]))
    else:
        return np.array([x.reward_range for x in agents])
    return np.array([r_min, r_max])
