import pathlib
import pprint
import typing
import click
import re

import numpy as np

import multi_agent_gym.envs as _mag_envs
import multi_agent_gym.core as _mag_core
import multi_agent_gym.utils as _mag_utils

__all__ = ["execute_episode", "generate_random_episode", "generate_random_data",
           "load_and_extract_agent_data",
           "list_environments", "is_mag_env",
           "run_random_policy_multi_agent_env"]

logger = _mag_utils.get_logger('multi-agent-gym')


def _get_mag_envs():
    return sorted([spec.id for spec in _mag_envs.multi_agent_registry.all_multi_agent()])


def _get_envs():
    return sorted([spec.id for spec in _mag_envs.multi_agent_registry.all()])


def is_mag_env(env_id):
    """
    Check if environment is registered in multi-agent gym

    Args:
        env_id(str): environment id as human-readable identifier

    Returns:
        bool: True, if env-id is registered
    """
    for spec in _mag_envs.multi_agent_registry.all_multi_agent():
        if env_id == spec.id:
            return True
    return False


def execute_episode(
        policy: typing.Callable[[_mag_core.EnvFeedback], dict],
        env: _mag_core.MultiAgentEnv,
        loop_level: int = 1,
        render: bool = False, catch_all=False, catch_runtime=True,
        nb_steps: int = 200, data: dict or None = None) -> _mag_core.EpisodeData:
    """
    Helper function to execute a policy on a multi-agent environment,

    Args:
        policy(typing.Callable[[EnvFeedback], dict]): actor / policy for current environment
        env(MultiAgentEnv): current environment
        nb_steps(int, optional): number of steps to be run in current environment
        loop_level(int, optional): level for :py:func:`tqdm.auto.trange` if available
        render(bool, optional): rendering flag.
        catch_all(bool, optional): flag to catch all Exceptions
        catch_runtime(bool, optional): flag to catch all Exceptions
        data(dict or None, optional): optional external data that is stored along episode-trajectory

    Returns:
        EpisodeData: collected data of current episode
    """

    def step():
        fb = env.multi_agent_step(action)
        OBS.append(fb.observation)
        A.append(action)
        R.append(fb.reward)
        if fb.info:
            infos.append(fb.info)
        return fb.done

    OBS = [env.reset()]
    R = []
    A = []
    infos = []
    done = False
    for ti in _mag_utils.get_range(nb_steps, color='green', desc='steps', position=loop_level):
        action = policy(OBS[-1])
        if catch_all:
            try:
                done = step()
            except Exception as e:
                logger.error(f"encountered error {e} after {ti + 1} / {nb_steps} steps. Cancel execution")
                break
        elif catch_runtime:
            try:
                done = step()
            except RuntimeError as e:
                logger.error(f"encountered error {e} after {ti + 1} / {nb_steps} steps. Cancel execution")
                break
        else:
            done = step()
        if done:
            logger.info(f"succeeded after {ti + 1} steps")
            break
        if render:
            env.render()
    env_info = dict(data=data, infos=infos)
    if not env_info['data'] and not env_info['infos']:
        env_info = None
    return _mag_core.EpisodeData(observations=OBS, rewards=R, actions=A, env_info=env_info, success=done)


def generate_random_episode(
        env: _mag_core.MultiAgentEnv,
        filename: typing.Union[str, pathlib.Path],
        min_traj_length: int = 5, enforce_one_hot_encoding: bool = True, **eps_kwargs) -> None:
    """
    Generate random episodes and save result to data-file, if the batch is sufficiently long,
    i.e. it contains more than ``min_traj_length`` elements.

    Actual episode / data collection is run via :py:func:`~execute_episode`, data is collected as
    type :py:class:`~multi_agent_gym.core.EpisodeData` and is stored via
    :py:func:`~multi_agent_gym.utils.file_io.save_pickle.

    Args:
        env(_mag_core.MultiAgentEnv): current environment
        filename(typing.Union[str, pathlib.Path]): path to file where episode-result will be stored
        min_traj_length(int, optional): minimum episode batch-size. Defaults to 5
        enforce_one_hot_encoding(bool, optional): enforce one-hot encoding for discrete agent-action-spaces. Defaults to True.
        **eps_kwargs: optional arguments for :py:func:`~execute_episode`,
    """

    # https://stackoverflow.com/questions/29831489/convert-array-of-indices-to-1-hot-encoded-numpy-array
    def generate_one_hot_random_policy(_):
        return {k: E[k][ag.action_space.sample()] for k, ag in env.agent_dict.items()}

    def generate_random_policy(_):
        return {k: ag.action_space.sample() for k, ag in env.agent_dict.items()}

    random_policy = generate_random_policy
    E = {ag: np.empty([]) for ag in env.agent_dict}
    if enforce_one_hot_encoding and any([np.isscalar(x) for x in list(generate_random_policy(env.reset()).values())]):
        E = {a: np.eye(ag.action_space.n, dtype=np.int) for a, ag in env.agent_dict.items()}
        random_policy = generate_one_hot_random_policy
    eps_data = execute_episode(random_policy, env, **eps_kwargs)
    assert eps_data.valid(), "collected episode data is invalid"
    try:
        if len(eps_data.observations) > min_traj_length:
            _mag_utils.save_pickle(eps_data, f"{filename}", force_overwrite=True)
    except TypeError:
        logger.error(f"could not check length of feedback data. "
                     f"Obtained unknown type {type(eps_data.observations)}")


def generate_random_data(
        env: _mag_core.MultiAgentEnv, filename: str, nb_episodes: int,
        enforce_hot_one_encoding=True,
        loop_level: int = 0,
        **eps_kwargs) -> None:
    """
    Generate random data from an environment.
    The data is collected via :py:func:`~generate_random_episode`,
    and thus eventually :py:func:`~execute_episode`

    Args:
        env(MultiAgentEnv): environment to be evaluated
        filename(str or pathlib.Path): path to base-file
        loop_level(int, optional): level for :py:func:`tqdm.auto.trange` if available
        enforce_hot_one_encoding(bool, optional): enforce one-hot-encoding for discrete action-spaces
        nb_episodes(int): number of episodes to be run
    """
    assert nb_episodes > 0, "number of episodes needs to be positive"
    if nb_episodes == 1:
        return generate_random_episode(env, filename=filename, loop_level=loop_level, **eps_kwargs)

    for ei in _mag_utils.get_range(nb_episodes, color='red', desc='episodes', position=loop_level):
        generate_random_episode(env, filename=f'{filename}_{ei:03}', loop_level=loop_level + 1,
                                **eps_kwargs)


@click.command()
@click.option('--multi-agent-only/--no-multi-agent-only', default=True, help="only print multi-agent environments")
def list_environments(multi_agent_only):
    """print all environments in current registry."""
    if multi_agent_only:
        pprint.pprint(_get_mag_envs())
    else:
        pprint.pprint(_get_envs())


@click.command()
@click.argument("env_ids", type=click.Choice(_get_mag_envs() + ['all']), nargs=-1)
@click.option("--render/--no-render", default=False, help="enable rendering for each episode")
@click.option("--steps", default=200, help="number of steps per episode. Defaults to 200")
@click.option("--episodes", default=1, help="number of episodes. Defaults to 1")
@click.option("--data-path", default=_mag_utils.DATA_PATH,
              help=f"path to store data. Defaults to {_mag_utils.DATA_PATH}")
@click.option("--one-hot/--no-one-hot", default=True,
              help=f"Enforces one-hot-encodings for discrete action-spaces. Defaults to True.")
@click.option("--filename", type=str, default="",
              help="filename to store data at. Defaults to {env_id}_random_batch, "
                   "where env_id is taken from the arguments")
def run_random_policy_multi_agent_env(env_ids, render, steps, episodes, data_path, filename, one_hot):
    """
    Generate random episode batches for a given (or all) registered multi-agent environment.
    The argument `env-id` sets the environment to generate data from.
    if 'all' is selected, data for all registered environments is created

    This script allows optional rendering at runtime. Each run will be collected in a dedicated directory in
    a separate. These files may be inspected via the `load_and_extract_agent_data` function afterwards.
    """

    def get_file(e_id):
        if filename:
            if env_ids[0] == "all" or len(env_ids) > 1:
                return data_path / f"{filename}_{e_id}"
            else:
                return data_path / f"{filename}"
        return data_path / f"{e_id}_random_batch"

    def run_env(e_id):
        env = _mag_envs.make(e_id)
        generate_random_data(env, filename=get_file(e_id), loop_level=loop_level,
                             nb_steps=steps, enforce_hot_one_encoding=one_hot,
                             nb_episodes=episodes,
                             render=render)

    loop_level = 1
    data_path = _mag_utils.assert_path(data_path)
    if len(env_ids) == 1:
        if env_ids[0] == 'all':
            all_envs = _get_mag_envs()
            files = "\n".join([f"{e:<30}\t->\t{get_file(e)}" for e in all_envs])
            logger.info(f"creating data for all existing environments with {steps} steps for {episodes}. "
                        f"Data will be stored at:\n{files}")
            for ex in _mag_utils.get_loop(all_envs, color='yellow', desc='environment', position=0):
                run_env(ex)
        else:
            loop_level = 0
            logger.info(f"data for {env_ids[0]} with {steps} steps for {episodes}. "
                        f"The resulting data will be stored at:\n{get_file(env_ids[0])}_DDD.pkl")
            run_env(env_ids[0])
    else:
        files = "\n".join([f"{e:<30}\t->\t{get_file(e)}" for e in env_ids])
        logger.info(f"data for:\n{pprint.pformat(env_ids)}\nwith {steps} steps for {episodes}. "
                    f"Data will be stored at:\n{files}")
        for ex in _mag_utils.get_loop(env_ids, color='yellow', desc='environment', position=0):
            run_env(ex)


@click.command()
@click.argument("env_ids", type=click.Choice(_get_mag_envs() + ['all']), nargs=-1)
@click.option("--force/--no-force", default=True, help=f"overwrite existing files")
@click.option("--verbose/--no-verbose", default=True, help=f"enable verbose printing")
@click.option("-t", "--file-type", default="yaml", type=click.Choice(("json", "yaml", "pickle")),
              help=f"choose file-type to store config. Defaults to yaml if PyYAML is available. "
                   f"Falls back to json and eventually to pickle")
@click.option("-d", "--data-path", default=pathlib.Path.cwd(),
              help=f"path to store data. Defaults to current directory ({pathlib.Path.cwd()}")
def env_id_to_cfg(env_ids, data_path, force, verbose, file_type):
    """
    Generate kwargs-config from current environment-ID
    """
    data_path = _mag_utils.assert_path(data_path)
    if 'all' in env_ids:
        env_ids = _get_mag_envs()
    for e in env_ids:
        try:
            nb_agents = int(re.findall(r'\d+', ''.join(re.findall(r'N\d+', e)))[0])
        except IndexError:
            logger.error(f"could not parse number of agents from {e}")
            continue
        try:
            version = int(re.findall(r'\d+', ''.join(re.findall(r'v\d+', e)))[0])
        except IndexError:
            logger.error(f"could not parse version from {e}")
            continue
        env_base = e.split('-')[0]
        discrete_action = re.match(r".*Disc-N\d+-v\d+", env_base) is not None
        mlp_action = re.match(r".*Mlp-N\d+-v\d+", env_base) is not None
        if discrete_action:
            env_base = env_base[:env_base.find('Disc')]
        elif mlp_action:
            env_base = env_base[:env_base.find('Mlp')]
        else:
            try:
                env_base = env_base[:env_base.find('Cont')]
            except IndexError:
                pass
        sparse = re.match(r".*Sparse.*-N\d+-v\d+", e) is not None
        scenario = env_base.replace('__', '_').lower()
        print(scenario)
        env_base = env_base.split('_')[0].lower()
        scenario = re.findall(f"[a-z]+.*[a-z]+", scenario.replace(env_base, ''))[0]
        data = dict(env_id=e, env_base=env_base, scenario=scenario,
                    sparse_rewards=sparse, discrete_action=discrete_action, mlp_action=mlp_action,
                    nb_agents=nb_agents, env_version=version)
        path = _mag_utils.assert_path(data_path / f"{env_base}")
        data.update(_mag_envs.spec(e).get_kwargs())
        if verbose:
            logger.info(f"{e} => version: {version} with {nb_agents} agents")
            logger.info(f"base-name {env_base} and scenario {scenario}")
            logger.info(f"data:{data}\nwill be stored in {data_path / env_base / scenario}.{file_type}")
        if file_type == "yaml":
            try:
                _mag_utils.save_yaml(data, path / scenario, force_overwrite=force)
            except ModuleNotFoundError as e:
                logger.error(f"could not save data as yaml: {e}. Use Json instead")
                file_type = "json"
        if file_type == "json":
            _mag_utils.save_json(data, path / scenario, force_overwrite=force)
        elif file_type == "pickle":
            _mag_utils.save_json(data, path / scenario, force_overwrite=force)


@click.command()
@click.argument("env_id", type=click.Choice(_get_mag_envs()))
@click.option("--data-path", default=_mag_utils.DATA_PATH,
              help=f"path to store data. Defaults to {_mag_utils.DATA_PATH}")
@click.option("--filename", type=str, default="",
              help="filename to store data at. Defaults to {env_id}_random_batch, where env_id is taken from the arguments")
def load_and_extract_agent_data(env_id, data_path, filename):
    """
    Load an episode batch and print information
    Args:
        env_id:
        data_path:
        filename:

    Returns:

    """
    data_path = _mag_utils.assert_path(data_path)
    if not filename:
        filename = f"{env_id}_random_batch_000.pkl"
    filename = data_path / filename
    if _mag_utils.file_exists(filename):
        data = _mag_utils.load_pickle(filename)
        print(_mag_core.episode_info(data, title=filename.stem))
    else:
        logger.error(f"file {filename} does not exist")
