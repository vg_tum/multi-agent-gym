r"""
Multi Agent Gym
----------------

This module wraps the modules provided by [OpenAI2016]_ gym into a multi-agent domain, while bridging the API consistent
with the default gym structure.

In contrast to default gym environments, a multi-agent environment consists of multiple :py:class:`~BaseAgent` agents,
which are then fused into a central :py:class:`~MultiAgentEnv` environment.
The total action-, state- and observation-space composite from each individual agent as well as the *world* state.
In order to outline this explanation thoroughly, the components of the :py:class:`~BaseAgent`
class will be outlined first. The basic outline is that:

* Each agent has internal states, which are (in general) fully observable to it.
* The world has a separate world state.
* The environment state is then a combination of all internal agent states and the world state.
* The observation of each agent consist of an extension of the internal state and the *perceived* states of the other
  agents, as well as the world state.
* The world as such has no observation state, such that the observation space is returned as a combination of all
  individual states as well as the world state.
* As each agent is assumed to act independently, the global agent space is analogously fused from each agent's
  individual agent space.
* The reward range of the global environment is then finally fused from each individual reward ranges of each agent,
  with the difference that it is differentiated between a centralized reward metric, e.g. accumulated team reward
  or individual payoff-metrics.

Each agent has an own observation-, state and action space. Accordingly, an individual state, observation and action
and thus serves as a reduced gym-environment as such.
As in general each agent receives an individual reward, this class must also return / set an agent specific reward.
In order to ease the assessment, each agent as a unique identifier given as an integer, which eases hashing and
equality checks. Further a unique name is strongly recommended, to allow human-readable printouts / logging.

Even though the main gym-styled functions are handled within the environment, within a multi-agent environment
an agent implementation is expected to provide a set of functions, which are outlined in short

* :py:meth:`~BaseAgent.name`: return a human readable string
* :py:meth:`~BaseAgent.reset`: analogue function call of gym-environment reset, but handles all internal state variables
* :py:meth:`~BaseAgent.set_action`: as outlined later, the environment gets joint actions as input, which are then fed
  to each individual  agent. which is then calling this function to actually set the action for each agent,
  before updating the environment directly afterwards.
* :py:meth:`~BaseAgent.info`: set the environment specific, yet agent specific, additional information as needed
  into this dictionary.
* :py:meth:`~BaseAgent.end_step`: This function serves as the antagonist of ``set_action`` and is called at the end of
  a step to update the agents state or reset any single-step information

In contrast to this, the :py:mod:`~.MultiAgentEnv` class wraps the classical gym environment to the multi-agent domain.
The main important attribute is thus the ``agent`` Attribute, which wraps all agents in a list, which allows a
centralized execution that can be mapped to the standard gym function interfaces. In total, the environment maps the
main functionality of [OpenAI2016]) as:

* :py:meth:`~MultiAgentEnv.step`: the extended environment update that allows setting a joint action for multiple
  agents involved.
* :py:meth:`~MultiAgentEnv.reset`: Extended environment reset, that calls each individual agent resets and also resets
  the shared environment to then return a joint (initial) observation of the environment.
* :py:meth:`~MultiAgentEnv.render`: analogue version of gyms :py:meth:`~gym.core.Env.render` (see [OpenAI2016]_)
* :py:meth:`~MultiAgentEnv.close`: analogue version of gyms :py:meth:`~gym.core.Env.close` (see [OpenAI2016]_)
* :py:meth:`~MultiAgentEnv.seed`: analogue version of gyms  :py:meth:`~gym.core.Env.seed` (see [OpenAI2016]_)
* :py:meth:`~MultiAgentEnv.action_space`: The Space object corresponding to valid actions, parsed from agents attribute.
* :py:meth:`~MultiAgentEnv.observation_space`: The Space object corresponding to valid observations. parsed from agents.
* :py:meth:`~MultiAgentEnv.reward_range`: A tuple corresponding to the min and max possible rewards

As the step function is not as straight forward due to the multi-agent characteristics, the multi-agent environment
needs further extensions per environment implementation.
This brings in the following additional function calls, which are all wrapped into the step call function.

* :py:meth:`~MultiAgentEnv._set_actions`: set action of all agents
* :py:meth:`~MultiAgentEnv._sim_world`: updates environment
* :py:meth:`~MultiAgentEnv._step_update`: update observation and reward for a single agent and joint environment given the current task/environment/scenario.

"""
import abc
from collections import OrderedDict
import dataclasses
import functools
import typing

import gym
import numpy as np

from pkg_resources import parse_version

import multi_agent_gym.spaces
import multi_agent_gym.utils as _utils

try:
    from gym.utils import closer

    env_closer = closer.Closer()
except (ModuleNotFoundError, ImportError):
    pass
logger = _utils.get_logger(__name__)

__all__ = ["EnvFeedback", "EpisodeData", "AgentEpisodeData",
           "BaseAgent", "MultiAgentEnv", "GaolMultiAgentEnv"]


@dataclasses.dataclass(frozen=True)
class EnvFeedback:
    observation: typing.Dict[str, typing.Any]
    reward: typing.Dict[str, typing.Any]
    done: bool
    info: dict = dataclasses.field(default_factory=dict)

    def as_tuple(self):
        return self.observation, self.reward, self.done, self.info


@dataclasses.dataclass(frozen=True)
class EpisodeData:
    observations: typing.List[typing.Dict[str, typing.Any]]
    actions: typing.List[typing.Dict[str, typing.Any]]
    rewards: typing.List[typing.Dict[str, typing.Any]]
    success: bool
    env_info: typing.Any = None

    @property
    def agent_names(self) -> typing.Set[str]:
        try:
            return set(self.observations[0].keys())
        except IndexError:
            return set()

    def valid(self):
        return len(self.observations) - 1 == len(self.rewards) == len(self.actions)


@dataclasses.dataclass(frozen=True)
class AgentEpisodeData:
    x: typing.Union[np.ndarray, typing.Dict[str, np.ndarray]]  # (D+1 x X)
    y_env: typing.Union[np.ndarray, typing.Dict[str, np.ndarray]]  # (D+1 x Y_env)
    y_ag: typing.Dict[str, np.ndarray]  # (D+1 x (N - 1) x Y_ag)
    actions: np.ndarray  # (D x A)
    rewards: np.ndarray  # (D x 1)
    success: bool

    def valid(self):
        try:
            N_x = self.x.shape[0]
        except AttributeError:
            N_x = [v.shape[0] for v in self.x.values()]
            assert len(N_x) == len(set(N_x))
            N_x = N_x[0]
        try:
            N_ye = self.y_env.shape[0]
        except AttributeError:
            N_ye = [v.shape[0] for v in self.y_env.values()]
            assert len(set(N_ye)) == 1
            N_ye = N_ye[0]
        return N_ye - 1 == N_x - 1 == self.actions.shape[0] == self.rewards.shape[0]

    @classmethod
    def extract_from_episode_data(cls, eps_data: EpisodeData, ag_name, all_agents):
        def stack_obs(key):
            if isinstance(obs0[key], dict):
                return {sub_key: np.array([elem[ag_name][key][sub_key] for elem in eps_data.observations]) for sub_key
                        in obs0[key].keys()}
            return np.array([elem[ag_name][key] for elem in eps_data.observations])

        def stack_agent_obs(oag):
            if isinstance(obs0['y_ag'][oag], dict):
                return {sub_key: np.array([elem[ag_name]['y_ag'][oag][sub_key] for elem in eps_data.observations]) for
                        sub_key
                        in obs0['y_ag'][oag].keys()}
            return np.array([elem[ag_name]['y_ag'][oag] for elem in eps_data.observations])

        obs0 = eps_data.observations[0][ag_name]
        try:
            x = stack_obs('x')
            y_ag = {oag: stack_agent_obs(oag) for oag in all_agents if oag != ag_name}
            y_env = stack_obs('y_env')
            act = [elem[ag_name] for elem in eps_data.actions]
            rew = [elem[ag_name] for elem in eps_data.rewards]
            out = cls(x=np.array(x), y_env=y_env, y_ag=y_ag,
                      actions=np.array(act), rewards=np.array(rew), success=eps_data.success)
            if not out.valid():
                raise RuntimeError(
                    f"Episode data has corrupt data. Could not extract valid agent episode for {ag_name}")
            return out
        except KeyError as e:
            logger.error(f"could not extract Agent-Episode Data due to {e}")
            raise RuntimeError("Episode data has corrupt data. Could not extract valid agent episode for {ag_name}")

    @property
    def average_reward(self):
        return np.mean(self.rewards)

    @property
    def std_reward(self):
        return np.std(self.rewards)


def episode_info(eps_data: EpisodeData, key_size=25, value_size=22, title="ENV-data") -> str:
    """extract information from an episode data buffer """
    main_line = functools.partial(_utils.key_value_string, key_size=key_size, value_size=value_size)
    block_size = (key_size + value_size + 6)
    out = [f"{title.upper():-^{block_size}}",
           main_line("agent_names", ','.join(eps_data.agent_names)),
           main_line("eps-length", len(eps_data.actions)),
           main_line("success", eps_data.success)]
    ag_line = functools.partial(_utils.key_value_string, key_size=key_size, value_size=value_size, indent=' ')
    for ag in eps_data.agent_names:
        ag_data = AgentEpisodeData.extract_from_episode_data(eps_data, ag, all_agents=eps_data.agent_names)
        out += [f"{ag:-^{block_size}}"]
        try:
            out += [ag_line("D_x", ag_data.x.shape), ]
        except AttributeError:
            out += [ag_line(f"D_x({k})", v.shape) for k, v in ag_data.x.items()]
        try:
            out += [ag_line("D_y_env", ag_data.y_env.shape), ]
        except AttributeError:
            out += [ag_line(f"D_y_env({k})", v.shape) for k, v in ag_data.y_env.items()]
        try:
            out += [ag_line(f"D_y_ag({oag})", elem.shape) for oag, elem in ag_data.y_ag.items()]
        except AttributeError:
            out += [ag_line(f"D_y_ag({oag}-{subkey})", elem.shape) for oag in ag_data.y_ag.keys()
                    for subkey, elem in ag_data.y_ag[oag].items()
                    ]
        out += [ag_line("D_a", ag_data.actions.shape),
                ag_line("D_r", ag_data.rewards.shape),
                ag_line("μ(rew)", ag_data.average_reward),
                ag_line("σ(rew)", ag_data.std_reward)]
    return "\n".join(out)


class BaseAgent(abc.ABC):
    """
    Base template for an agent within a multi-agent environment.
    """
    _id = None  # type: int
    observation_space: multi_agent_gym.spaces.AgentObservation
    state_space: gym.spaces.Space
    action_space: gym.spaces.Space

    @property
    def name(self):
        """
        string id / name of agent. Defaults to "agent1", ..., "agentN"

        Returns:
            str:    human-readable string id
        """
        return f"agent_{self._id + 1}"

    def reset(self):
        """
        Reset agent
        """
        pass

    @abc.abstractmethod
    def set_action(self, action, time=None):
        """
        Set action for agent given the current action space and limits

        Args:
            action(object): action to be set for current agent
            time(float or None): optional current time value

        Returns:
             object: action assigned after running constraint checks
        """

    @property
    def info(self):
        """
        Optional information return

        Returns:
             dict: agent specific information
        """
        return {}

    def __int__(self):
        return int(self._id)

    def __hash__(self):
        return self._id

    def __eq__(self, other):
        if self.__class__ == other.__class__:
            return int(self) == int(other)
        else:
            return False

    def __lt__(self, other):
        return int(self) < int(other)

    def __str__(self):
        return self.name


class BaseModelAgent(BaseAgent, abc.ABC):
    """Model-based variant of :py:class:`~BaseAgent`,
    i.e. this class provides access to:

    * internal agent forward-dynamics via :py:meth:`~f`
    * linearized system via :py:meth:`~linear_model`.
    """

    @property
    def n_dof(self):
        return

    @property
    @abc.abstractmethod
    def x(self) -> np.ndarray:
        """agent state at current time step"""

    @property
    @abc.abstractmethod
    def y(self):
        """agent observation at current time step"""

    @property
    @abc.abstractmethod
    def u(self) -> np.ndarray:
        """agent control input at current time step"""

    @abc.abstractmethod
    def f(self, x, u):
        r"""internal dynamic model to calculate next state of agent i.e.

        .. math::

            {\bf x}_{t+1} = {\bf f}({\bf x}_{t}, {\bf u}_{t})

        Returns:
            object: numpy array or torch.Tensor
        """

    @abc.abstractmethod
    def linear_model(self) -> (np.ndarray, np.ndarray):
        """get LTI dynamics (discrete), assuming full observability for internal agent state

        .. math::

            {\bf x}_{t+1} = {\bf A}({\bf x}_{t} + {\bf B} {\bf u}_{t})
        """


class MultiAgentEnv(gym.Env):
    r"""Extension of openAi Gym as a multi-agent environment for arbitrary multi-agent scenes.

    """
    _agents = OrderedDict()
    _ag_idx = {}  # type: typing.Dict[int, str]
    observation_space: multi_agent_gym.spaces.JointObservationSpace
    action_space: gym.spaces.Dict
    scalar_reward = False
    world_state_space = None
    world_state = None

    @property
    def nb_agents(self):
        try:
            return len(self._agents)
        except TypeError:
            return 0

    def multi_agent_step(self, action: typing.Dict[str, object]) -> EnvFeedback:
        """
        As this contradicts the default gym structure, a single step is unified for all environments. The assignment of
        actions to agents, as well as the actual environment update and (de)central state updates:

        #. apply actions of (1-N) agents in :py:meth:`~set_actions`
        #. simulate an environment update as defined in :py:meth:`~sim_world`
        #. get task specific observation, reward, done and info return value in :py:meth:`~step_return`

        Args:
            action(Sequence[object]):     joint action for all agents.
        Note:
            Adjust the agent specific set_action function in your environment for decentralized executions.
        Returns:
            EnvFeedback: step-feedback
        """
        # set actions for each agent, and adjust wrt limits as needed
        self.set_actions(action)
        # progress environment by one step
        self.env_update()
        return self.get_step_return()

    def step(self, action: typing.Union[dict, OrderedDict]) -> tuple:
        """default gym action"""
        return self.multi_agent_step(action).as_tuple()

    @property
    def agent_dict(self) -> OrderedDict:
        return self._agents

    @property
    def agent_list(self) -> list:
        return [self._agents[self._ag_idx[ai]] for ai in range(self.nb_agents)]

    def _set_observation_space(self,
                               agent_spaces: typing.Optional[
                                   typing.Dict[str, multi_agent_gym.spaces.AgentObservation]] = None):
        if agent_spaces is None:
            try:
                self.observation_space = multi_agent_gym.spaces.JointObservationSpace(
                    agent_names=list(self._agents.keys()),
                    agent_spaces=[ag.observation_space for ag in self._agents.values()])
            except AttributeError:
                pass
        else:
            self.observation_space = multi_agent_gym.spaces.JointObservationSpace(space_dict=agent_spaces)

    def add_agent(self, agent: typing.Union[BaseAgent, BaseModelAgent]):
        """add agent to environment"""
        self._agents[agent.name] = agent
        self._ag_idx[int(agent)] = agent.name
        self._set_observation_space()

    def set_actions(self, joint_action):
        """
        Set joint actions before calling simulation step update

        Args:
            joint_action (Dict[agent_id, object]): joint action  vector to be applied on the environment
        """
        raise NotImplementedError()

    def get_step_return(self) -> EnvFeedback:
        """
        Returns feedback in form of :py:class:`~EnvFeedback`

        Note:
            This function is intended to be called as often as needed, without altering the environment
            unlike :py:meth:`~step`, and explicitly after the update step has been processed.
        Returns:
            EnvFeedback: feedback after latest update
        """
        raise NotImplementedError()

    def reset(self) -> typing.Dict[str, typing.Any]:
        """
        Call a global environment reset function wih can reset all components at once or individually

        Returns:
            dict: initial observation
        """
        raise NotImplementedError()

    def render(self, mode: str = 'human', title=""):
        """
        simple original gym function, added for compatibility, but not mandatory.
        """
        pass

    def env_update(self) -> None:
        """
        Simulate an update of the simulated environment
        """
        raise NotImplementedError()

    def _close(self) -> None:
        """
        Close environment and do cleanups
        """
        raise NotImplementedError()

    if parse_version(gym.__version__) < parse_version('0.9.6'):
        raise NotImplementedError("UPDATE GYM VERSION higher than 0.9.6")

    def _idx2agent(self, ai: int):
        return self._agents[self._ag_idx[ai]]

    # backward compatibility
    def _sim_world(self):
        self.env_update()

    def _set_actions(self, action):
        self.set_actions(action)

    def __del__(self):
        self._close()


class GaolMultiAgentEnv(MultiAgentEnv):

    @property
    def goals(self) -> object or None:
        """
        Defines the (optional) environment goals for the multi-agent system.
        May be empty for non-goal driven environment

        Returns:
            object or None: a sequence of goals to be achieved or None if environment has not (joint) goal
        """
        return None

    def render_sub_goals(self, goals, **kwargs):
        """Optional render sub_goals function"""
        pass
