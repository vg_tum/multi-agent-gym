from gym import error
from gym.envs import registration
from multi_agent_gym.wrappers import TimeLimitMultiAgent

__all__ = ["register", "make", "spec", "multi_agent_registry"]

_multi_agent_specs = ["nb_agents", "heterogeneous", "cooperative"]
_gym_specs = ["entry_point", "reward_threshold",
              "kwargs", "nondeterministic",
              "max_episode_steps"]


class MuAgEnvSpec(registration.EnvSpec):
    """
    inherit Environment spec by agents list argument

    """

    def __init__(self, id, entry_point=None, reward_threshold=None,
                 kwargs=None, nondeterministic=False,
                 max_episode_steps=None, heterogeneous=True,
                 nb_agents=None, cooperative=True):
        super(MuAgEnvSpec, self).__init__(id=id, entry_point=entry_point, reward_threshold=reward_threshold,
                                          kwargs=kwargs, nondeterministic=nondeterministic,
                                          max_episode_steps=max_episode_steps)
        self.heterogeneous = heterogeneous
        self.nb_agents = nb_agents
        self.cooperative = cooperative

    def get_kwargs(self):
        return self._kwargs.copy()


class MuAgEnvRegistry(registration.EnvRegistry):
    """
    Small extension wrapper of Gym original EnvRegistry
    """

    def __init__(self):
        super(MuAgEnvRegistry, self).__init__()
        self._multi_agent_ids = []

    def register(self, id, **kwargs):
        if id in self.env_specs:
            raise error.Error('Cannot re-register id: {}'.format(id))
        self.env_specs[id] = MuAgEnvSpec(id, **kwargs)
        if any(x in kwargs.keys() for x in _multi_agent_specs):
            if kwargs["nb_agents"] >= 1:
                self._multi_agent_ids.append(id)

    def all_multi_agent(self):
        return [self.env_specs[x] for x in self._multi_agent_ids]

    def make(self, path, **kwargs):
        env = super().make(path, **kwargs)
        if (env.spec.max_episode_steps is not None) and hasattr(env.spec, 'nb_agents'):
            nb_agents = getattr(env.spec, "nb_agents", 1)
            if nb_agents is None or nb_agents == 1:
                return env
            return TimeLimitMultiAgent(env.env, max_episode_steps=env.spec.max_episode_steps)
        return env


multi_agent_registry = MuAgEnvRegistry()


def register(id, **kwargs):
    try:
        return registration.register(id, **kwargs)
    except:
        return multi_agent_registry.register(id, **kwargs)


def make(id, **kwargs):
    return multi_agent_registry.make(id, **kwargs)


def spec(id):
    return multi_agent_registry.spec(id)


def reg_helper(name, path, nb_agents, env_kwargs, layout=None, max_steps=200, rew_threshold=0.0,
               cooperative=True, heterogeneous=True, version=0):
    base_args = dict(nb_agents=nb_agents)
    if layout is not None:
        env_id = "{}-{}-N{:d}-v{:d}".format(name, layout, nb_agents, version)
        base_args.update(dict(layout=layout))
    else:
        env_id = "{}-N{:d}-v{:d}".format(name, nb_agents, version)
    env_kwargs.update(base_args)
    register(
        id=env_id, entry_point=path, kwargs=env_kwargs,
        nb_agents=nb_agents, max_episode_steps=max_steps, reward_threshold=rew_threshold,
        heterogeneous=heterogeneous, cooperative=cooperative
    )
