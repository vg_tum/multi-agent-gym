import abc
import typing

import gym.spaces
import numba as nb
import numpy as np
import scipy.spatial as sci_spat

import multi_agent_gym.core as _mag_core
import multi_agent_gym.envs.particles_wrapper.agents as _agents
import multi_agent_gym.envs.particles_wrapper.entities as _entities
import multi_agent_gym.envs.particles_wrapper.render as _render


@nb.njit
def _nb_distance_calculation(x):
    N = int(x.size // 4)
    delta = np.zeros((N, N, 2), dtype=x.dtype)
    dist = np.zeros((N, N), dtype=x.dtype)
    x = x.reshape((N, 4))
    for i in range(N):
        for j in range(1, N):
            delta[i, j, 0:2] = x[j, 0:2] - x[i, 0:2]
            delta[j, i, 0:2] = -1 * delta[i, j, 0:2]
            dist[i, j] = np.linalg.norm(delta[i, j])
            dist[j, i] = dist[i, j]
    return delta, dist


def _dist_calc(x):
    N = int(x.size // 4)
    delta = np.zeros((N, N, 2), dtype=x.dtype)
    dist = np.zeros((N, N), dtype=x.dtype)
    tmp = x.reshape((N, 4))
    for i in range(N - 1):
        delta[i, i + 1:] = tmp[i + 1:, 0:2] - tmp[i, 0:2]
        delta[i + 1:, i] = -tmp[i + 1:, 0:2] + tmp[i, 0:2]
        dist[i, i + 1:] = np.linalg.norm(tmp[i + 1:, 0:2] - tmp[i, 0:2], axis=1)
    return delta, dist


@nb.njit
def rotate_vector(vec, x, p_ub, p_lb):
    """rotate_vector"""
    if x[1] > p_ub[1] or x[1] < p_lb[1]:
        vec[1] *= -1.0
    elif x[0] > p_ub[0] or x[0] < p_lb[0]:
        vec[0] *= -1.0
    return vec


def _bounds_bounce(x, p_lb, p_ub, bounce_elasticity=0.8):
    for idx in np.where(np.any(x[:, 0:2] > p_ub, axis=1))[0]:
        x[idx, 2:4] = rotate_vector(x[idx, 2:4], x[idx], p_lb=p_lb[idx], p_ub=p_ub[idx]) * bounce_elasticity
        x[idx, 0:2] -= np.clip(x[idx, 0:2] - p_ub[idx], np.zeros(2), np.inf * np.ones(2))

    for idx in np.where(np.any(x[:, 0:2] < p_lb, axis=1))[0]:
        x[idx, 2:4] = rotate_vector(x[idx, 2:4], x[idx], p_lb=p_lb[idx], p_ub=p_ub[idx]) * bounce_elasticity
        x[idx, 0:2] += np.clip(p_lb[idx] - x[idx, 0:2], np.zeros(2), np.inf * np.ones(2))

    return x


def _limit_to_bounds(x, x_prev, p_lb, p_ub, samples: int = 20):
    x = x.reshape((int(x.size // 4), 4))
    for idx in np.where(np.any(x[:, 0:2] > p_ub, axis=1))[0]:
        line = np.linspace(x_prev[idx, 0:2], x[idx, 0:2], samples)
        x[idx, 0:2] = line[np.min(np.where(np.any(line < p_ub[idx], axis=1))), :]
        x[idx, 2:].fill(0.0)
    for idx in np.where(np.any(x[:, 0:2] < p_lb, axis=1))[0]:
        line = np.linspace(x_prev[idx, 0:2], x[idx, 0:2], samples)
        x[idx, 0:2] = line[np.min(np.where(np.any(line > p_lb[idx], axis=1))), :]
        x[idx, 2:].fill(0.0)
    return x


class ParticleBaseEnv(_mag_core.MultiAgentEnv, abc.ABC):

    # objects to be instantiated
    def __init__(self, use_nb=True, dt=0.02):
        self._agents = {}  # type: typing.Dict[str, _agents.ParticleAgentType]
        self._static_entities = []  # type: typing.List[_entities.Entity]
        self._dynamic_entities = []  # type: typing.List[_entities.Entity]
        self._collision_entities = []  # type: typing.List[_entities.Entity]
        self._goals = []  # type: typing.List[_entities.Entity]
        self._cache_dist = np.zeros((self.nb_agents, self.nb_agents))
        self._cache_collisions = []  # type: typing.List[typing.Tuple[_entities.Entity, _entities.Entity]]
        self._collision_white_mask = np.ones_like(self._cache_dist)
        self._collision_threshold = self._cache_dist.copy()
        self._boundary_dist_min = dict()  # type: typing.Dict[str, float]
        self._ent_list = []
        self.nb_dyn_entities = 0
        self.nb_entities = 0
        self._use_nb = use_nb
        self._dt = dt
        self._x_cur = np.array([a.x for a in self.agent_list] +
                               [e.x for e in self.entities])
        self._pos_lb = np.array([], dtype=np.double)
        self._pos_ub = np.array([], dtype=np.double)
        self._rendering = None
        self.bounce_at_surfaces = True

    def render(self, mode: str = 'hlogituman', title="ParticlesWorld"):
        if self._rendering is None:
            self._rendering = _render.PyGameRendering(title=title)
        self._rendering.render(self._ent_list)

    def init_env(self):
        """Reset caches and static thresholds to increase update rate"""

        def ai2lb(ai):
            ag = self._idx2agent(ai)
            return ag.p_min + .5 * ag.size

        def ai2ub(ai):
            ag = self._idx2agent(ai)
            return ag.p_max - .5 * ag.size

        self.nb_dyn_entities = len(self._agents) + len(self._dynamic_entities)
        self.nb_entities = len(self._agents) + len(self.entities)
        self._cache_dist = np.zeros((self.nb_dyn_entities, self.nb_entities))
        self._collision_white_mask = np.ones_like(self._cache_dist)
        self._pos_lb = np.array(list(map(ai2lb, range(self.nb_agents))))
        self._pos_ub = np.array(np.array(list(map(ai2ub, range(self.nb_agents)))))
        e_sizes = np.array(list(map(lambda ai: self._idx2agent(ai).size, range(self.nb_agents))))
        if len(self.entities) > 0:
            e_sizes = np.r_[e_sizes, np.array([e.size for e in self.entities])]
        if len(self._dynamic_entities) > 0:
            self._pos_lb = np.r_[self._pos_lb,
                                 np.array([e.p_min + 0.5 * e.size for e in self._dynamic_entities])]
            self._pos_ub = np.r_[self._pos_ub,
                                 np.array([e.p_max - 0.5 * e.size for e in self._dynamic_entities])]

        self._collision_threshold = np.zeros_like(self._cache_dist)
        self._collision_threshold[0, 1:] = e_sizes[0] / 2.0 + e_sizes[1:] / 2.0
        for i in range(1, self.nb_dyn_entities):
            self._collision_threshold[i, :i] = e_sizes[i] / 2.0 + e_sizes[:i] / 2.0
            if i + 1 < self._collision_threshold.shape[1]:
                self._collision_threshold[i, i + 1:] = e_sizes[i] / 2.0 + e_sizes[i + 1:] / 2.0
        self._ent_list = self.agent_list + self.entities
        self.action_space = gym.spaces.Dict({ag: agent.action_space for ag, agent in self.agent_dict.items()})

    @property
    def entities(self) -> typing.List[_entities.Entity]:
        return self._static_entities + self._dynamic_entities + self._collision_entities

    @property
    def default_agents(self):
        return filter(lambda x: _agents.is_default_agent(x), self._agents.values())

    def _reset_state(self):
        def _ent_reset(x):
            x.sample_pos()

        valid = False
        while not valid:
            list(map(_ent_reset, self._dynamic_entities))
            list(map(_ent_reset, self._agents.values()))
            agents = [self._idx2agent(ai) for ai in range(self.nb_agents)]
            self._x_cur = np.array([a.x for a in agents] +
                                   [e.x for e in self.entities])
            agents = [self._idx2agent(ai) for ai in range(self.nb_agents)]
            s0 = np.array([a.x for a in agents] + [e.x for e in self.entities])

            self.calc_entity_distances(s0)
            if np.any(np.triu(self._cache_dist < self._collision_threshold)):
                pass
            else:
                valid = True
        return self.get_base_observation()

    def get_base_observation(self):
        return {k: ag.x for k, ag in self.agent_dict.items()}

    def calc_entity_distances(self, x, ii=0, ji=None, ij=None):
        r"""
        Calculate entity distance matrices from vector of positions or state, i.e.
        dimension of D x 2,3,4,5, 6), where `D` denotes the number of vectors and each vector defines x,y-position
        and optionally one of the following

        * orientation in yaw
        * velocity in xy
        * orientation plus velocity in xy
        * orientation plus velocity in xy and angular velocity

        by default all content of matrix ``cache_dist``, i.e. the spatial relation of all entities / agents is calculated.
        By setting ``ii``, ``ji`` and ``ji`` a sub-matrix can be chosen. By default only the distances of
        dynamic entities to all obstacles is calculated, i.e. :math:`N \times M`, where
        :math:`N:=N_\mathrm{ag} + N_\mathrm{ent, dyn}` and
        :math:`M:=N_\mathrm{ag} + N_\mathrm{ent, dyn} + N_\mathrm{ent, stat}`

        Args:
            x(np.ndarray):
            ii(int, optional): upper left limit of sub-matrix. Defaults to 0, i.e. (0,0)
            ji(int or None. Optionally sets the lower left limit. Defaults to None, i.e. (N, 0)
            ij(int or None. Optionally sets the upper right limit. Defaults to None, i.e. (0, N)
        """
        N = self.nb_dyn_entities
        M = self.nb_agents + len(self.entities)
        assert M >= N, "this environment is messed up"
        if ji is None:
            ji = N
        if ij is None:
            ij = M
        ii = max(0, ii)
        ij = min(ij, M)
        ji = min(ji, N)

        if self._use_nb:
            delta, dist = _nb_distance_calculation(x)
        else:
            delta, dist = _dist_calc(x)
        self._cache_dist[ii:ji, ii:ij] = dist[ii:ji, ii:ij]

    def calc_boundary_dist(self, x_new=None) -> dict:
        if x_new is None:
            x_new = self._x_cur
        d_min = np.amin(np.hstack((self._pos_ub[:self.nb_agents] - x_new[:self.nb_agents, :2],
                                   x_new[:self.nb_agents, :2] - self._pos_lb[:self.nb_agents])), axis=1)
        return {ag: d_min[int(agent)] for ag, agent in self._agents.items()}

    def _sim_world(self) -> None:
        # forward integration
        agents = [self._idx2agent(ai) for ai in range(self.nb_agents)]
        x_prev = self._x_cur.copy()
        x_new = self._x_cur.copy()
        for i, ag in enumerate(agents):
            x_new[i, :] = ag.f(x=ag.x, u=ag.u)
        for i, ent in enumerate(self._dynamic_entities):
            x_new[i, :] = ent.integrate()
        # state correction wrt constraints
        self._boundary_dist_min = self.calc_boundary_dist(x_new)
        if self.bounce_at_surfaces:
            x_new[:self.nb_dyn_entities] = _bounds_bounce(x_new[:self.nb_dyn_entities], p_lb=self._pos_lb,
                                                          p_ub=self._pos_ub)
        else:
            x_new[:self.nb_dyn_entities] = \
                _limit_to_bounds(x_new[:self.nb_dyn_entities], x_prev=x_prev, p_lb=self._pos_lb, p_ub=self._pos_ub)

        self._cache_dist = sci_spat.distance_matrix(x_new[:self.nb_dyn_entities, 0:2], x_new[:, :2])
        self._cache_collisions = np.where(np.triu(self._cache_dist * self._collision_white_mask <
                                                  self._collision_threshold[0:self.nb_dyn_entities, :]))
        for e1, e2 in zip(*self._cache_collisions):
            _entities.collide_entities(self._ent_list[e1], self._ent_list[e2],
                                       p1_new=x_new[e1, 0:2], p2_new=x_new[e2, 0:2])
            x_new[e1] = self._ent_list[e1].x.copy()
            x_new[e2] = self._ent_list[e2].x.copy()
        for i, ag in enumerate(agents):
            ag.update(x_new[i])
        for i, ent in enumerate(self._dynamic_entities):
            ent.update(x_new[i + len(agents)])
        self._x_cur = x_new.copy()

    @property
    def done(self) -> bool:
        """Optional done callback as a property. Returns False by default """
        return False

    def _close(self) -> None:
        pass

    def set_actions(self, joint_action: dict):
        for ag, action in joint_action.items():
            self._agents[ag].set_action(action)

    def reset(self) -> dict:
        return self._reset_state()

    def env_update(self) -> None:
        self._sim_world()

    def get_step_return(self) -> _mag_core.EnvFeedback:
        return _mag_core.EnvFeedback(
            observation=self.get_observation(),
            reward=self.get_rewards(),
            done=self.done,
            info=self.info
        )

    @property
    def info(self) -> dict:
        return dict(collisions=np.array(self._cache_collisions, dtype=np.int),
                    x_full=self._x_cur.copy())

    def multi_agent_step(self, action: typing.Dict[str, object]) -> _mag_core.EnvFeedback:
        out = super().multi_agent_step(action)
        self.post_step()
        return out

    def post_step(self):
        pass

    @abc.abstractmethod
    def get_observation(self) -> dict:
        raise NotImplementedError

    @abc.abstractmethod
    def get_rewards(self):
        raise NotImplementedError
