"""
Actual available environments
"""
import numpy as np
import scipy.optimize
import typing
import gym.spaces

import multi_agent_gym.spaces
import multi_agent_gym.core as _mag_core
import multi_agent_gym.utils as _mag_utils

from .agents import ParticleAgentType, CollectAgentType, instantiate_agent
from .entities import CircularEntity, Entity
from .numba_utils import update_mask
from .multi_agent_particle_env import ParticleBaseEnv

__all__ = ["TreasureCollect", "CooperativeCollection", "CooperativeNavigation"]


def get_boundary_cost(boundary_dist_min: dict, agent: ParticleAgentType, collision_cost, d_soft_scale=1.1,
                      shape_scale=2.0) -> float:
    d_soft = d_soft_scale * agent.size
    if boundary_dist_min[str(agent)] < d_soft:
        return collision_cost * \
               np.clip((boundary_dist_min[str(agent)] - d_soft) ** shape_scale / d_soft ** shape_scale, 0., 1.0)
    return 0.0


def get_collision_cost(agent: ParticleAgentType, collision_cost: float, collision_cache):
    if int(agent) in np.unique(collision_cache):
        return collision_cost
    return 0.0


def get_goals(gi):
    gi = int(gi % 6)
    return np.array([[-0.3, 0.4],
                     [0.4, 0.25],
                     [-0.15, -0.65],
                     [0.35, 0.5],
                     [-0.55, -0.15],
                     [0.65, -0.35],
                     ])[gi]


class TreasureCollect(ParticleBaseEnv, _mag_core.GaolMultiAgentEnv):
    _COL = 0
    _DROP = 1

    def __init__(self, nb_agents=8, nb_visible=7, nb_collectors=6, sparse_rewards=False,
                 pickup_reward=1.0, drop_reward=1.0, drop_penalty=1.0, collision_cost=1.0,
                 discrete_action=False, mlp_action=False, use_nb=True, dt=0.02):
        self._treasure_targets = []
        self._treasure_locations = []
        self._cur_ag_types = []
        self._collision_cost = collision_cost
        self._pickup_reward = pickup_reward
        self._drop_reward = drop_reward
        self._drop_penalty = drop_penalty
        ParticleBaseEnv.__init__(self, use_nb=use_nb, dt=dt)
        _mag_core.GaolMultiAgentEnv.__init__(self)
        self._nb_collectors = nb_collectors
        self._sparse_rewards = sparse_rewards
        self._agent_color = np.r_[0.85 * np.ones(3), 1.0]
        self._treasure_colors = _mag_utils.get_rgba_colors(nb_agents - nb_collectors)
        self._cache_updates = dict(pickup=[], drop=[], false_drop=[])
        for ai in range(nb_agents):
            size = 0.05 if ai < nb_collectors else 0.2
            mass = 1.0 if ai < nb_collectors else 2.25
            ag = instantiate_agent(discrete=discrete_action, mlp=mlp_action, collect=True,
                                   i=ai, dt=self._dt, mass=mass, max_speed=1.0, size=size, u_max=1.5)
            if ai < nb_collectors:
                ag.rgba_color = np.array([0.85, 0.85, 0.85, 1.0])
                ag.team_id = self._COL
            else:
                ag.rgba_color = self._treasure_colors[ai - nb_collectors]
                ag.type_id = ai - nb_collectors + 1
                ag.team_id = self._DROP
            self.add_agent(ag)
        for col in range(self.nb_agents - self._nb_collectors + 1):
            ent = CircularEntity(mass=0.0, max_speed=0.0, size=0.1)
            self._treasure_targets.append(ent)
        self._nb_visible_agents = min(nb_visible, self.nb_agents - 1)
        self._nb_visible_ents = min(nb_visible, nb_agents - nb_collectors)
        self._drop_distances = np.zeros((nb_agents - nb_collectors, nb_agents - nb_collectors))
        self.init_env()
        self.set_spaces()
        assert all([hasattr(ag, "type_id") for ag in self._agents.values()])
        self.reset()

    @property
    def entities(self) -> typing.List[Entity]:
        return self._dynamic_entities + self._static_entities + self._treasure_targets + self._treasure_locations

    def _sim_world(self) -> None:
        ag_types = np.array([ag.type_id for ag in self.agent_list])
        update_mask(self._collision_white_mask[:self.nb_agents, :self.nb_agents], ag_types)
        super()._sim_world()
        for ai, ag in enumerate(self.agent_list):
            if ag.team_id != self._COL:
                continue
            if ag.type_id[-1] == 0:
                if np.any(self._cache_dist[ai, self.nb_agents:] < 0.5 * self._collision_threshold[ai, self.nb_agents:]):
                    ei = np.argsort(self._cache_dist[ai, self.nb_agents:])[0]
                    self._cache_updates['pickup'].append((ag, self.entities[ei]))
                    ag.pickup(ei)
            else:
                for oaj in np.where(self._cache_dist[ai, self._nb_collectors:self.nb_agents] <
                                    self._collision_threshold[ai, self._nb_collectors:self.nb_agents])[0]:
                    if ag_types[self._nb_collectors + oaj, 1] == ag_types[ai, 1]:
                        self._cache_updates['drop'].append((ag, self.agent_list[self._nb_collectors + oaj]))
                    else:
                        self._cache_updates['false_drop'].append((ag, self.agent_list[self._nb_collectors + oaj]))

    def set_spaces(self):
        """
        Agent observation space

        Dictionary containing:

        * ``x``: internal agent state as a Box for  position and velocity
        * ``y_d``: discrete observation as agent and entity encodings
        * ``y_d``: continuous agent observations, i.e. distance to nearby agents + velocity of nearby agents;
            as well as distance to nearby entities

        """
        p_min, p_max = self.agent_list[0].p_min, self.agent_list[0].p_max
        nb_treasures = self.nb_agents - self._nb_collectors
        obs_space = dict()
        for k, ag in self.agent_dict.items():
            state_space = ag.state_space
            interaction_observation_space = gym.spaces.Dict(
                {str(oag):
                    gym.spaces.Dict({
                        "y": gym.spaces.Box(oag.state_space.low, oag.state_space.high, dtype=np.double),
                        "encoding": gym.spaces.MultiDiscrete([2, nb_treasures])})
                    for oag in self._agents.values() if oag != ag})
            len_encodings = nb_treasures if ag.team_id == self._DROP else nb_treasures + 1
            env_observation_space = gym.spaces.Dict({
                "y": gym.spaces.Box(np.tile(p_min, self._nb_visible_ents),
                                    np.tile(p_max, self._nb_visible_ents), dtype=np.double),
                "encoding": gym.spaces.MultiDiscrete([nb_treasures] * len_encodings)})
            obs_space[k] = multi_agent_gym.spaces.AgentObservation(
                state_space=state_space, env_observation_space=env_observation_space,
                interaction_observation_space=interaction_observation_space)
        self._set_observation_space(obs_space)

    def init_env(self):
        super().init_env()
        self._collision_white_mask[self._nb_collectors:self.nb_agents, self._nb_collectors:self.nb_agents].fill(1.0)
        for i, e in enumerate(self.entities):
            if not e.can_collide:
                if e.can_move:
                    self._collision_white_mask[self.nb_agents + i, :].fill(np.inf)
                self._collision_white_mask[:, self.nb_agents + i].fill(np.inf)

    def color2id(self, e: Entity) -> int:
        for i, color in enumerate(self._treasure_colors):
            if np.all(color == e.rgba_color):
                return i + 1
        return -1

    def get_observation(self):
        """
        Observation for each agent:

        * state (position and velocity) 2 * dim_p = 4
        * own holding (collectors only) nb_deposits = 2
        * delta pos to and velocity of nb_visible other agents: nb_visible * 4 = 28
        * encoding of nb_visible other agents: nb_visible * nb_deposits * 2 = 28
        * delta pos to objects: nb_visible_treasures * 2 = 12
        * color encoding of objects: nb_visible_treasures * nb_deposits = 12

        Note:
            as picked up objects are virtually set into infinity to prevent them being listed as nearby
            objects for other agents. Nonetheless, the distance is actually 0.0 as an agent is currently
            carrying said object further, keeping the infinity values would require to mask data within
            outer RL algorithms in order to prevent normalizing by infinity, and thus leading to
            nan-observation/action problems.

        Returns:
            OrderedDict: Dictionary of individual agent observations.
        """
        entities = self.entities
        ent_encoding = list(map(self.color2id, self.entities))

        try:
            obs = self.observation_space.sample()
        except AttributeError:
            self.set_spaces()
            obs = self.observation_space.sample()

        for k, ag in self.agent_dict.items():
            obs[k]['x'] = ag.x.copy()
            for oag in obs[k]['y_ag']:
                obs[k]['y_ag'][oag]['y'][:2] = self._agents[oag].pos - ag.pos
                obs[k]['y_ag'][oag]['y'][2:] = self._agents[oag].x[2:]
                obs[k]['y_ag'][oag]['encoding'][:] = self._agents[oag].type_id
            closest_ents = np.argsort(self._cache_dist[int(ag), self.nb_agents:])
            obs[k]['y_env']['y'] = np.hstack([entities[ei].pos - ag.pos for ei in closest_ents][:self._nb_visible_ents])
            if ag.team_id == self._COL:
                obs[k]['y_env']['encoding'][:-1] = np.array(ent_encoding)[closest_ents][:self._nb_visible_ents]
                obs[k]['y_env']['encoding'][-1] = ag.type_id[-1]
            else:
                obs[k]['y_env']['encoding'][:] = np.array(ent_encoding)[closest_ents][:self._nb_visible_ents]
        return obs

    def reset(self) -> dict:
        for ent in self._treasure_targets:
            ent.rgba_color = self._treasure_colors[np.random.randint(self.nb_agents - self._nb_collectors)]
            ent.sample_pos()
        self._reset_state()
        for a_name, ag in self._agents.items():
            if ag.team_id == self._COL:
                ag.rgba_color = self._agent_color
            else:
                ag.rgba_color = self._treasure_colors[int(ag) - self._nb_collectors]
        return self.get_observation()

    def get_rewards(self) -> dict:
        team_reward = 0
        for _ in self._cache_updates['pickup']:
            team_reward += self._pickup_reward
        for _ in self._cache_updates['drop']:
            team_reward += self._drop_reward
        for _ in self._cache_updates['false_drop']:
            team_reward -= self._drop_penalty
        return {
            k: team_reward - self.agent_cost(ag) for k, ag in self.agent_dict.items()
        }

    def post_step(self):
        for k in ('drop', 'false_drop'):
            while self._cache_updates[k]:
                ag, oag = self._cache_updates[k].pop()
                ag.rgba_color = self._agent_color
                ei = ag.drop()
                self.entities[ei].sample_pos()
                self.entities[ei].rgba_color = \
                    self._treasure_colors[np.random.randint(self.nb_agents - self._nb_collectors)]
        while self._cache_updates['pickup']:
            ag, ent = self._cache_updates['pickup'].pop()
            col = ag.rgba_color + ent.rgba_color
            col[-1] = 1.0
            try:
                col[:3] /= np.linalg.norm(col[:3])
            except ZeroDivisionError:
                pass
            ag.rgba_color = col
            ag.pickup(self.color2id(ent))
            ent.make_invisible()

    def agent_cost(self, agent: CollectAgentType):
        cost = get_collision_cost(agent, self._collision_cost, self._cache_collisions) + \
               get_boundary_cost(self._boundary_dist_min, agent, self._collision_cost)

        if cost == 0.0:
            if agent.type_id[-1] == 0:
                agent.rgba_color = self._agent_color
            else:
                agent.rgba_color = self._treasure_colors[agent.type_id[-1] - 1]
        else:
            agent.rgba_color = np.r_[np.clip(cost / self._collision_cost, 0., 1.0), 0., 0., 1.]
        return cost


class CooperativeNavigation(ParticleBaseEnv, _mag_core.MultiAgentEnv):

    def __init__(self, nb_agents=3, nb_goals=3, u_max=1.5,
                 sparse_rewards=False, discrete_action=False, mlp_action=True,
                 static_goals: bool = False,
                 collision_cost=1, step_cost=1, use_nb=True, dt=0.02):
        self._static_goals = static_goals
        self._goal_locations = []
        self._collected_goals = set()
        self._ignore_goals = set()
        colors = _mag_utils.get_rgba_colors(nb_goals + 1)
        self._agent_color = colors[0]
        self._goal_colors = colors[1:]
        self._step_cost = step_cost
        self._collision_cost = collision_cost
        self.sparse_rewards = sparse_rewards
        for col in self._goal_colors:
            ent = CircularEntity(mass=0.0, max_speed=0.0, size=0.05)
            ent.rgba_color = col
            self._goal_locations.append(ent)
        self._goal_threshold = 0.02
        ParticleBaseEnv.__init__(self, use_nb=use_nb, dt=dt)
        _mag_core.MultiAgentEnv.__init__(self)
        for ai in range(nb_agents):
            ag = instantiate_agent(collect=False, discrete=discrete_action, mlp=mlp_action,
                                   i=ai, dt=self._dt, u_max=u_max)
            ag.rgba_color = self._agent_color
            self.add_agent(ag)

        self.set_spaces()
        self.init_env()
        self.reset()

    @property
    def entities(self) -> typing.List[Entity]:
        return self._dynamic_entities + self._static_entities + self._goal_locations

    def set_spaces(self):
        p_min, p_max = self.agent_list[0].p_min, self.agent_list[0].p_max
        obs_space = dict()
        for k, ag in self.agent_dict.items():
            obs_space[k] = multi_agent_gym.spaces.AgentObservation(
                state_space=ag.state_space,
                interaction_observation_space=gym.spaces.Dict(
                    {str(oag): gym.spaces.Box(oag.state_space.low, oag.state_space.high, dtype=np.double)
                     for oag in self._agents.values() if oag != ag}),
                env_observation_space=gym.spaces.Box(np.tile(p_min, len(self._goal_locations)),
                                                     np.tile(p_max, len(self._goal_locations)), dtype=np.double))
        self._set_observation_space(obs_space)

    def get_observation(self):
        """
        Observation for each agent:

        * state (position and velocity) 2 * dim_p = 4
        * own holding (collectors only) nb_deposits = 2
        * delta pos to and velocity of nb_visible other agents: nb_visible * 4 = 28
        * encoding of nb_visible other agents: nb_visible * nb_deposits * 2 = 28
        * delta pos to objects: nb_visible_treasures * 2 = 12
        * color encoding of objects: nb_visible_treasures * nb_deposits = 12

        Note:
            as picked up objects are virtually set into infinity to prevent them being listed as nearby
            objects for other agents. Nonetheless, the distance is actually 0.0 as an agent is currently
            carrying said object further, keeping the infinity values would require to mask data within
            outer RL algorithms in order to prevent normalizing by infinity, and thus leading to
            nan-observation/action problems.

        Returns:
            OrderedDict: Dictionary of individual agent observations.
        """

        def get_ent_obs():
            e_obs = []
            for ei in closest_ents:
                e_obs.append(
                    entities[ei].pos - ag.pos if self._cache_dist[int(ag), self.nb_agents + ei] != 0.0
                    else np.zeros(2)
                )
            return e_obs

        entities = self.entities
        ent_encoding = []
        for e in self.entities:
            for ag in self.agent_list:
                if np.all(ag.rgba_color == e.rgba_color):
                    ent_encoding.append(ag.type_id)
                    break
        ai_x = np.array([[a.x] for a in self.agent_list])
        if self._cache_dist is None:
            agents = [self._idx2agent(ai) for ai in range(self.nb_agents)]
            self.calc_entity_distances(np.array([a.x for a in agents] +
                                                [e.x for e in entities]), ji=self.nb_dyn_entities)
        try:
            obs = self.observation_space.sample()
        except AttributeError:
            self.set_spaces()
            obs = self.observation_space.sample()
        for k, ag in self.agent_dict.items():
            obs[k]['x'] = ag.x.copy()
            for oai, oag in self._agents.items():
                if oag == ag:
                    continue
                obs[k]['y_ag'][str(oag)] = np.r_[oag.pos - ag.pos, oag.vel]
            closest_ents = np.argsort(self._cache_dist[int(ag), self.nb_agents:])
            obs[k]['y_env'] = np.hstack(get_ent_obs())
        return obs

    def reset(self) -> dict:
        for gi, ent in enumerate(self._goal_locations):
            ent.rgba_color = self._goal_colors[gi]
            if self._static_goals:
                x_new = ent.x.copy()
                x_new[:-2] = get_goals(gi)
                ent.update(x_new)
            else:
                ent.sample_pos()
        self._reset_state()
        for a_name, ag in self._agents.items():
            ag.rgba_color = self._agent_color
        self._collected_goals = set()
        self._ignore_goals = set()
        self._collision_white_mask[:, self.nb_agents:self.nb_agents + len(self._goal_locations)].fill(np.inf)
        return self.get_observation()

    def get_rewards(self) -> dict:
        team_reward = 0
        distances = np.zeros((self.nb_agents, len(self._goal_locations)))
        for ei, e in enumerate(self._goal_locations):
            if e in self._collected_goals:
                continue
            else:
                distances[:, ei] += self._cache_dist[:self.nb_agents, self.nb_agents + ei]

        extra_goals = len(self._goal_locations) - len(self._collected_goals) - self.nb_agents
        if extra_goals > 0:
            distances = np.vstack((distances, np.repeat(distances[-1:], extra_goals, 0)))
        # team_reward -= distances[scipy.optimize.linear_sum_assignment(distances)].sum()
        if self.sparse_rewards:
            team_reward = -self._step_cost * (np.min(distances, axis=0) > self._goal_threshold).sum()
        else:
            team_reward -= np.min(distances, axis=0).sum()
        return {
            k: team_reward - self.agent_cost(ag) for k, ag in self.agent_dict.items()
        }

    def agent_cost(self, agent: ParticleAgentType):
        cost = get_collision_cost(agent, self._collision_cost, self._cache_collisions) + \
               get_boundary_cost(self._boundary_dist_min, agent, self._collision_cost)
        if cost == 0.0:
            agent.rgba_color = self._agent_color
        else:
            agent.rgba_color = np.r_[np.clip(cost / self._collision_cost, 0., 1.0), 0., 0., 1.]
        return cost


class CooperativeCollection(CooperativeNavigation):

    def __init__(self, **env_args):
        super(CooperativeCollection, self).__init__(**env_args)
        self._cache_goals = []

    @property
    def done(self):
        return len(self._collected_goals) == len(self._goal_locations)

    def get_rewards(self) -> dict:
        while self._cache_goals:
            ag, ent = self._cache_goals.pop()
            if ent not in self._collected_goals:
                ent.rgba_color = np.r_[ent.rgba_color[:3], 0.1]
                self._collected_goals.add(ent)
        return super().get_rewards()

    def _sim_world(self) -> None:
        super()._sim_world()
        for ag, gi in zip(*np.where(
                self._cache_dist[:self.nb_agents, self.nb_agents:] <= 0.7 *
                self._collision_threshold[:self.nb_agents, self.nb_agents:])):
            g = self.entities[gi]
            self._ignore_goals.add(gi)
            if g in self._collected_goals:
                continue
            else:
                self._cache_goals.append((ag, g))
        for gi in self._ignore_goals:
            self._cache_dist[:, self.nb_agents + gi] = 0.0
