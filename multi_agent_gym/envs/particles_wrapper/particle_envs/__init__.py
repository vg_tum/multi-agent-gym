"""
Continuous Particle Domain Scenarios
-------------------------------------------

All scenarios contain the following attributes

Action space:

* 0 -> force x
* 1 -> force y

State per agent:

* pos(x,y)
* vel(x,y)

World state:

* concatenated agent state vector [s_a_0, ... s_a_N]
"""

