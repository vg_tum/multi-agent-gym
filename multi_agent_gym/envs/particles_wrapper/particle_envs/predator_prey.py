r"""
Multi-Agent Gym Implementation fo Predator-Prey from [Loewe2017]_
----------------------------------------------------------------------------

Agents:

* N0 adversary (red),
* N1 good agents (green),


Entities:

* O large, bulky objects

Good agents (green) are faster and want to avoid being hit by adversaries (red).
Adversaries are slower and want to hit good agents.
Obstacles (large black circles) block the way.
"""
import numpy as np
from collections import OrderedDict
from gym.spaces import Box
from multi_agent_gym.envs.particles_wrapper.multi_agent_particle_env import ParticleBaseEnv
from archived.particles.multiagent.core import Landmark
from multi_agent_gym.envs.particles_wrapper.utils.misc import is_collision, boundary_cost


class PredatorPrey(ParticleBaseEnv):

    def __init__(self, nb_agents=4, nb_obstacles=2, nb_adversaries=3, sparse_reward=False, **env_args):
        super(PredatorPrey, self).__init__(nb_agents, dim_c=0, **env_args)
        if not self._decentral:
            raise RuntimeError("Competitive scenarios are not intended to be run centralized")
        self.sparse_rewards = sparse_reward
        self.world.landmarks = [Landmark() for i in range(nb_obstacles)]
        for i, agent in enumerate(self.agents):
            agent.collide = True
            agent.silent = True
            if i < nb_adversaries:
                agent.team_id = 1
                agent.size = 0.075
                agent.u_range = (-3.0, 3.0)
                agent.max_speed = 1.0
            else:
                agent.size = 0.05
                agent.u_range = (-4.0, 4.0)
                agent.max_speed = 1.3
        for i, landmark in enumerate(self.world.landmarks):
            landmark._name = 'landmark{:d}'.format(i)
            landmark.collide = True
            landmark.movable = False
            landmark.size = 0.2
        self.set_spaces()
        self.cur_goal = None
        self.reset()

    def set_spaces(self):
        super(PredatorPrey, self).set_spaces()
        decentralized_observation_space(self.agents, self.world.landmarks, False)
        len_v_obs = self.world.dim_p * (self.nb_agents - 1)
        for agent in self.agents:
            try:
                v_max = [[o.v_max] * self.world.dim_p for o in filter(lambda x: x != agent, self.agents)]
            except TypeError:
                v_max = [[np.inf] * len_v_obs]
            lb_obs = agent.obs_space.low.tolist() + -1.0 * v_max
            ub_obs = agent.obs_space.high.tolist() + v_max
            agent.ent_idx[1] += len_v_obs
            agent.obs_space = Box(np.array(lb_obs), np.array(ub_obs), dtype=np.float32)

    def reset(self):
        for agent in self.agents:
            if agent.team_id:
                agent.color = np.array([0.85, 0.35, 0.35])
            else:
                agent.color = np.array([0.35, 0.35, 0.85])
        for i, g in enumerate(self.world.landmarks):
            g.color = np.array([0.25, 0.25, 0.25])
        self._reset_state()
        return self.get_observation()

    def get_observation(self):
        obs1 = agent_observations(self.agents, self.world.landmarks, False)
        for agent in self.default_agents:
            other_vel = [o.state.p_vel for o in filter(lambda x: x != agent, self.agents)]
            obs1[agent.name] = np.concatenate([obs1[agent.name], other_vel])
        return obs1

    def get_rewards(self) -> OrderedDict:
        out = OrderedDict()
        col_counter = 0
        for ag in filter(lambda x: x.team_id == 0, self.agents):
            for _ in filter(lambda x: x.team_id > 0 and is_collision(ag, x), self.agents):
                col_counter += 1
        for ag in self.agents:
            rew, cost = 0.0, 0.0
            if ag.team_id == 0:
                cost = col_counter * 10.0 + boundary_cost(ag)
                if not self.sparse_rewards:
                    for o_a in filter(lambda x: x.team_id > 0, self.agents):
                        rew += 0.1 * np.linalg.norm(ag.state.p_pos - o_a.state.p_pos, ord=2)
            else:
                rew = col_counter * 10.0
                if not self.sparse_rewards:
                    for o_a in filter(lambda x: x.team_id > 0, self.agents):
                        cost += 0.1 * np.linalg.norm(ag.state.p_pos - o_a.state.p_pos, ord=2)
            out[ag.name] = rew - cost
        return out
