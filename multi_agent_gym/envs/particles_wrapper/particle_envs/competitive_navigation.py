r"""
agent spaces broken / incorrect!

Multi-Agent Gym Implementation fo Physical Deception from [Loewe2017]_ :py:class:`~PhysicalDeception`
-------------------------------------------------------------------------------------------------------

Agents:

* N0 adversary (red),
* N1 good agents (green),

usually N0=1 and N0+N1 =3

Entities:

* N1 landmarks. Defaulting to N1=2.

All agents observe position of landmarks and other agents.
One landmark is the ‘target landmark’ (colored green).
Good agents rewarded based on how close one of them is to the target landmark,
but negatively rewarded if the adversary is close to target landmark.

World state (additional to default):

* pos_obstacles(x,y)

Observation (all agents):

* dist oA1, oA2, ..., oAN     distance vector to other agent(s)
* dist_e2_x, dist_e2_y,       distance vector to all entities

Additional Observations (green agents):

* g_des - p                   position vector to desired goal

Rewards:

Good agents rewarded based on how close one of them is to the target landmark,
but negatively rewarded if the adversary is close to target landmark.

Adversary is rewarded based on how close it is to the target, but it doesn’t know which landmark is the target landmark.

So good agents have to learn to ‘split up’ and cover all landmarks to deceive the adversary

Multi-Agent Gym Implementation for ``Keep Away`` from [Loewe2017]_ :py:class:`~KeepAway`
----------------------------------------------------------------------------------------

Agents:

* N0 adversary (red),
* N0 good agents (green),


Entities:

* N0 landmarks. Defaulting to N1=2.

default N0=1

N0 agents,adversaries and landmarks.
Agent are rewarded based on distance to landmarks.
Adversaries are rewarded if close to the landmark, and if the agents are far from the landmark.
So the adversary learns to push agent away from the landmark.
"""
import numpy as np
from collections import OrderedDict
from gym.spaces import Box
from multi_agent_gym.envs.particles_wrapper.multi_agent_particle_env import ParticleBaseEnv
from archived.particles.multiagent.core import Landmark


class PhysicalDeception(ParticleBaseEnv):

    def __init__(self, nb_agents=3, nb_goals=2, nb_adversaries=1, sparse_reward=True, **env_args):
        super(PhysicalDeception, self).__init__(nb_agents, dim_c=0, **env_args)
        assert nb_goals > 1, "physical deception is useless with a single landmark"
        self.world.landmarks = [Landmark() for i in range(nb_goals)]
        self.sparse_rewards = sparse_reward
        for i, agent in enumerate(self.agents):
            agent.collide = False
            agent.silent = True
            if i < nb_adversaries:
                agent.team_id = 1
            agent.size = 0.15
        for i, landmark in enumerate(self.world.landmarks):
            landmark._name = 'landmark{:d}'.format(i)
            landmark.collide = False
            landmark.movable = False
            landmark.size = 0.08
        if not self._decentral:
            raise RuntimeError("Competitive scenarios are not intended to be run centralized")
        self.set_spaces()
        self.cur_goal = None
        self.reset()

    @property
    def adversaries(self):
        return filter(lambda x: x.team_id == 1, self.agents)

    def set_spaces(self):
        super(PhysicalDeception, self).set_spaces()
        decentralized_observation_space(self.agents, self.world.landmarks, False)
        for agent in self.default_agents:
            new_limit = agent.obs_space.low.tolist() + [np.inf] * self.world.dim_p
            agent.ent_idx[1] += self.world.dim_p
            agent.obs_space = Box(-np.array(new_limit), np.array(new_limit), dtype=np.float32)

    def reset(self):
        goal = np.random.randint(len(self.world.landmarks))
        self.cur_goal = self.world.landmarks[goal]
        for agent in self.agents:
            if agent.team_id:
                agent.color = np.array([0.85, 0.35, 0.35])
            else:
                agent.color = np.array([0.35, 0.35, 0.85])
                agent.goal_a = self.cur_goal
        for i, g in enumerate(self.world.landmarks):
            if i == goal:
                g.color = np.array([0.15, 0.65, 0.15])
            else:
                g.color = np.array([0.25, 0.25, 0.25])
        self._reset_state()
        return self.get_observation()

    def get_observation(self):
        obs1 = agent_observations(self.agents, self.world.landmarks, False)
        for agent in self.default_agents:
            obs1[agent.name] = np.concatenate([obs1[agent.name], self.cur_goal.state.p_pos - agent.state.p_pos])
        return obs1

    def get_rewards(self) -> OrderedDict:
        adv_distances = []
        ag_distances = []
        for ag in self.agents:
            if ag.team_id > 0:
                adv_distances.append(np.linalg.norm(ag.state.p_pos - self.cur_goal.state.p_pos))
            else:
                ag_distances.append(np.linalg.norm(ag.state.p_pos - self.cur_goal.state.p_pos))
        # Warning: assumes identical agent sizes / team)
        if self.sparse_rewards:
            adv_rew = 5.0 * np.sum(np.array(adv_distances) < (self.cur_goal.size + self.agents[0].size) * 0.95)
            ag_rew = 0.0
            if min(ag_distances) < (self.cur_goal.size + self.agents[0].size) * 0.95:
                ag_rew = 5.0
        else:
            adv_rew = -sum(adv_distances)
            ag_rew = -min(ag_distances)
        ag_rew -= adv_rew
        out = OrderedDict()
        for ag in self.agents:
            out[ag.name] = adv_rew if ag.team_id else ag_rew
        return out


class KeepAway(PhysicalDeception):

    def __init__(self, nb_agents=2, nb_goals=2, **env_args):
        nb_adversaries = int(nb_agents / 2)
        super(KeepAway, self).__init__(nb_agents, nb_goals=nb_goals, nb_adversaries=nb_adversaries,
                                       sparse_reward=False, **env_args)
        for i, agent in enumerate(self.agents):
            agent.collide = True

    def set_spaces(self):
        super(PhysicalDeception, self).set_spaces()
        decentralized_observation_space(self.agents, self.world.landmarks, False)
        color_obs_len = 3 * self.nb_agents
        for agent in self.default_agents:
            lb_obs = agent.obs_space.low.tolist() + [0.0] * color_obs_len
            ub_obs = agent.obs_space.high.tolist() + [1.0] * color_obs_len
            agent.ent_idx[1] += color_obs_len
            agent.obs_space = Box(np.array(lb_obs), np.array(ub_obs), dtype=np.float32)

    def get_observation(self):
        obs1 = agent_observations(self.agents, self.world.landmarks, False)
        for agent in self.default_agents:
            other_colors = [o.color for o in filter(lambda x: x!=agent, self.agents)]
            obs1[agent.name] = np.concatenate([obs1[agent.name], agent.color, other_colors])
        return obs1

    def get_rewards(self) -> OrderedDict:
        ag_distances = []
        for ag in filter(lambda x: x.team_id == 0, self.agents):
            ag_distances.append(np.linalg.norm(ag.state.p_pos - self.cur_goal.state.p_pos, ord=2))
        # Warning: assumes identical agent sizes / team)
        out = OrderedDict()
        for ag in self.agents:
            if ag.team_id:
                out[ag.name] = min(ag_distances) - np.linalg.norm(ag.state.p_pos - self.cur_goal.state.p_pos, ord=2)
            else:
                out[ag.name] = -np.linalg.norm(ag.state.p_pos - self.cur_goal.state.p_pos, ord=2)
        return out
