import abc
import typing
from dataclasses import dataclass

import matplotlib.pyplot as plt
import numpy as np
from gym.spaces import Box

__all__ = ["Circle", "Triangle", "Rectangle", "RenderOptions",
           "Entity", "CircularEntity",
           "ParticleEntityDynamics", "PointMassEntityDynamics",
           "ShapeType", "DynamicsTypes"
           ]


@dataclass(frozen=True)
class Circle:
    size: float


@dataclass(frozen=True)
class Rectangle:
    width: float
    height: float


@dataclass(frozen=True)
class Triangle:
    corners: np.ndarray  # (3, 2)


ShapeType = typing.Union[Rectangle, Circle, Triangle]


@dataclass(frozen=True)
class RenderOptions:
    color: np.ndarray = np.zeros(3)
    alpha: float = 1.0
    invisible: bool = False


@dataclass(frozen=True)
class ParticleEntityDynamics:
    mass: float
    shape: ShapeType
    max_speed: float = 0.0
    p_min: np.ndarray = np.array([-1.0, -1.0])
    p_max: np.ndarray = np.array([1.0, 1.0])

    @property
    def state_space(self):
        return Box(np.r_[self.p_min + self.shape.size / 2.0, -self.max_speed, -self.max_speed],
                   np.r_[self.p_max - self.shape.size / 2.0, self.max_speed, self.max_speed], dtype=np.float64)


@dataclass(frozen=True)
class PointMassEntityDynamics(ParticleEntityDynamics):
    A: np.ndarray = np.eye(4)
    B: np.ndarray = np.zeros((4, 2))

    @classmethod
    def from_dt(cls, mass, dt, size, max_speed, p_min, p_max, damping=0.8):
        assert dt > 0.0, "time rate is not positive"
        assert damping <= 1.0, "only stable systems are supported"
        A = np.eye(4)
        A[0, 2] = dt
        A[1, 3] = dt
        A[2, 2] *= damping
        A[3, 3] *= damping
        B = np.zeros((4, 2))
        try:
            B[2, 0] = 1.0 / mass
            B[3, 1] = 1.0 / mass
        except ZeroDivisionError:
            raise RuntimeError(f"mass cannot be zero for point mass entity")
        return cls(mass=mass, shape=Circle(size=size), max_speed=max_speed,
                   p_min=p_min, p_max=p_max,
                   A=A, B=B)


@dataclass(frozen=True)
class DubinsCarEntityDynamics(ParticleEntityDynamics):
    rot_max_speed: float = 0.1

    @property
    def state_space(self):
        return Box(np.r_[self.p_min, -np.pi, -self.max_speed, -self.rot_max_speed],
                   np.r_[self.p_max, np.pi, self.max_speed, self.rot_max_speed], dtype=np.float64)


DynamicsTypes = typing.Union[ParticleEntityDynamics, PointMassEntityDynamics, DubinsCarEntityDynamics]


class Entity(abc.ABC):
    _static: DynamicsTypes
    _render = RenderOptions
    _patch_object = None

    def __init__(self):
        self._p = np.zeros(2)
        self._v = np.zeros(2)
        self.elasticity = 0.9

    def update(self, x_new):
        x_new = self.clip(x_new)
        self._p = np.copy(x_new[..., 0:2])
        self._v = np.copy(x_new[..., 2:4])

    @property
    def can_collide(self):
        return np.isfinite(self._static.mass) and self._static.mass != 0.0

    @property
    def can_move(self):
        return self._static.max_speed > 0.0

    @property
    def pos(self):
        return self._p

    @pos.setter
    def pos(self, value):
        self._p = np.clip(value,
                          self._static.state_space.low[0:2],
                          self._static.state_space.high[0:2])

    def make_invisible(self):
        try:
            self._render = RenderOptions(color=self._render.color,
                                         alpha=self._render.alpha,
                                         invisible=True)
        except AttributeError:
            pass

    @property
    def rgba_color(self):
        try:
            if self._render.invisible:
                return np.zeros(4)
            return np.r_[self._render.color, self._render.alpha]
        except AttributeError:
            default = RenderOptions()
            return np.r_[default.color, default.alpha]

    @rgba_color.setter
    def rgba_color(self, value):

        try:
            color = value[:3]
        except IndexError:
            color = np.zeros(3)
        try:
            alpha = value[3]
        except IndexError:
            alpha = 1.0
        try:
            self._render = RenderOptions(color=color, alpha=alpha,
                                         invisible=alpha <= 0.0)
        except AttributeError:
            self._render = RenderOptions(color=value, alpha=alpha, invisible=False)

    @property
    def size(self):
        try:
            return self._static.shape.size
        except AttributeError:
            RuntimeWarning(f"entity {self} is not a circular object")
            return 0.0

    def integrate(self):
        return self._static.A @ self.x + self._static.B @ np.zeros(2)

    def clip(self, x):
        return np.clip(x,
                       self._static.state_space.low,
                       self._static.state_space.high)

    @property
    def mass(self):
        return self._static.mass

    @property
    def speed(self):
        return np.hypot(*self._v)

    @property
    def shape(self):
        return self._static.shape

    @property
    def vel(self):
        return self._v

    @property
    def x(self):
        """internal agent state"""
        return np.r_[self._p, self._v]

    def sample_pos(self):
        x_new = self._static.state_space.sample()
        x_new[-2:] *= 0.0
        self.update(x_new)

    @property
    def p_min(self):
        return self._static.p_min

    @property
    def p_max(self):
        return self._static.p_max

    def _add_patch(self, ax=None):
        if self._patch_object is None:
            return
        if ax is None:
            ax = plt.gca()
        ax.add_artist(self._patch_object)

    @abc.abstractmethod
    def update_patch(self, ax=None):
        """
        Add / update current object as patch to a matplotlib axis

        Args:
            ax:
        """

    @property
    def angle_speed_velocity(self) -> np.ndarray:
        return np.array(vel2ang_speed(self._v))


class Wall(Entity):

    def __init__(self, width, height, angle=0.0, p_min=(-1.0, -1.0), p_max=(1.0, 1.0)):
        super(Wall, self).__init__()
        self._angle = angle
        self._static = ParticleEntityDynamics(mass=np.inf, shape=Rectangle(width=width, height=height),
                                              max_speed=0.0, p_min=p_min, p_max=p_max)

    def update_patch(self, ax=None):
        if self._patch_object is not None:
            if self._render.invisible:
                self._patch_object.set_alpha(1.0)
        elif not self._render.invisible:
            self._patch_object = plt.Rectangle((self._p[0], self._p[1]),
                                               width=self._static.shape.width,
                                               height=self._static.shape.height,
                                               color=self._render.color[:3],
                                               alpha=1.0 - self._render.alpha)
            self._add_patch(ax)


def ang_speed2vel(θ: float, v: float) -> np.ndarray:
    """get 2D-velocity from angle / speed representation"""
    return np.r_[np.cos(θ), np.sin(θ)] * v


def vel2ang_speed(x_dot: np.ndarray) -> np.ndarray:
    """get motion angle and speed from 2D-velocity"""
    return np.r_[np.arctan2(x_dot[1], x_dot[0]), np.hypot(*x_dot)]


class CircularEntity(Entity):

    def __init__(self, mass=1.0, max_speed=1.0,
                 size=1e-2,
                 p_min=np.r_[-1.0, -1.0],
                 p_max=np.r_[1.0, 1.0]):
        super(CircularEntity, self).__init__()
        self._static = ParticleEntityDynamics(mass=mass, shape=Circle(size=size),
                                              max_speed=max_speed,
                                              p_min=p_min, p_max=p_max)

    def update_patch(self, ax=None):
        if self._patch_object is not None:
            if self._render.invisible:
                self._patch_object.set_alpha(1.0)
        elif not self._render.invisible:
            p = self._p.copy()
            self._patch_object = plt.Circle((p[0], p[1]),
                                            radius=self._static.shape.size / 2.0,
                                            color=self._render.color[:3],
                                            alpha=1.0 - self._render.alpha)
            self._add_patch(ax)

    def update_vel(self, new_angle, new_speed):
        self._v[0] = np.sin(new_angle) * new_speed
        self._v[1] = np.cos(new_angle) * new_speed


class DubinsCarEntity(Entity):

    def __init__(self, mass=1.0, max_speed=1.0,
                 size=5e-3, head_size=2e-3,
                 p_min=(-1.0, -1.0),
                 p_max=(1.0, 1.0)):
        super(DubinsCarEntity, self).__init__()
        self._ϕ = 0.0
        self._ω = 0.0
        self._speed = 0.0
        self._head_size = head_size
        self._head_patch_object = None
        self._static = DubinsCarEntityDynamics(mass=mass, shape=Circle(size=size),
                                               max_speed=max_speed,
                                               p_min=p_min, p_max=p_max)

    @property
    def angle_speed_velocity(self) -> np.ndarray:
        return np.r_[self._ϕ, self._speed]

    @property
    def vel(self):
        return np.array(ang_speed2vel(self._ϕ, self._speed))

    @property
    def x(self):
        """internal agent state"""
        return np.r_[self._p, self._ϕ, self._speed, self._ω]

    def integrate(self):
        raise NotImplementedError

    def update_patch(self, ax=None):
        r = np.r_[self._static.shape.size / 2.0 - self._head_size / 2.0, 0.0]
        head_pos = self._p + np.array([[np.cos(self._ϕ), -np.sin(self._ϕ)],
                                       [np.sin(self._ϕ), np.cos(self._ϕ)]]) @ r
        if self._patch_object is not None:
            self._patch_object.set_center(self.pos)
            self._head_patch_object.set_center(head_pos)
            if self._render.invisible:
                self._patch_object.set_alpha(1.0)
                self._head_patch_object.set_alpha(1.0)
        elif not self._render.invisible:
            self._patch_object = plt.Circle((self._p[0], self._p[1]),
                                            radius=self._static.shape.size / 2.0,
                                            color=self._render.color[:3],
                                            alpha=1.0 - self._render.alpha)
            self._head_patch_object = plt.Circle((head_pos[0], head_pos[1]),
                                                 radius=self._head_size / 2.0,
                                                 color=self._render.color[:3],
                                                 alpha=1.0 - max(0.5 * self._render.alpha, 0.1))
            if ax is None:
                ax = plt.gca()
            ax.add_artist(self._patch_object)
            ax.add_artist(self._head_patch_object)

    def update_pose(self, overlap, angle, new_angle, new_speed):
        self._p[0] += np.sin(angle) * overlap
        self._p[1] += np.cos(angle) * overlap
        self._ϕ = new_angle
        self._speed = new_speed
        self._ω = 0.0


def collision_with(e1, m1, m2, angle, speed2):
    """update velocity angle and magnitude given two point-mass objects"""
    if np.isinf(m1):
        return e1.angle_speed_velocity
    elif np.isinf(m2):
        return np.r_[np.pi / 2.0 - angle, speed2]
    e1_angle, e1_speed = e1.angle_speed_velocity
    e1_speed *= (m1 - m2) / m1 + m2
    v_new = vel2ang_speed(ang_speed2vel(e1_angle, e1_speed) +
                          ang_speed2vel(angle, 2 * speed2 * m1 / (m1 + m2)))
    v_new[0] = np.pi / 2.0 - v_new[0]
    return v_new


def collide_entities(e1: typing.Union[CircularEntity, DubinsCarEntity],
                     e2: typing.Union[CircularEntity, DubinsCarEntity],
                     p1_new: np.ndarray, p2_new:np.ndarray
                     ):
    """apply collision between two (circular shaped) entities

    """
    if e1.mass <= 0.0 or e2.mass <= 0.0:
        return
    delta = p2_new - p1_new
    dx, dy = delta
    d = np.hypot(dx, dy)
    overlap = (e1.size + e2.size) / 2.0 - d
    if overlap > 0.0:
        angle = np.arctan2(dy, dx) + np.pi / 2.0
        elasticity = e1.elasticity * e2.elasticity
        v1_new = collision_with(e1, e1.mass, e2.mass, angle, e2.speed)
        v2_new = collision_with(e2, e2.mass, e1.mass, angle + np.pi, e1.speed)

        e1.update_vel(new_angle=v1_new[0], new_speed=v1_new[1] * elasticity)
        e2.update_vel(new_angle=v2_new[0], new_speed=v2_new[1] * elasticity)
        e1.pos = p1_new + ang_speed2vel(angle + np.pi / 2.0, overlap * 1.01)
        e2.pos = p2_new + ang_speed2vel(angle - np.pi / 2.0, overlap * 1.01)
        return True
    return False
