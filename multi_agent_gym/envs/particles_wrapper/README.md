# Particle Environment wrapper 

reworked particle environment.
Main resources

* [Loewe2017_] 
* [Iqbal2018_]

## Major changes

* Removed OpenGL by pygame and matplotlib
* Minor performance enhancements via centralized numpy execution
* Added agent models / multi-agent interface

## 