"""
Particle Rendering
"""
import functools
import os
import pathlib
import typing

import matplotlib.animation
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

import multi_agent_gym.core as _mag_core
import multi_agent_gym.envs.particles_wrapper.entities as _entities
import multi_agent_gym.utils as _mag_utils

os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = '1'

import pygame

if _mag_utils.in_ipynb():
    sns.set_theme('notebook')
else:
    sns.set_theme('paper')

_figure_cache = []


def plot_collision(x, x_prev, x_corrected, collision_indices, samples=50):
    def plot_velocity(j, ax):
        dx = np.linalg.norm(x_prev[j] - x[j])
        p0 = x_prev[j, 0:2] + np.r_[-dx, 0]
        p1 = x_prev[j, 0:2] + np.r_[dx, 0]
        v_orig = np.linspace(p0, p0 + x[j, 2:4] * dx / np.linalg.norm(x[j, 2:4]), samples)
        v_corr = np.linspace(p1, p1 + x_corrected[j, 2:4] * dx / np.linalg.norm(x_corrected[j, 2:4]), samples)
        ax.scatter(v_orig[:, 0], v_orig[:, 1], label=rf'$\dot{x}^{j}_{{orig}}$', s=5, color='cyan')
        ax.scatter(v_orig[-1, 0], v_orig[-1, 1], s=15, color='cyan')
        ax.scatter(v_corr[:, 0], v_corr[:, 1], label=rf'$\dot{x}^{j}_{{corr}}$', s=5, color='orange')
        ax.scatter(v_corr[-1, 0], v_corr[-1, 1], s=15, color='orange')

    def plot_collision_samples(j, line, ax=plt.gca()):
        ax.scatter(line[:, 0], line[:, 1], label=rf'$x^{j}_{{line}}$', s=5, color='blue')
        ax.scatter(x_prev[j, 0], x_prev[j, 1], label=rf'$x^{j}_{{t-1}}$', color='yellow')
        ax.scatter(x[j, 0], x[j, 1], label=rf'$x^{j}_{{t, ff}}$', color='red')
        ax.scatter(x_corrected[j, 0], x_corrected[j, 1], label=rf'$x^{j}_{{t, c}}$', color='green')
        plot_velocity(j, ax)

    for e1, e2 in zip(*collision_indices):
        line1 = np.linspace(x_prev[e1, 0:2], x[e1, 0:2], samples)
        line2 = np.linspace(x_prev[e2, 0:2], x[e2, 0:2], samples)
        _, axs = plt.subplots(2)
        plot_collision_samples(e1, line1, axs[0])
        plot_collision_samples(e2, line2, axs[1])
        plt.show(block=False)


def meter2dot(size_meter):
    fig = plt.gcf()
    dot_per_meter = fig.dpi / 0.0254
    return dot_per_meter * size_meter


def plot_2D_circle_entity(ent: _entities.CircularEntity, ax=None):
    if ax is None:
        ax = plt.gca()
    # assert ent.shape == _agents.Circle, "this function only fits simple circles"
    return ax.scatter(*ent.pos, c=ent.rgba_color[:3][None, :], s=meter2dot(ent.size) ** 2)


def plot_env_interactive(env: typing.Any, fig=None):
    """plot environment"""
    if fig is None:
        try:
            fig = _figure_cache[0]
        except IndexError:
            fig = plt.figure()
            _figure_cache.append(fig)

    plt.clf()
    list(map(plot_2D_circle_entity, env.agent_dict.values()))
    fig.canvas.draw_idle()


def plot_env(env: typing.Any, ax=None):
    """plot environment"""
    for current_entity_container in (env.agent_dict.values(), env.entities):
        list(map(functools.partial(plot_2D_circle_entity, ax=ax),
                 filter(lambda x: isinstance(x, _entities.CircularEntity),
                        current_entity_container)))
    plt.show()


def update_entity_scatter(ent: _entities.CircularEntity, ax, scatter_dict):
    try:
        scatter_dict[str(ent)].set_offsets(ent.pos)
    except KeyError:
        scatter_dict[str(ent)] = ax.scatter(*ent.pos, c=ent.rgba_color[:3][None, :],
                                            s=meter2dot(ent.size))


def update_circle_artist(ent: _entities.CircularEntity, ax, artist_dict):
    rgba_color = ent.rgba_color
    try:
        artist_dict[str(ent)].set_center(ent.pos)
        artist_dict[str(ent)].set_color(rgba_color[:-1])
        artist_dict[str(ent)].set_alpha(rgba_color[-1])
    except KeyError:
        artist_dict[str(ent)] = plt.Circle(ent.pos, radius=ent.size / 2.0,
                                           color=rgba_color[:3],
                                           alpha=rgba_color[3])
        ax.add_artist(artist_dict[str(ent)])


def update_from_env(ax, env, scatter_dict=None, artist_dict=None):
    if scatter_dict is not None:
        for current_entity_container in (env.agent_dict.values(), env.entities):
            list(map(functools.partial(update_entity_scatter, ax=ax, scatter_dict=scatter_dict),
                     filter(lambda x: isinstance(x, _entities.CircularEntity),
                            current_entity_container)))
    if artist_dict is not None:
        for current_entity_container in (env.agent_dict.values(), env.entities):
            list(map(functools.partial(update_circle_artist, ax=ax, artist_dict=artist_dict),
                     filter(lambda x: isinstance(x, _entities.CircularEntity),
                            current_entity_container)))


def update_scatter_pos(scatter_dict, key, pos):
    scatter_dict[key].set_offsets(pos)


def plot_animation(anim: matplotlib.animation.FuncAnimation):
    assert anim is not None
    plt.show()


def ipy_js_animation(anim: matplotlib.animation.FuncAnimation):
    from IPython.display import HTML
    return HTML(anim.to_jshtml())


def ipy_html_video(anim: matplotlib.animation.FuncAnimation):
    from IPython.display import HTML
    return HTML(anim.to_html5_video())


class MatplotlibPlotter:

    def __init__(self, **ax_kw):
        self.fig, self.ax = plt.subplots()
        _mag_utils.set2D_axis(self.ax, **ax_kw)
        self.scatters = {}
        self.lines = {}
        self.artists = {}

    def update_circle(self, key, xy):
        try:
            self.artists[key].set_center(xy)
        except KeyError:
            pass

    def update_circles(self, xy_dict):
        for cur_a in self.artists:
            try:
                self.update_circle(cur_a, xy_dict[cur_a]['x'][0:2])
            except KeyError:
                pass

    def add_circle_entity(self, name, ent: _entities.CircularEntity):
        if not isinstance(ent, _entities.CircularEntity):
            return
        rgba_color = ent.rgba_color
        self.artists[name] = plt.Circle(ent.pos, radius=ent.size / 2.0,
                                        color=rgba_color[:3],
                                        alpha=rgba_color[3])
        self.ax.add_artist(self.artists[name])

    @classmethod
    def from_env(cls, env):
        out = cls()
        for k, ent in env.agent_dict.items():
            if isinstance(ent, _entities.CircularEntity):
                out.add_circle_entity(k, ent)
        for ent in env.entities:
            if isinstance(ent, _entities.CircularEntity):
                out.add_circle_entity(str(ent), ent)
        return out

    def animate_episode_data(self, env, data: _mag_core.EpisodeData) -> matplotlib.animation.FuncAnimation:

        def animate(i):
            title.set_text(f"step {i + 1} / {len(data.observations) - 1}")
            try:
                cols = np.unique(data.env_info['infos'][i]['collisions'])
                for i, ag in enumerate(env.agent_list):
                    if i in cols:
                        self.artists[ag].set_alpha(0.7)
                    else:
                        self.artists[ag].set_alpha(1.0)
                if cols.size > 0:
                    collision_text.set_text(f"collisions ({i + 1}):\n{data.env_info['infos'][i]['collisions']}")
            except (KeyError, IndexError):
                pass
            for cur_a in env.agent_dict:
                try:
                    self.update_circle(cur_a, data.observations[i][cur_a]['x'][0:2])
                except KeyError:
                    pass

        title = self.ax.text(0.5, 1.05, "",
                             bbox={'facecolor': 'w', 'alpha': 0.5, 'pad': 5},
                             transform=self.ax.transAxes, ha="center")
        collision_text = self.ax.text(1.01, 0.5, "",
                                      bbox={'facecolor': 'w', 'alpha': 0.5, 'pad': 5},
                                      transform=self.ax.transAxes, ha="left")
        for k, ent in env.agent_dict.items():
            rgba_color = ent.rgba_color
            self.artists[k] = plt.Circle(ent.pos, radius=ent.size / 2.0,
                                         color=rgba_color[:3],
                                         alpha=rgba_color[3])
            self.ax.add_artist(self.artists[k])
        update_from_env(ax=self.ax, env=env, artist_dict=self.artists)

        return matplotlib.animation.FuncAnimation(self.fig, animate, frames=len(data.actions),
                                                  interval=1e3 / 25)

    def process_data_recording(self, env, filename: str or pathlib.Path):
        data = _mag_utils.load_pickle(filename)
        assert isinstance(data, _mag_core.EpisodeData), f"cannot evaluate data of type {type(data)}"
        assert isinstance(data.observations[0],
                          dict), f"recorded episode does not contain dictionary-based observations"
        assert isinstance(data.rewards[0], dict), f"recorded episode does not contain dictionary-based rewards"
        assert isinstance(data.rewards[0], dict), f"recorded episode does not contain dictionary-based rewards"
        return self.animate_episode_data(env, data=data)


def animate_data_recording(env, filename, html=False) -> typing.Tuple[typing.Any, matplotlib.animation.FuncAnimation]:
    tmp = MatplotlibPlotter()
    anim = tmp.process_data_recording(env, filename)
    if _mag_utils.in_ipynb():
        plt.close(tmp.fig)
        if html:
            return ipy_html_video(anim), anim
        else:
            return ipy_js_animation(anim), anim
    return plot_animation(anim), anim


class PyGameRendering:

    def __init__(self, title="", width=1000,
                 si_limits=(-1, 1),
                 rgb_bg_color=(255, 255, 255)):
        self._w = width
        self._si2game_scale = width / (si_limits[1] - si_limits[0])
        self._si2game_bias = 0.5 * (si_limits[1] - si_limits[0])
        self._flip_values = np.r_[1, -1]
        self.screen = pygame.display.set_mode((width, width))  # type: pygame.display
        if title:
            pygame.display.set_caption(title)
        self.bg_color = rgb_bg_color

    def si2game(self, x: np.ndarray) -> np.ndarray or int:
        game_version = self._si2game_scale * (x * self._flip_values + self._si2game_bias)
        return game_version.astype(int)

    def render(self, circle_entities: typing.List[_entities.CircularEntity]):
        _surface = pygame.Surface((self._w, self._w), pygame.SRCALPHA)
        self.screen.fill(self.bg_color)

        for e in circle_entities:
            color = (e.rgba_color * 255).astype(int)
            circle_kwargs = dict(color=color,
                                 center=self.si2game(e.pos),
                                 radius=self._si2game_scale * (e.size / 2.0))
            if e.rgba_color[-1] != 1.0:
                pygame.draw.circle(_surface, **circle_kwargs)
                self.screen.blit(_surface, (0, 0))
            else:
                pygame.draw.circle(self.screen, **circle_kwargs)
        pygame.display.flip()
