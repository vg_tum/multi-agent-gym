import abc
import typing

import gym
import numpy as np

import multi_agent_gym.utils as _mag_utils
import multi_agent_gym.core as _core
import multi_agent_gym.envs.particles_wrapper.entities as _entities

__all__ = ["ParticleAgentType", "CollectAgentType", "is_default_agent", "instantiate_agent"]


class ParticleBaseAgent(_core.BaseModelAgent, _entities.CircularEntity, abc.ABC):
    """Particle Agent wrapped as a multi-agent gym agent"""

    def __init__(self, i, dt=0.02, mass=1.0, max_speed=1.0,
                 u_max=5.0, size=5e-2, damping=0.9,
                 p_min=(-1.0, -1.0),
                 p_max=(1.0, 1.0)):
        _core.BaseAgent.__init__(self)
        _entities.CircularEntity.__init__(self, size=size, mass=mass, max_speed=max_speed,
                                          p_min=p_min, p_max=p_max)
        self._id = i
        self.team_id = 0
        self._u = np.zeros(2)
        self._y = None

        self._static = _entities.PointMassEntityDynamics.from_dt(mass=mass, size=size, dt=dt,
                                                                 p_min=np.array(list(p_min)),
                                                                 p_max=np.array(list(p_max)),
                                                                 max_speed=max_speed, damping=damping)
        u_max = min(u_max, max_speed / mass)
        assert u_max > 0.0, f"control space of agent {self} is empty"
        self.action_space = self._set_action_space(u_max)

    @property
    def state_space(self):
        return self._static.state_space

    @property
    def x(self):
        """external observations"""
        return np.r_[self._p, self._v]

    @property
    def y(self):
        """external observations"""
        return self._y

    @y.setter
    def y(self, value):
        self._y = value

    @property
    def u(self):
        """internal agent state"""
        return self._u

    def f(self, x=None, u=None):
        if x is None:
            x = self.x
        if u is None:
            u = self.u
        return self._static.A @ x + self._static.B @ u

    def linear_model(self):
        return self._static.A, self._static.B

    @abc.abstractmethod
    def _set_action_space(self, u_max: float) -> typing.Union[gym.spaces.Box, gym.spaces.Discrete]:
        """
        Set action-space for current agent

        Args:
            u_max(float): maximum control input / force.

        Returns:
            typing.Union[gym.spaces.Box, gym.spaces.Discrete]: action-space of this particular agent
        """

    @abc.abstractmethod
    def set_action(self, action, time=None) -> None:
        """
        Set action for current action, where

        ..code-block:: python

            assert self.action_space.contains(action)

        should not raise an AssertionError

        Args:
            action(typing.Any): current action
            time(float or None, optional): current time step
        """
        assert self.action_space.contains(action)


class MLPParticleAgent(ParticleBaseAgent):

    def __init__(self, i, **kw):
        super(MLPParticleAgent, self).__init__(i=i, **kw)

    def _set_action_space(self, u_max: float) -> gym.spaces.Box:
        return gym.spaces.Box(np.array([-u_max for _ in range(4)]),
                              np.array([u_max for _ in range(4)]),
                              dtype=np.float64)

    def set_action(self, action, time=None):
        assert isinstance(self.action_space, gym.spaces.Box)  # type: gym.spaces.Box
        u_mlp = np.clip(action, self.action_space.low, self.action_space.high)
        self._u[0] = u_mlp[1] - u_mlp[0]
        self._u[1] = u_mlp[3] - u_mlp[2]


class ContinuousParticleAgent(ParticleBaseAgent):
    """

    """

    def __init__(self, i, **kw):
        super(ContinuousParticleAgent, self).__init__(i=i, **kw)

    def _set_action_space(self, u_max: float) -> gym.spaces.Box:
        return gym.spaces.Box(np.r_[-u_max, -u_max],
                              np.r_[u_max, u_max], dtype=np.float64)

    def set_action(self, action, time=None):
        assert isinstance(self.action_space, gym.spaces.Box)  # type: gym.spaces.Box
        self._u = np.clip(action, self.action_space.low, self.action_space.high)


class DiscreteParticleAgent(ParticleBaseAgent):
    """Particle Agent wrapped as a multi-agent gym agent"""

    def __init__(self, i, **kw):
        self._u_max = 0.0
        super(DiscreteParticleAgent, self).__init__(i=i, **kw)

    def _set_action_space(self, u_max: float) -> gym.spaces.Discrete:
        self._u_max = np.abs(u_max)
        return gym.spaces.Discrete(5)

    def set_action(self, action, time=None):
        """

        Args:
            action(np.ndarray or int):
            time:
        """
        self._u.fill(0.0)
        try:
            self._u[0] = action[1] - action[2]
            self._u[1] = action[3] - action[4]
        except TypeError:
            if action == 1:
                self._u[0] = -self._u_max
            elif action == 2:
                self._u[0] = self._u_max
            elif action == 3:
                self._u[1] = -self._u_max
            elif action == 4:
                self._u[1] = self._u_max


ParticleAgentType = typing.Union[DiscreteParticleAgent, ContinuousParticleAgent, MLPParticleAgent]


def is_default_agent(agent: ParticleAgentType):
    """
    check if current agent is a default agent

    Args:
        agent(AgentType): current agent

    Returns:
        bool: True if agent is a default agent
    """

    return agent.team_id == 0


class CollectBase(abc.ABC):
    team_id: int
    _holds = None  # type: typing.Optional[int]
    _type_id = None  # type: typing.Optional[int]

    def pickup(self, a: typing.Optional[int]):
        if self._holds is None:
            self._holds = a

    def drop(self) -> typing.Optional[int]:
        if self._holds is not None:
            out = self._holds
            self._holds = None
            return out - 1

    @property
    def type_id(self) -> (int, int):
        if self._type_id is not None:
            return self.team_id, self._type_id
        if self._holds is not None:
            return self.team_id, self._holds
        return self.team_id, 0

    @type_id.setter
    def type_id(self, value: int):
        if self._holds is not None:
            _mag_utils.error(f"cannot set type-id. Agent already carries object {self._holds}")
            return
        self._type_id = value


class ContinuousCollectingAgent(ContinuousParticleAgent, CollectBase):

    def __init__(self, i, **kw):
        CollectBase.__init__(self)
        ContinuousParticleAgent.__init__(self, i=i, **kw)


class MLPCollectingAgent(MLPParticleAgent, CollectBase):

    def __init__(self, i, **kw):
        CollectBase.__init__(self)
        MLPParticleAgent.__init__(self, i=i, **kw)


class DiscreteCollectAgent(DiscreteParticleAgent, CollectBase):

    def __init__(self, i, **kw):
        CollectBase.__init__(self)
        DiscreteParticleAgent.__init__(self, i=i, **kw)


CollectAgentType = typing.Union[MLPCollectingAgent, DiscreteCollectAgent, ContinuousCollectingAgent]


def instantiate_particle_agent(discrete=False, mlp=False, **agent_kwargs) -> ParticleAgentType:
    if discrete:
        return DiscreteParticleAgent(**agent_kwargs)
    elif mlp:
        return MLPParticleAgent(**agent_kwargs)
    else:
        return ContinuousParticleAgent(**agent_kwargs)


def instantiate_collecting_agent(discrete=False, mlp=False, **agent_kwargs) -> CollectAgentType:
    if discrete:
        return DiscreteCollectAgent(**agent_kwargs)
    elif mlp:
        return MLPCollectingAgent(**agent_kwargs)
    else:
        return ContinuousCollectingAgent(**agent_kwargs)


def instantiate_agent(collect=False, discrete=False, mlp=False, **agent_kwargs) -> typing.Union[
    CollectAgentType, ParticleAgentType]:
    if collect:
        return instantiate_collecting_agent(discrete=discrete, mlp=mlp, **agent_kwargs)
    return instantiate_particle_agent(discrete=discrete, mlp=mlp, **agent_kwargs)
