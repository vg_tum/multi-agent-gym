import sys

import multi_agent_gym.core as _mag_core

import multi_agent_gym.envs.particles_wrapper.render as _render
import multi_agent_gym.utils as _mag_utils

from multi_agent_gym.envs.particles_wrapper.environments import CooperativeCollection, CooperativeNavigation, \
    TreasureCollect


def generate_samples(nb_episodes=1):
    env = CooperativeCollection()
    filename = _mag_utils.DATA_PATH / "test_1"
    _mag_core.generate_random_data(env, filename=filename, nb_episodes=nb_episodes,
                                   render=True)


def generate_and_plot_samples(nb_steps=300):
    env = TreasureCollect()
    filename = _mag_utils.DATA_PATH / "test_plot.pkl"
    _mag_core.generate_random_data(env, filename=filename, render=True,
                                   nb_episodes=1, nb_steps=nb_steps)
    _render.animate_data_recording(env=env, filename=filename)


def run_until_error(nb_steps=200):
    valid = True
    env = TreasureCollect()
    filename = _mag_utils.DATA_PATH / "test_error.pkl"
    cnt = 0
    while valid:
        _mag_core.generate_random_data(env, filename=filename, nb_episodes=1, nb_steps=nb_steps, render=True)
        data = _mag_utils.load_pickle(filename)  # type: _mag_core.EpisodeData
        if len(data.actions) != nb_steps:
            valid = False
            print(f'encountered error after {cnt + 1} runs')
        cnt += 1
    # harry_plotter = _render.MatplotlibPlotter()
    # harry_plotter.process_data_recording(filename=filename, env=env)


def run_func(f):
    try:
        f()
    except KeyboardInterrupt:
        print('Interrupted')
        sys.exit(0)


if __name__ == "__main__":
    run_func(generate_samples)
