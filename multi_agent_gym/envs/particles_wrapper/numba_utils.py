import numba as nb
import numpy as np

__all__ = ["update_mask"]


@nb.njit
def check_agent_types(ag_types, ii, jj):
    return ag_types[ii, 1] * ag_types[jj, 1] > 0 and \
           ag_types[ii, 0] != ag_types[jj, 0] and \
           ag_types[ii, 1] > 0 and \
           ag_types[jj, 1] > 0


@nb.njit
def update_mask(mask_arr: np.ndarray, ag_types):
    N = mask_arr.shape[0]
    M = mask_arr.shape[1]
    if M < N:
        return
    for ii in range(N):
        for jj in range(ii + 1, M):
            if check_agent_types(ag_types, ii, jj):
                mask_arr[ii, jj] = np.inf
            else:
                mask_arr[ii, jj] = 1.0
    return mask_arr

