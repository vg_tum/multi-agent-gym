"""
Register particle world environments



"""
from functools import partial
from ..registration import reg_helper

# __ Particle World ___
_layouts_particle = ["empty", "free", "room"]


def _env_name(n):
    # type: (str) -> str
    return f"Particles_{n}"


def _entry_str(f, c):
    # type: (str, str) -> str
    return f"multi_agent_gym.envs.particles_wrapper.{f}:{c}"


def _kwargs(**kwargs):
    _default_kwargs = dict(dt=0.02)
    try:
        return _default_kwargs | kwargs
    except TypeError:
        return {**_default_kwargs, **kwargs}


def sub_batch_reg(base_name, entry, num_agents, base_kwargs):
    base_kwargs['discrete_action'] = False
    base_kwargs['mlp_action'] = False
    disc_kwargs = base_kwargs.copy()
    mlp_kwargs = base_kwargs.copy()
    cont_kwargs = base_kwargs.copy()
    disc_kwargs['discrete_action'] = True
    mlp_kwargs['mlp_action'] = True
    reg_helper(_env_name(f"{base_name}_Cont"), _base_entry(c=entry), num_agents, cont_kwargs, **_reg_kwargs)
    reg_helper(_env_name(f"{base_name}_Mlp"), _base_entry(c=entry), num_agents, mlp_kwargs, **_reg_kwargs)
    reg_helper(_env_name(f"{base_name}_Disc"), _base_entry(c=entry), num_agents, disc_kwargs, **_reg_kwargs)


def batch_reg(base_name, entry, num_agents, base_kwargs, add_static=True, add_sparse=True):
    sub_batch_reg(base_name, entry, num_agents, base_kwargs)
    if add_static:
        st_base_kwargs = base_kwargs.copy()
        st_base_kwargs['static_goals'] = True
        sub_batch_reg(f"Static_{base_name}", entry, num_agents, st_base_kwargs)
    if add_sparse:
        sp_base_kwargs = base_kwargs.copy()
        sp_base_kwargs['sparse_rewards'] = True
        sub_batch_reg(f"Sparse_{base_name}", entry, num_agents, sp_base_kwargs)
    if add_sparse and add_static:
        sp_st_base_kwargs = base_kwargs.copy()
        sp_st_base_kwargs['static_goals'] = True
        sp_st_base_kwargs['sparse_rewards'] = True
        sub_batch_reg(f"Sparse_Static_{base_name}", entry, num_agents, sp_st_base_kwargs)


# cooperative and heterogeneous environments
_reg_kwargs = dict(cooperative=True, heterogeneous=True)
# cooperative navigation
_base_entry = partial(_entry_str, "environments")

# cooperative collection
batch_reg("CoopCollect", "CooperativeCollection", 3, _kwargs(nb_goals=3))
batch_reg("Collect", "CooperativeCollection", 1, _kwargs(nb_goals=1))

# cooperative navigation
batch_reg("CoopNav", "CooperativeNavigation", 3, _kwargs(nb_goals=3))
batch_reg("Reach", "CooperativeNavigation", 1, _kwargs(nb_goals=1))
# treasure collections
batch_reg("TreasureCollect", "TreasureCollect", 8, _kwargs(nb_visible=7, nb_collectors=6), add_static=False)

