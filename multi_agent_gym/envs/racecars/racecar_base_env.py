from functools import partial
from abc import ABC, abstractmethod
from multi_agent_gym.core import MultiAgentEnv
from typing import List
from _collections import OrderedDict
import numpy as np
import pybullet as _p
import pybullet_utils.bullet_client as bc

from .core import *


class RaceCarsBaseEnv(MultiAgentEnv, ABC):

    # objects to be instantiated
    def __init__(self, nb_agents, dim_p=2, shared_viewer=True, render=False, layout="free", nb_sub_steps=50):
        if render:
            self.bc = bc.BulletClient(connection_mode=_p.GUI)
            _p.resetDebugVisualizerCamera(12.0, 90, -60, [0.0, 0.0, 0.0])
        else:
            self.bc = bc.BulletClient(connection_mode=_p.DIRECT)
        self.nb_agents = nb_agents
        self.agents = list(map(partial(BaseCarAgent, bc=self.bc, dim_p=dim_p), range(nb_agents)))
        self.world = World(self.bc, layout=layout)
        self.shared_viewer = shared_viewer
        self._nb_sub_steps = nb_sub_steps
        self.goal_distances = None
        if not self.shared_viewer:
            self.viewers = [None] * self.nb_agents
        else:
            self.viewers = [None]
        self._reset_height = 5e-2

    def _close(self):
        self.bc = None

    def __del__(self):
        return self._close()

    @property
    def dynamic_entities(self):
        return self.agents

    def _sim_world(self) -> None:
        for _ in range(self._nb_sub_steps):
            self.bc.stepSimulation()
        for e_dyn in self.dynamic_entities:
            e_dyn.update()
        # for g_active in filter(lambda g: not g.is_achieved, self.goals):
        #     for ag in self.agents:
        #         list_contact_points = self.bc.getContactPoints(g_active.uniqueIdBullet, ag.uniqueIdBullet)
        #         if len(list_contact_points) > 0:
        #             g_active.is_achieved = True
        #             g_active.remove()
        pass

    @abstractmethod
    def get_observation(self) -> OrderedDict:
        """implementation of observation callback"""

    @abstractmethod
    def get_world_observation(self) -> np.ndarray:
        """centralized observation callback. Useless for competitive scenarios"""

    @abstractmethod
    def get_rewards(self) -> OrderedDict:
        """implementation of reward callback"""

    @property
    def done(self) -> bool:
        """Optional done callback as a property. Returns False by default """
        return False

    def _step_return(self):
        obs1 = self.get_observation()
        obs1["world"] = self.get_world_observation()
        return obs1, self.get_rewards(), self.done, {a.name: a.info for a in self.agents}

    def render(self, mode="rgb_array", close=False):
        pass
        # """
        # Copied from bullet_envs->kukaGymEnv
        #
        # Args:
        #     mode:
        #     close:
        #
        # Returns:
        #     np.ndarray: RGB image
        #
        # """
        # if mode != "rgb_array":
        #     return np.array([])
        # base_pos, orn = self._p.getBasePositionAndOrientation(self.agents[0].uniqueIdBullet)
        # view_matrix = self._p.computeViewMatrixFromYawPitchRoll(cameraTargetPosition=base_pos,
        #                                                         distance=self._cam_dist,
        #                                                         yaw=self._cam_yaw,
        #                                                         pitch=self._cam_pitch,
        #                                                         roll=0,
        #                                                         upAxisIndex=2)
        # proj_matrix = self._p.computeProjectionMatrixFOV(fov=60,
        #                                                  aspect=float(_RENDER_WIDTH) / _RENDER_HEIGHT,
        #                                                  nearVal=0.1,
        #                                                  farVal=100.0)
        # (_, _, px, _, _) = self._p.getCameraImage(width=_RENDER_WIDTH,
        #                                           height=_RENDER_HEIGHT,
        #                                           viewMatrix=view_matrix,
        #                                           projectionMatrix=proj_matrix,
        #                                           renderer=self._p.ER_BULLET_HARDWARE_OPENGL)
        # rgb_array = np.array(px, dtype=np.uint8)
        # rgb_array = np.reshape(rgb_array, (_RENDER_HEIGHT, _RENDER_WIDTH, 4))
        # rgb_array = rgb_array[:, :, :3]
        # return rgb_array
