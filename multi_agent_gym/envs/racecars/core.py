#!/usr/bin/env python
from dataclasses import dataclass
from pathlib import Path

import numpy as np
import pybullet_data

from multi_agent_gym.core import BaseAgent


def get_urdf_path():
    return Path(pybullet_data.getDataPath())


def get_floor(bc, layout):
    if layout.lower() == "stadium":
        return bc.loadSDF(str(get_urdf_path() / "stadium.sdf"))
    return bc.loadURDF(str(get_urdf_path() / "plane.urdf"))


@dataclass
class Pose:
    p_pos: np.ndarray = np.zeros(3)
    r_pos: np.ndarray = np.zeros(3)

    def set_from_bc(self, bc, bullet_id):
        pos, quat = get_pose_from_bc(bc, bullet_id)
        self.p_pos[:] = pos
        self.r_pos[:] = bc_quat2eul(bc, quat)


@dataclass
class DynState(Pose):
    p_vel: np.ndarray = np.zeros(3)
    r_vel: np.ndarray = np.zeros(3)

    def set_from_bc(self, bc, bullet_id):
        super().set_from_bc(bc, bullet_id)
        self.p_vel[:], self.r_vel[:] = get_velocity_from_bc(bc, bullet_id)

    def flatten(self):
        return np.hstack((self.p_pos, self.r_pos, self.p_vel, self.r_vel))


def get_pose_from_bc(bc, bullet_id):
    pos, quat = bc.getBasePositionAndOrientation(bullet_id)
    return pos, quat


def get_velocity_from_bc(bc, bulet_id):
    p_vel, rot_vel = bc.getBaseVelocity(bulet_id)
    return p_vel, rot_vel


def bc_quat2eul(bc, quat):
    return bc.getEulerFromQuaternion(quat)


def bc_eul2quat(bc, eul):
    return bc.getQuaternionFromEuler(eul)


def is_close(agent, ent):
    # type: (Agent, Entity)->bool
    return np.linalg.norm(agent.x.p_pos[:2] - ent.x.p_pos[:2]) <= 2e-1


def is_collision(bc, agent, ent):
    if is_close(agent, ent):
        return len(bc.getContactPoints(int(agent), int(ent))) > 0
    return False


class Entity:

    def __init__(self, urdf_path):
        self._urdf_path = str(urdf_path)
        self._id = -1
        self.x = Pose()

    def __int__(self):
        return self._id

    def add_at(self, bc, position, rot_eul=None):
        self.x.p_pos = position
        if rot_eul is None:
            self._id = bc.loadURDF(self._urdf_path, basePosition=position, useFixedBase=False)
        else:
            self._id = bc.loadURDF(self._urdf_path, basePosition=position,
                                   orientation=bc_eul2quat(bc, rot_eul), useFixedBase=False)
            self.x.r_pos = rot_eul


class Goal(Entity):

    def __init__(self, **kwargs):
        super(Goal, self).__init__(urdf_path=Path(__file__).parent / "models" / "cylinder" / "cylinder.urdf")
        self.active = True

    def add_at(self, bc, position, **kwargs):
        super().add_at(bc, position)
        self.active = True


class BaseCarAgent(BaseAgent, Entity):
    _steering_links = [0, 2]
    _motors = [8, 15]

    def __init__(self, a_i, bc, dim_p=2, speed_multiplier=20.0, steering_multiplier=20.0, max_force=50.0):
        self._ag_id = a_i
        self.team_id = 0
        self.bc = bc
        self.dim_p = dim_p
        self._speed_multiplier = speed_multiplier
        self._steering_multiplier = steering_multiplier
        self._max_force = max_force
        super(BaseAgent, self).__init__(urdf_path=get_urdf_path() / "racecar" / "racecar_differential.urdf")
        self.x = DynState()

    def add_at(self, bc, position, rot_eul=None):
        def set_gear_ratio(l1, l2, **ratio_kwargs):
            c = bc.createConstraint(self._id, l1, self._id, l2,
                                    jointType=bc.JOINT_GEAR,
                                    jointAxis=[0, 1, 0],
                                    parentFramePosition=[0, 0, 0],
                                    childFramePosition=[0, 0, 0])
            bc.changeConstraint(c, maxForce=10000, **ratio_kwargs)

        super().add_at(bc, position)
        for wheel in range(bc.getNumJoints(self._id)):
            bc.setJointMotorControl2(self._id, wheel, bc.VELOCITY_CONTROL, targetVelocity=0, force=0)
            bc.getJointInfo(self._id, wheel)

        set_gear_ratio(9, 11, gearRatio=1)
        set_gear_ratio(10, 13, gearRatio=-1)
        set_gear_ratio(9, 13, gearRatio=-1)
        set_gear_ratio(16, 18, gearRatio=1)
        set_gear_ratio(16, 19, gearRatio=-1)
        set_gear_ratio(17, 19, gearRatio=-1)
        set_gear_ratio(1, 18, gearRatio=-1, gearAuxLink=15)
        set_gear_ratio(3, 19, gearRatio=-1, gearAuxLink=15)
        self.x.p_vel.fill(0.0)
        self.x.r_vel.fill(0.0)
        self.set_state()

    @property
    def name(self):
        return f"agent_{self._ag_id}"

    def set_action(self, action, time=None):
        self.action = action
        targetVelocity = action[0] * self._speed_multiplier
        steeringAngle = action[1] * self._steering_multiplier

        for motor in self._motors:
            self.bc.setJointMotorControl2(self._id, motor, self.bc.VELOCITY_CONTROL,
                                          targetVelocity=targetVelocity, force=self._max_force)
        for steer in self._steering_links:
            self.bc.setJointMotorControl2(self._id, steer, self.bc.POSITION_CONTROL, targetPosition=steeringAngle)
        return self.action

    def set_state(self):
        if self.dim_p == 2:
            self.state = np.hstack((self.x.p_pos[0:2], self.x.p_vel[0:2]))
        elif self.dim_p == 3:
            self.state = np.hstack((self.x.p_pos[0:2], self.x.r_pos[2], self.x.p_vel[0:2], self.x.r_vel[2]))
        else:
            self.state = self.x.flatten()

    def update(self):
        self.x.set_from_bc(self.bc, self._id)
        self.set_state()


class World:

    def __init__(self, bc, layout="free"):
        self.ground = get_floor(bc, layout)
        self.goals = []
        self.obstacles = []

    @property
    def entities(self):
        return self.goals + self.obstacles
