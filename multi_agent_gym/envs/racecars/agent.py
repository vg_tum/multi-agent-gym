import os
import numpy as np
from multi_agent_gym.core import BaseAgent
from .entities import Entity
import gym.spaces as _g_spaces
from ._constants import _RESET_Z_OFFSET


class RacecarAgent(BaseAgent, Entity):
    """
    A generic agent in the racecar environment.

    Attributes:
        ind (int):                  id of agent
        obs_space(gym_space):       observation space of a agent
        action_space(gym_space):    action space of an agent (position of steering and velocity of car)
        bc:                         bullet client
        team_id(int)                team_id of agent
        reward_range(np.ndarray)    reward range of agent
        maxForce(float)             maximum Force for velocity control
        speedMultiplier(float)      speedMultiplier for velocity control
        steeringMultiplier(float):  steeringMultiplier for steering position control
    Note:
        This implementation does not contain any communication
    """

    def __init__(self,
                 ind: int,
                 obs_space,
                 action_space,
                 bc,
                 team_id=0,
                 reward_range=np.array([-np.inf, np.inf]),
                 maxForce=50,
                 speedMultiplier=20,
                 steeringMultiplier=0.5):
        Entity.__init__(self, bc=bc, ind=ind)
        self._id = ind
        self.team_id = team_id
        self.reward_range = reward_range

        self.action_space = action_space
        self.obs_space = obs_space
        self.state_space = _g_spaces.Box(-np.ones(6) * np.inf, np.ones(6) * np.inf, dtype=np.float32)

        # constants for racecar Agents from
        # https://github.com/bulletphysics/bullet3/blob/master/examples/pybullet/gym/pybullet_envs/bullet/racecar.py
        self.steeringLinks = [0, 2]
        self.nMotors = 2
        self.motorizedwheels = [8, 15]

        self.maxForce = maxForce
        self.speedMultiplier = speedMultiplier
        self.steeringMultiplier = steeringMultiplier
        self.state_idx = (0, len(self.pose) + len(self.vel))
        self.agent_idx = (None, None)
        self.ent_idx = (None, None)
        self.goal_idx = (None, None)

    @property
    def info(self):
        return dict(x=self.state_idx, ag_obs=self.agent_idx, e_obs=self.ent_idx, g_obs=self.goal_idx)

    def reset(self, random_init=False, init_square_size=2.0):
        position = np.zeros(3)
        position[2] = _RESET_Z_OFFSET

        if random_init:
            position[0:2] = np.random.uniform(low=-init_square_size, high=init_square_size, size=(2,))
            orientation = self.bc.getQuaternionFromEuler(
                [0.0, 0.0, np.random.uniform(low=-3.141, high=3.141, size=(1))])
        else:
            position[0:2] = np.array([-self._id, 0.0])
            orientation = np.array([0.0, 0.0, 0.0, 1.0])

        # see: https://github.com/bulletphysics/bullet3/blob/master/examples/pybullet/gym/pybullet_envs/bullet/racecar.py
        self.uniqueIdBullet = self.bc.loadURDF(
            fileName=os.path.join(self._urdfRoot, "racecar/racecar_differential.urdf"),
            basePosition=position,
            baseOrientation=orientation,
            useFixedBase=False)

        for wheel in range(self.bc.getNumJoints(self.uniqueIdBullet)):
            self.bc.setJointMotorControl2(self.uniqueIdBullet,
                                          wheel,
                                          self.bc.VELOCITY_CONTROL,
                                          targetVelocity=0,
                                          force=0)
            self.bc.getJointInfo(self.uniqueIdBullet, wheel)

        c = self.bc.createConstraint(self.uniqueIdBullet,
                                     9,
                                     self.uniqueIdBullet,
                                     11,
                                     jointType=self.bc.JOINT_GEAR,
                                     jointAxis=[0, 1, 0],
                                     parentFramePosition=[0, 0, 0],
                                     childFramePosition=[0, 0, 0])
        self.bc.changeConstraint(c, gearRatio=1, maxForce=10000)

        c = self.bc.createConstraint(self.uniqueIdBullet,
                                     10,
                                     self.uniqueIdBullet,
                                     13,
                                     jointType=self.bc.JOINT_GEAR,
                                     jointAxis=[0, 1, 0],
                                     parentFramePosition=[0, 0, 0],
                                     childFramePosition=[0, 0, 0])
        self.bc.changeConstraint(c, gearRatio=-1, maxForce=10000)

        c = self.bc.createConstraint(self.uniqueIdBullet,
                                     9,
                                     self.uniqueIdBullet,
                                     13,
                                     jointType=self.bc.JOINT_GEAR,
                                     jointAxis=[0, 1, 0],
                                     parentFramePosition=[0, 0, 0],
                                     childFramePosition=[0, 0, 0])
        self.bc.changeConstraint(c, gearRatio=-1, maxForce=10000)

        c = self.bc.createConstraint(self.uniqueIdBullet,
                                     16,
                                     self.uniqueIdBullet,
                                     18,
                                     jointType=self.bc.JOINT_GEAR,
                                     jointAxis=[0, 1, 0],
                                     parentFramePosition=[0, 0, 0],
                                     childFramePosition=[0, 0, 0])
        self.bc.changeConstraint(c, gearRatio=1, maxForce=10000)

        c = self.bc.createConstraint(self.uniqueIdBullet,
                                     16,
                                     self.uniqueIdBullet,
                                     19,
                                     jointType=self.bc.JOINT_GEAR,
                                     jointAxis=[0, 1, 0],
                                     parentFramePosition=[0, 0, 0],
                                     childFramePosition=[0, 0, 0])
        self.bc.changeConstraint(c, gearRatio=-1, maxForce=10000)

        c = self.bc.createConstraint(self.uniqueIdBullet,
                                     17,
                                     self.uniqueIdBullet,
                                     19,
                                     jointType=self.bc.JOINT_GEAR,
                                     jointAxis=[0, 1, 0],
                                     parentFramePosition=[0, 0, 0],
                                     childFramePosition=[0, 0, 0])
        self.bc.changeConstraint(c, gearRatio=-1, maxForce=10000)

        c = self.bc.createConstraint(self.uniqueIdBullet,
                                     1,
                                     self.uniqueIdBullet,
                                     18,
                                     jointType=self.bc.JOINT_GEAR,
                                     jointAxis=[0, 1, 0],
                                     parentFramePosition=[0, 0, 0],
                                     childFramePosition=[0, 0, 0])
        self.bc.changeConstraint(c, gearRatio=-1, gearAuxLink=15, maxForce=10000)
        c = self.bc.createConstraint(self.uniqueIdBullet,
                                     3,
                                     self.uniqueIdBullet,
                                     19,
                                     jointType=self.bc.JOINT_GEAR,
                                     jointAxis=[0, 1, 0],
                                     parentFramePosition=[0, 0, 0],
                                     childFramePosition=[0, 0, 0])
        self.bc.changeConstraint(c, gearRatio=-1, gearAuxLink=15, maxForce=10000)

    @property
    def state(self) -> np.ndarray:
        return np.append(self.pose, self.vel)

    def set_action(self, action, time=None) -> np.ndarray:
        """
        sets action in agent entity and in pybullet simulation
        """
        self.action = action

        targetVelocity = action[0] * self.speedMultiplier
        steeringAngle = action[1] * self.steeringMultiplier

        for motor in self.motorizedwheels:
            self.bc.setJointMotorControl2(self.uniqueIdBullet,
                                          motor,
                                          self.bc.VELOCITY_CONTROL,
                                          targetVelocity=targetVelocity,
                                          force=self.maxForce)
        for steer in self.steeringLinks:
            self.bc.setJointMotorControl2(self.uniqueIdBullet,
                                          steer,
                                          self.bc.POSITION_CONTROL,
                                          targetPosition=steeringAngle)
        return self.action
