import numpy as np
import os, pybullet_data
from abc import ABC, abstractmethod
from ._constants import _RESET_Z_OFFSET


class Entity(ABC):
    """
    Entity in Pybullet

    Args:
        bc:                     BulletClient
        ind(int):               id of entity
        urdfRoot(str)          path to urdf/sdf of entity

    TODO:
        add collisions with attribute

    """
    def __init__(self, bc, ind, urdfRoot=pybullet_data.getDataPath()):
        self.bc = bc
        self._urdfRoot=urdfRoot
        self.uniqueIdBullet = -1
        self.ind = ind  # entity id

        # TODO  attributes:
        self.has_collisions_with = []  # this has to be set in class MuAgRacecarEnv in sim function
        self.color = None
        self._pos = np.zeros(3)
        self._rot_euler = np.zeros(3)
        self._vel_pos = np.zeros(3)
        self._vel_rot = np.zeros(3)

    def update(self):
        if self.uniqueIdBullet >= 0:
            self._pos, oriQuaternion = self.bc.getBasePositionAndOrientation(self.uniqueIdBullet)
            self._rot_euler = self.bc.getEulerFromQuaternion(oriQuaternion)
            self._vel_pos, self._vel_rot = self.bc.getBaseVelocity(self.uniqueIdBullet)

    @property
    def pose(self) -> np.ndarray:
        """
        get pose of entity in 2D world coordinates (x,y,yaw)
        """
        return np.array([self._pos[0], self._pos[1], self._rot_euler[2]])

    @property
    def vel(self) -> np.ndarray:
        """
        get velocity of entity in 2D world coordinates (xdot,ydot,yawdot)
        """
        return np.array([self._vel_pos[0], self._vel_pos[1], self._vel_rot[2]])

    @property
    def pos(self) -> np.ndarray:
        """
        get position of entity in 2D world coordinates (x,y)
        """
        return np.array([self._pos[0], self._pos[1]])

    @property
    def angle(self) -> np.ndarray:
        
        """
        get yaw angle
        """
        return np.array([self._rot_euler[2]])

    def remove(self):
        """
        removes entity from world
        """
        self.bc.removeBody(self.uniqueIdBullet)
        self.uniqueIdBullet = -1

    def getPoseVelOfEntity3D(self, ent) -> np.ndarray:
        """
            coordinate transform of pose and velocity in 3D world
            returns the pose and the velocity of ent from this enities coordinate frame
            pose: (x,y,z, roll, pitch, yaw)
            vel: (xdot,ydot,zdot, rolldot, pitchdot, yawdot)
            TODO: 
                angular velocity for roll and pitch are set to None. 
        """
        # Pose
        own_pos_base, own_orn_base = self.bc.getBasePositionAndOrientation(self.uniqueIdBullet)
        trans_base_to_own_lin, trans_base_to_own_orn = self.bc.invertTransform(own_pos_base, own_orn_base)

        ent_pos_base, ent_orn_base = self.bc.getBasePositionAndOrientation(ent.uniqueIdBullet)
        ent_pos_own, ent_orn_own = self.bc.multiplyTransforms(trans_base_to_own_lin, trans_base_to_own_orn, ent_pos_base, ent_orn_base)
        ent_orn_own_euler = self.bc.getEulerFromQuaternion(ent_orn_own)

        # Velocity
        no_translation = (0.0, 0.0, 0.0)
        no_rotation = (0.0, 0.0, 0.0, 1.0)
        _ , trans_velbase_to_velown_orn = self.bc.invertTransform(no_translation, own_orn_base)

        own_lin_vel_base, own_ang_vel_base = self.bc.getBaseVelocity(self.uniqueIdBullet)
        ent_lin_vel_base, ent_ang_vel_base = self.bc.getBaseVelocity(ent.uniqueIdBullet)

        ent_lin_vel_base = np.array(ent_lin_vel_base) - np.array(own_lin_vel_base)
        ent_ang_vel_base = np.array(ent_ang_vel_base) - np.array(own_ang_vel_base) 

        ent_lin_vel_own, _ = self.bc.multiplyTransforms(no_translation, trans_velbase_to_velown_orn, ent_lin_vel_base, no_rotation)

        return np.array(ent_pos_own + ent_orn_own_euler + ent_lin_vel_own + (None, None, ent_ang_vel_base[2]))

    def getPoseVelOfEntity2D(self, ent) -> np.ndarray:
        """
            coordinate transform of pose and velocity in 2D world
            returns the pose and the velocity of ent from this enities coordinate frame
            pose: (x,y, yaw)
            vel: (xdot,ydot, yawdot)
        """
        pose_vel3D = self.getPoseVelOfEntity3D(ent)
        return np.array([pose_vel3D[0], pose_vel3D[1], pose_vel3D[5], pose_vel3D[6], pose_vel3D[7], pose_vel3D[11]])

    @abstractmethod
    def reset(self, random_init=False):
        """
        each object needs a reset function, Basically the urdf/sdf file is loaded.
        """


class Ground(Entity, ABC):
    def __init__(self, **kwargs):
        super(Ground, self).__init__(ind=0, **kwargs)


class FreeGround(Ground):
    def __init__(self, **kwargs):
        super(FreeGround, self).__init__(**kwargs)

    def reset(self, random_init=False):
        """
        spawns Plane in world
        """
        self.uniqueIdBullet = self.bc.loadURDF(os.path.join(self._urdfRoot, "plane.urdf"))


class StadiumGround(Ground):
    def __init__(self, **kwargs):
        super(StadiumGround, self).__init__(**kwargs)

    def reset(self, random_init=False):
        """
        spawns Stadium in world
        """
        self.uniqueIdBullet = self.bc.loadSDF(os.path.join(self._urdfRoot, "stadium.sdf"))


class Goal(Entity):
    def __init__(self, **kwargs):
        super(Goal, self).__init__(**kwargs)
        self._urdfRoot, _ = os.path.split((os.path.realpath(__file__)))
        self._urdfRoot = os.path.join(self._urdfRoot, "models", "cylinder")
        self.is_achieved = None

    def reset(self, random_init=False, init_square_size=8.0):
        """
        spawns goal in world
        """
        position = np.zeros(3)
        position[2] = _RESET_Z_OFFSET

        if random_init:
            position[0:2] = np.random.uniform(low=-init_square_size, high=init_square_size, size=(2,))
        else:
            position[0:2] = np.array([self.ind + 1, 0.0])

        # self.uniqueIdBullet = self.bc.loadURDF(os.path.join("models", "cylinder", "cylinder.urdf"))
        self.uniqueIdBullet = self.bc.loadURDF(os.path.join(self._urdfRoot, "cylinder.urdf"),
                                               basePosition=position,
                                               useFixedBase=False)
        self.is_achieved = False

    @property
    def pos(self):
        """
        overload pos property.
        if goal was achieved we set position to infinity
        """
        if self.is_achieved:
            return np.ones(2) * np.inf
        else:
            return super().pos




def getPoseVelOfEntity3D(bc, bullet_id, other_bullet_id) -> np.ndarray:
    """
    coordinate transform of pose and velocity in 3D world
    returns the pose and the velocity of ent from this enities coordinate frame
    pose: (x,y,z, roll, pitch, yaw)
    vel: (xdot,ydot,zdot, rolldot, pitchdot, yawdot)
    TODO:
        angular velocity for roll and pitch are set to None.
    Note:
        this is bullshit
    """
    # Pose
    own_pos_base, own_orn_base = bc.getBasePositionAndOrientation(bullet_id)
    trans_base_to_own_lin, trans_base_to_own_orn = bc.invertTransform(own_pos_base, own_orn_base)

    ent_pos_base, ent_orn_base = bc.getBasePositionAndOrientation(ent.uniqueIdBullet)
    ent_pos_own, ent_orn_own = bc.multiplyTransforms(trans_base_to_own_lin, trans_base_to_own_orn,
                                                     ent_pos_base, ent_orn_base)
    ent_orn_own_euler = bc.getEulerFromQuaternion(ent_orn_own)

    # Velocity
    no_translation = (0.0, 0.0, 0.0)
    no_rotation = (0.0, 0.0, 0.0, 1.0)
    _, trans_velbase_to_velown_orn = bc.invertTransform(no_translation, own_orn_base)

    own_lin_vel_base, own_ang_vel_base = bc.getBaseVelocity(bullet_id)
    ent_lin_vel_base, ent_ang_vel_base = bc.getBaseVelocity(ent.uniqueIdBullet)

    ent_lin_vel_base = np.array(ent_lin_vel_base) - np.array(own_lin_vel_base)
    ent_ang_vel_base = np.array(ent_ang_vel_base) - np.array(own_ang_vel_base)

    ent_lin_vel_own, _ = bc.multiplyTransforms(no_translation, trans_velbase_to_velown_orn, ent_lin_vel_base,
                                               no_rotation)

    return np.array(ent_pos_own + ent_orn_own_euler + ent_lin_vel_own + (None, None, ent_ang_vel_base[2]))


def getPoseVelOfEntity2D(self, ent) -> np.ndarray:
    """
        coordinate transform of pose and velocity in 2D world
        returns the pose and the velocity of ent from this enities coordinate frame
        pose: (x,y, yaw)
        vel: (xdot,ydot, yawdot)
    """
    pose_vel3D = self.getPoseVelOfEntity3D(ent)
    return np.array([pose_vel3D[0], pose_vel3D[1], pose_vel3D[5], pose_vel3D[6], pose_vel3D[7], pose_vel3D[11]])
