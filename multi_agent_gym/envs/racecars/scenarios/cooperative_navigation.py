from collections import OrderedDict
from gym.spaces import Box
import numpy as np
from ..core import Goal, Pose, DynState, is_collision
from ..racecar_base_env import RaceCarsBaseEnv


def get_pos_diff(x1, x2, dim_p):
    if dim_p == 2:
        return x1.p_pos[:2] - x2.p_pos[:2]


class CooperativeNavigation(RaceCarsBaseEnv):
    """


    """
    dim_p = 2

    def __init__(self, nb_agents=3, nb_goals=3, **env_args):
        super(CooperativeNavigation, self).__init__(nb_agents, dim_p=self.dim_p, **env_args)
        self.world.goals = [Goal() for _ in range(nb_goals)]
        self.reset()
        self.set_spaces()

    def set_spaces(self):
        for agent in self.agents:
            agent.state_space = Box(-2.0 * np.ones(agent.state.shape), 2.0 * np.ones(agent.state.shape))
            delta_obs = len(self.world.goals) + self.nb_agents - 1
            agent.obs_space = Box(
                np.concatenate((agent.state_space.low,
                                np.array([-np.inf] * self.dim_p * delta_obs))),
                np.concatenate((agent.state_space.high,
                                np.array([np.inf] * self.dim_p * delta_obs))),
                dtype=np.float32)
            agent.action_space = Box(np.array((-10.0, -np.pi)), np.array((10.0, np.pi)))
        self.world_state_space = Box(np.hstack([a.state_space.low for a in self.agents]),
                                     np.hstack([a.state_space.high for a in self.agents]), dtype=np.float32)

    def reset(self):
        for g in self.world.goals:
            pos = np.hstack((np.random.uniform(-1, +1, self.dim_p), self._reset_height))
            g.add_at(self.bc, pos)
        for agent in self.agents:
            pos = np.hstack((np.random.uniform(-1, +1, self.dim_p), self._reset_height))
            rot = np.array([0.0, 0.0, np.random.uniform(-np.pi, np.pi)])
            agent.add_at(self.bc, pos, rot)

        return self.get_observation()

    def get_observation(self):
        def get_ent_dist(e):
            return get_pos_diff(e.x, agent.x, self.dim_p)

        obs1 = OrderedDict()
        for agent in self.agents:
            entity_pos = list(map(get_ent_dist, self.world.entities))
            others_pos = list(map(get_ent_dist, filter(lambda x: x != agent, self.agents)))
            obs1[agent.name] = np.hstack((agent.state, *entity_pos, *others_pos))
        return obs1

    def get_world_observation(self):
        positions = [e.x.p_pos[:2] for e in self.world.entities]
        velocities = [ag.state for ag in self.agents]
        return np.hstack((*positions, *velocities))

    @property
    def done(self):
        return False

    def get_team_reward(self):
        rew = 0
        for g in filter(lambda x: x.active, self.world.goals):
            dists = [np.sqrt(np.sum(np.square(a.x.p_pos[:2] - g.x.p_pos[:2]))) for a in self.agents]
            rew -= min(dists)
        return rew

    def get_rewards(self) -> OrderedDict:
        team_reward = self.get_team_reward()
        reward = OrderedDict()
        for agent in self.agents:
            cost = 0
            for _ in filter(lambda x: is_collision(self.bc, agent, x), self.agents):
                cost += 1
            reward[agent.name] = team_reward - cost
        return reward


class CooperativeCollection(CooperativeNavigation):

    def __init__(self, nb_agents=3, nb_goals=3, **env_args):
        super(CooperativeCollection, self).__init__(nb_agents, nb_goals, **env_args)

    @property
    def done(self):
        return not any([g.active for g in self.world.goals])

    def get_team_reward(self):
        rew = super(CooperativeCollection, self).get_team_reward()
        for g in filter(lambda x: x.active, self.world.goals):
            if any([is_collision(self.bc, ag, g) for ag in self.agents]):
                g.active = False
        return rew
