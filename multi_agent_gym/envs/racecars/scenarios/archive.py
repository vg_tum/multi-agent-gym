from collections import OrderedDict
from multi_agent_gym.envs.racecars.agent import RacecarAgent
import gym.spaces as _g_spaces
import numpy as np

from ..racecar_base_env import RaceCarsBaseEnv


# class MuAgRacecarEnv(MultiAgentEnv, ABC):
#     """
#     Racecar World
#
#     Description:
#         Generic Racecar Environment
#
#
#     World state:
#         agent position
#         agent velocity
#         goal positions
#
#
#     Agents:
#         Type: RacecarAgent
#         length: nb_agents
#
#     Args:
#         nb_agents(int):        number of agents.
#         nb_goals(int):         number of goals.
#         render(bool)            render environment ang agents
#         random_init(bool)       initialize entities in environment randomly
#         actionRepeat(int)       number of simulation steps per RL-environment steps
#         num_adversaries(int)    number of adversaries if there are two teams
#         sparse_reward(bool)     use sparse reward model
#     """
#     metadata = {'render.modes': ['human', 'rgb_array'], 'video.frames_per_second': 50}
#
#     def __init__(self,
#                  nb_agents=2,
#                  nb_goals=0,
#                  render=False,
#                  layout = "free",
#                  random_init=True,
#                  actionRepeat=50,
#                  num_adversaries=0,
#                  sparse_reward=False,
#                  **kwargs):
#         # PyBullet attributes
#         self._actionRepeat = actionRepeat
#         self._renders = render
#         self.sparse_rewards = sparse_reward
#
#         if self._renders:
#             self.bc = bc.BulletClient(connection_mode=_p.GUI)
#             _p.resetDebugVisualizerCamera(12.0, 90, -60, [0.0, 0.0, 0.0])
#         else:
#             self.bc = bc.BulletClient(connection_mode=_p.DIRECT)
#
#         # Env entities
#         self.ground = None
#         self.agents = []  # type: List[RacecarAgent]
#         self.goal_objs = []  # type: List[Goal]
#         # self.obstacles = []  # TODO: include walls and other objects
#
#         self.nb_agents = nb_agents
#         self.nb_goals = nb_goals
#
#         self.random_init = random_init
#         self.num_adversaries = num_adversaries
#
#         self.entities = []
#         self.set_layout(layout=layout)
#         self._agent_pos = []
#
#     @property
#     def world_state(self):
#         state_agents = np.array([np.append(ag.pose, ag.vel) for ag in self.agents]).ravel()
#         state_goals = np.array([g.pos for g in self.goals]).ravel()
#         return np.clip(np.append(state_agents, state_goals), self.world_state_space.low,
#                        self.world_state_space.high)
#
#     @property
#     def goals(self):
#         return self.goal_objs
#
#     def _sim_world(self) -> None:
#         """
#         step in pybullet
#         TODO:
#             detect collisions
#         """
#         for i in range(self._actionRepeat):
#             self.bc.stepSimulation()
#             # after each simulation step we check if the active goals collided with an agent
#             # if this is the case this goal is removed from the world
#         for g_active in filter(lambda g: not g.is_achieved, self.goals):
#             for ag in self.agents:
#                 list_contact_points = self.bc.getContactPoints(g_active.uniqueIdBullet, ag.uniqueIdBullet)
#                 if len(list_contact_points) > 0:
#                     g_active.is_achieved = True
#                     g_active.remove()
#         for e in self.entities:
#             e.update()
#
#     def reset(self) -> np.ndarray or OrderedDict:
#         """
#         resets the whole world by calling each entity's reset function
#         TODO:
#             assure that there are no collisions for random init positions
#         """
#         self.bc.resetSimulation()
#         self.bc.setGravity(0, 0, -10)
#
#         for entity in self.entities:
#             entity.reset(random_init=self.random_init)
#
#         reset_action_n = np.array([np.zeros(ag.action_space.shape) for ag in self.agents])
#         return self.step(reset_action_n)[0]
#
#     # def set_layout(self, layout="free"):
#     #     # Create ground entity (other entities are created in init function of child class)
#     #     if layout == "free":
#     #         self.ground = FreeGround(bc=self.bc)
#     #     elif layout == "stadium":
#     #         self.ground = StadiumGround(bc=self.bc)
#     #     else:
#     #         raise NotImplementedError("unknown layout {}".format(layout))
#     #     self.entities.append(self.ground)
#     #     for k in range(self.nb_goals):
#     #         self.goal_objs.append(Goal(bc=self.bc, ind=k))
#     #     self.entities.extend(self.goal_objs)


class CooperativeNavigation(RaceCarsBaseEnv):
    """

    Agent observation:
        * 1x own position (x,y, angle) and velocity (xdot, ydot, angledot) relative to fix world frame
        * (nb_agents-1)x other agents position (x,y, angle) and velocity (xdot, ydot, angledot) relative to agent frame
        * num_goals x position (x,y) of goals relative to agent frame

    TODO:
        1. here also the laser scanner could be used
        2. try out angle and distance instead of cartesian coordinates (x,y)
    """

    def __init__(self, ind: int, bc, nb_agents, num_goals):
        obs_dim = 6 * nb_agents + 2 * num_goals
        action_dim = 2
        obs_space = _g_spaces.Box(low=-np.inf * np.ones(obs_dim), high=np.inf * np.ones(obs_dim), dtype=np.float32)
        action_space = _g_spaces.Box(low=-np.inf * np.ones(action_dim), high=np.inf * np.ones(action_dim),
                                     dtype=np.float32)
        reward_range = (-np.inf, np.inf)
        super(SpreadAgent, self).__init__(ind, obs_space, action_space, bc, team_id=0, reward_range=reward_range)


class CooperativeCollection(MuAgRacecarEnv):
    def __init__(self,
                 nb_agents=2,
                 nb_goals=2,
                 render=False,
                 layout="free",
                 random_init=True,
                 actionRepeat=50,
                 num_adversaries=0,
                 **kwargs):
        world_dim = 6 * nb_agents + 2 * nb_goals
        world_state_low = -np.inf * np.ones(world_dim)
        world_state_high = np.inf * np.ones(world_dim)
        self.world_state_space = _g_spaces.Box(low=world_state_low, high=world_state_high, dtype=np.float64)
        super(CooperativeCollection, self).__init__(nb_agents=nb_agents, nb_goals=nb_goals, render=render, layout=layout,
                                                    random_init=random_init, actionRepeat=actionRepeat,
                                                    num_adversaries=num_adversaries, **kwargs)
        for n in range(self.nb_agents):
            self.agents.append(SpreadAgent(ind=n, bc=self.bc, nb_agents=self.nb_agents, num_goals=self.nb_goals))
            assert n == int(self.agents[n]), "mismatch in agent list"
        self.entities.extend(self.agents)
        self.reset()  # reset all entities
        super().filter_kwargs(**kwargs)
        assert self.num_adversaries == 0, "In RacecarSpread, there is only one team"
        assert self.nb_goals > 0, "In RacecarSpread, at least one goal is needed"

    @property
    def done(self) -> bool:
        """
        Episode is finished, when all goals are achieved by the team
        """
        return all([g.is_achieved for g in self.goals])

    def _step_return(self):
        """
        Interpret new state of environment and return gym-style return values
        """
        info = {}
        observation = OrderedDict({ag.name: np.zeros(ag.obs_space.shape) for ag in self.agents})
        reward = np.zeros(self.nb_agents)
        # record observation for each agent
        N = self.nb_agents
        for a_i, ag in enumerate(self.agents):
            goals_obs = np.array([(g.pos - ag.pos) if not g.is_achieved else np.zeros(2) for g in self.goals],
                                 dtype=np.float64)
            observation[ag.name] = np.concatenate([
                ag.state.flatten(),
                np.array([self.agents[a_j].pose for a_j in filter(lambda x: x != a_i, range(N))]).flatten(),
                np.array([self.agents[a_j].vel for a_j in filter(lambda x: x != a_i, range(N))]).flatten(),
                goals_obs.flatten()])
            info[ag.name] = ag.info
            # todo: add individual agent reward here
        team_rew = 0.0
        if self.sparse_rewards:
            raise NotImplementedError("implement sparse team reward model")
        else:
            for g_active in filter(lambda g: not g.is_achieved, self.goals):
                dists = [np.linalg.norm(g_active.pos - ag.pos) for ag in self.agents]
                team_rew = team_rew - min(dists)
        reward += team_rew
        info['world_state'] = self.world_state
        rew = OrderedDict({ag.name: reward[int(ag)] for ag in self.agents})
        for ag in self.agents:
            ag.end_step()
        return observation, rew, self.done, info


# visualization of environment
# inspired from https://github.com/bulletphysics/bullet3/blob/master/examples/pybullet/gym/pybullet_envs/examples/racecarGymEnvTest.py
if __name__ == "__main__":
    env = CooperativeCollection(nb_agents=1, nb_goals=3, render=True, random_init=True, layout="free", num_adversaries=0)
    targetVelocitySlider_n = [env.bc.addUserDebugParameter("wheelVelocity{}".format(n), -1, 1, 0) for n in
                              range(env.nb_agents)]
    steeringSlider_n = [env.bc.addUserDebugParameter("steering{}".format(n), -1, 1, 0) for n in range(env.nb_agents)]
    for _ in range(10):
        env.reset()
        for _ in range(200):
            action_n = []
            for n in range(env.nb_agents):
                targetVelocity = env.bc.readUserDebugParameter(targetVelocitySlider_n[n])
                steeringAngle = env.bc.readUserDebugParameter(steeringSlider_n[n])
                action_n.append([targetVelocity, steeringAngle])
            obs_n, reward_n, done_n, info_n = env.step(action_n)
            if done_n:
                break
        print("----------DONE--------------")
