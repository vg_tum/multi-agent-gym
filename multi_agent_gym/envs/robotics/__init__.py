from ..registration import reg_helper
"""
Multi Agent Robotics
--------------------------------

"""
_base_entry = "multi_agent_gym.envs.robotics.reaching.{f}:{c}"
_base_name = "Robotics_{}"

#
# # cooperative navigation
# reg_helper(_base_name.format("Reach"),
#            _base_entry.format(f="reach_goals", c="DyadicReach"), 2,
#            dict(nb_goals=2))
# reg_helper(_base_name.format("Reach"),
#            _base_entry.format(f="reach_goals", c="TriadicReach"), 3,
#            dict(nb_goals=2))
#
