from typing import List
import numpy as np
from gym.error import DependencyNotInstalled
try:
    import pybullet as _p
except ImportError as e:
    raise DependencyNotInstalled("{}.".format(e) +
       "(HINT: you need to install pybullet, and also perform the setup instructions here:" \
       "https://github.com/bulletphysics/bullet3/blob/master/README.md)")
from multi_agent_gym.core import MultiAgentEnv
from multi_agent_gym.envs.robotics.agents.robot_agent import ManipulationAgent
from .utils import parse_reward_type

RENDER_HEIGHT = 720
RENDER_WIDTH = 960


class MuAgRobotEnv(MultiAgentEnv):
    """
    Robot Manipulation (Mujoco based multi-agent environment)
    Motivated by and partially copied from robotics gym environments

    Description:
        Generic Multi Robot Manipulation Environment

    Agents:
        Type: ManipulationAgent
        length: nb_agents

    Args:
        agents(Sequence[ManipulationAgent]):    list of agents
        dof(int):           DoF of robot agents
        cart_dof(int):      DoF in Cartesian Space. Defaults to 2.
        n_substeps(int):    number of time-steps for a single time update in the given environment.
        render(str or None): Rendering mode. Defaults to None.
        reward_type('sparse' or 'dense') :
    """
    _model_path = None

    def __init__(self, model, agents, render_size=(RENDER_WIDTH, RENDER_WIDTH),
                 n_substeps=10, reward_type="sparse", **kwargs):
        self.metadata = {
            'render.modes': ['rgb_array'],
            'video.frames_per_second': int(np.round(1.0 / self.dt))
        }
        self._p = _p
        self._cam_dist = 1.3
        self._cam_yaw = 180
        self._cam_pitch = -40
        if self._renders:
            cid = _p.connect(_p.SHARED_MEMORY)
            if (cid < 0):
                cid = _p.connect(_p.GUI)
            _p.resetDebugVisualizerCamera(self._cam_dist, self._cam_yaw, self._cam_pitch, [0.52, -0.2, -0.33])
        else:
            _p.connect(_p.DIRECT)

        self.nb_agents = len(agents)
        self.agents = agents            # type: List[ManipulationAgent]
        self._real_time = False
        # speed up integration
        self.cur_time = 0.0
        self.vel_lim = None
        self.acc_lim = None
        self._n_sub_steps = n_substeps
        self.reward_type = parse_reward_type(reward_type)
        self.viewer = None
        self._viewers = {}
        self._render_width = render_size[0]
        self._render_height = render_size[1]
        dofs = np.max(np.array([len(ag.q) for ag in self.agents]))
        self._q_cur = np.zeros((self.nb_agents, dofs))
        self._q_dot_cur = np.zeros((self.nb_agents, dofs))
        self._x_cur = np.zeros((self.nb_agents, 3))
        self._v_cur = np.zeros((self.nb_agents, 3))
        self._quat_cur = np.zeros((self.nb_agents, 4))
        self._quat_cur[:, 0] = 1.0
        self._omega_cur = np.zeros((self.nb_agents, 3))
        self.seed()
        self.set_world(**kwargs)
        self.set_agents(**kwargs)
        self._mat_shine_orig = self.sim.model.mat_shininess.copy()

    @property
    def initial_state(self):
        s0 = self.sim.get_state()
        for a in self.agents:
            s0.qpos[a.joint_handles] = a.q0
            s0.qvel[a.joint_handles] = a.q_dot0
            try:
                s0.act[a.joint_handles[a.controlled_joints]] = a.init_act
            except TypeError:
                pass
        return s0

    def _sim_world(self):
        """
        Simply call step update function of mujoco simulation interface.
        """
        def _col_handler(ag, link, pos, o_ag=None):
            # type: (ManipulationAgent, str, np.ndarray, ManipulationAgent or None) -> None
            if ag is not None:
                lgr().debug("Collision for agent {}: with link {} of agent".format(ag, link, o_ag))
            else:
                lgr().debug("Collision for agent {}: with link {}".format(ag, link))

            if o_ag is None and o_ag != ag:
                ag.add_collision(link, pos)
            else:
                ag.add_collision(link, pos, o_ag)
        self.sim.step()
        self.sim.forward()
        for agent in self.agents:
            agent.update(self.sim)
            a_ind = int(agent)
            self._q_cur[a_ind, :] = agent.q
            self._q_dot_cur[a_ind, :] = agent.q_dot
            self._x_cur[a_ind, :] = agent.x_pos
            self._v_cur[a_ind, :] = agent.x_vel_p
            self._quat_cur[a_ind, :] = agent.x_quat
            self._omega_cur[a_ind, :] = agent.x_vel_r
        for i in range(self.sim.data.ncon):
            contact = self.sim.data.contact[i]
            l1 = self.sim.model.geom_id2name(contact.geom1)
            l2 = self.sim.model.geom_id2name(contact.geom2)
            ag0, ag1 = [None] * 2
            for a in self.agents:
                if a.is_link_of(l1):
                    ag0 = a
                elif a.is_link_of(l2):
                   ag1 = a
            if ag0 is not None:
                _col_handler(ag0, l2, contact.pos, ag1)
                lgr().debug("Contact applied force on link {}: {}".format(l2,
                                                                              self.sim.data.cfrc_ext[contact.geom2]))
            if ag1 is not None:
                _col_handler(ag1, l1, contact.pos, ag0)

            elif ag0 is None and ag1 is None:
                lgr().warning("Ignoring Collision between objects")
                continue

    def _close(self):
        """
        Close Particle Environment
        """
        self.viewer = None
        self.sim = None
        self.model = None
        self.agents = []

    # copied / pseudo inherit from RobotEnv
    @property
    def dt(self):
        return self.sim.model.opt.timestep * self.sim.nsubsteps

    def render(self, mode="rgb_array", close=False):
        """
        Copied from bullet_envs->kukaGymEnv

        Args:
            mode:
            close:

        Returns:
            np.ndarray: RGB image

        """
        if mode != "rgb_array":
            return np.array([])
        base_pos, orn = self._p.getBasePositionAndOrientation(self.agents[0].pb_id)
        view_matrix = self._p.computeViewMatrixFromYawPitchRoll(cameraTargetPosition=base_pos,
                                                                distance=self._cam_dist,
                                                                yaw=self._cam_yaw,
                                                                pitch=self._cam_pitch,
                                                                roll=0,
                                                                upAxisIndex=2)
        proj_matrix = self._p.computeProjectionMatrixFOV(fov=60,
                                                         aspect=float(RENDER_WIDTH) / RENDER_HEIGHT,
                                                         nearVal=0.1,
                                                         farVal=100.0)
        (_, _, px, _, _) = self._p.getCameraImage(width=RENDER_WIDTH,
                                                  height=RENDER_HEIGHT,
                                                  viewMatrix=view_matrix,
                                                  projectionMatrix=proj_matrix,
                                                  renderer=self._p.ER_BULLET_HARDWARE_OPENGL)
        rgb_array = np.array(px, dtype=np.uint8)
        rgb_array = np.reshape(rgb_array, (RENDER_HEIGHT, RENDER_WIDTH, 4))
        rgb_array = rgb_array[:, :, :3]
        return rgb_array

    def _get_viewer(self, mode):
        """
        Copied from RobotEnv

        Args:
            mode:
        """
        self.viewer = self._viewers.get(mode)
        if self.viewer is None:
            if mode == 'human':
                self.viewer = mujoco_py.MjViewer(self.sim)
            elif mode == 'rgb_array':
                self.viewer = mujoco_py.MjRenderContextOffscreen(self.sim, device_id=-1)
            self._viewer_setup()
            self._viewers[mode] = self.viewer
        return self.viewer

    def close(self):
        if self.viewer is not None:
            # self.viewer.finish()
            self.viewer = None
            self._viewers = {}

    def reset(self):
        # type: ()->np.ndarray
        """Resets a simulation and indicates whether or not it was successful.
        If a reset was unsuccessful (e.g. if a randomized state caused an error in the
        simulation), this method should indicate such a failure by returning False.
        In such a case, this method will be called again to attempt a the reset again.

        Returns:
            np.ndarray: concatenated array of a,lll agents observations. Last row as world state
        """
        for a in self.agents:
            a.reset()
        # simulate a dummy step update
        self.sim.set_state(self.initial_state)
        [self.sim.step() for _ in range(50)]
        self.sim.forward()
        self._sim_world()
        # record observation for each agent
        init_obs = []
        for agent in self.agents:
            ag_obs, _ = self.get_agent_update(agent)
            init_obs.append(ag_obs)
        return np.array(init_obs)

    # functions to be implemented in actual scenario specification
    def set_agents(self, **kwargs):
        self.sim.forward()
        for a in self.agents:
            a.sim = self.sim

    def set_world(self, **kwargs):
        raise NotImplementedError

    def _set_actions(self, action):
        if isinstance(action, np.ndarray) and len(action.shape) == 1:
            action = np.reshape(action, (self.nb_agents, *self.agents[0].action_space.shape))
        for n, agent in enumerate(self.agents):
            if agent.action_space is None:
                continue
            agent.set_action(action[n], self.sim)

    def get_agent_update(self, agent):
        raise NotImplementedError

    def get_world_update(self, obs_n, reward_n=None):
        return obs_n, reward_n, False, {}

    def _viewer_setup(self):
        """
        center focus on agent 0 by default
        """
        self.viewer.cam.distance = 2.5
        self.viewer.cam.azimuth = 132.0
        self.viewer.cam.elevation = -14.0
        self._render_height = 800
        self._render_width = 500

    def _render_callback(self):
        self.sim.model.mat_shininess[:] = self._mat_shine_orig.copy()[:]
        for a in self.agents:
            for link, pos in a.render_collisions:
                if link == 'None' or link is None:
                    self.viewer.add_marker(pos=pos, size=np.ones(3) * 0.02,
                                           rgba=np.ones(4) * 0.5, label="")
                elif not any([a.name in link for a in self.agents]):
                    self.viewer.add_marker(pos=pos, size=np.ones(3) * 0.02,
                                           rgba=np.array([0.0, 0.0, 0.0, 1.0]),
                                           label=link)
                else:
                    mat_id = self.sim.model.geom_matid[self.sim.model.geom_name2id(link)]
                    self.sim.model.mat_shininess[mat_id] = 1.0
            a.render_collisions = []

