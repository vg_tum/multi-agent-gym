import numpy as np
try:
    from multi_agent_gym.envs.robotics.sim_robots.mj_robot import MjRobot as ManipulationAgent
except ImportError:
    from multi_agent_gym.envs.robotics.sim_robots.pb_robot import PyBulRobot as ManipulationAgent


class ReachBot(ManipulationAgent):
    """
    Reaching Robot.

    Inherits from Racer5 robot and simplifies the implementation.
    This robot is only controlled via the first 4 joints , with only the cartesian position being of interest

    Args:
        model:                  mujoco model to be simulated
        num_id(int):            numeric id, which uniquely identifies the agent
        reward_range(tuple):    reward range for current model / environment

    """

    def __init__(self, model, num_id, robot_name, reward_range, state_limits,
                 q_dot_max=1.0, **kwargs):
        super().__init__(model=model, robot_name=robot_name, num_id=num_id,
                         reward_range=reward_range, state_limits=state_limits, **kwargs)
        self.joint_speed_limits = q_dot_max * np.ones(self.joint_limits.shape)
        self.joint_speed_limits[:, 0] *= -1
        self._achieved = []

    def set_incremental_action_space(self, dt):
        self._incremental_pos_control = True
        self.action_space.low = self.joint_speed_limits[self.controlled_joints, 0] * dt
        self.action_space.high = self.joint_speed_limits[self.controlled_joints, 1] * dt

    @property
    def info(self):
        return dict(object_collision=self.object_collision,
                    agent_collision=self.agent_collision,
                    coll_with_link=self.collision_with,
                    coll_with_agent=list(self._agent_col.values()),
                    achieved=self._achieved)

    @property
    def object_collision(self):
        # type: () -> bool
        if len(self.collision_with) > 0:
            return any([x not in self._agent_col.keys() for x in self.collision_with])
        return False

    @property
    def agent_collision(self):
        # type: () -> bool
        return len(self._agent_col) > 0

    def reset(self):
        """
        Reset agent
        """
        super().reset()
        self._goals = []
        self._achieved = []

    def add_achievement(self, g_pos):
        self._achieved.append(g_pos)


class Racer5(ReachBot, SymbolicKinematicRobot):

    def __init__(self, model, num_id, reward_range, **kwargs):
        ReachBot.__init__(self, model=model, num_id=num_id, robot_name="racer5_{}".format(num_id),
                          reward_range = reward_range, **kwargs)
        SymbolicKinematicRobot.__init__(self, 6)
        self.joint_speed_limits = np.array([400.0 / 180.0 * np.pi * np.array([-1.0, 1.0]),
                                            360.0 / 180.0 * np.pi * np.array([-1.0, 1.0]),
                                            400.0 / 180.0 * np.pi * np.array([-1.0, 1.0]),
                                            500.0 / 180.0 * np.pi * np.array([-1.0, 1.0]),
                                            500.0 / 180.0 * np.pi * np.array([-1.0, 1.0]),
                                            800.0 / 180.0 * np.pi * np.array([-1.0, 1.0])])
        self.initialize_kinematic(joint_limits=model.jnt_range[self.joint_handles],
                                  jnt_axes=model.jnt_axis[self.joint_handles],
                                  delta_links=np.array([model.body_pos[model.body_name2id(x)] for x in self.link_names])
                                  )
