import re
from abc import ABC
import numpy as np
import gym.spaces as _g_spaces
from multi_agent_gym.core import BaseAgent


class ManipulationAgent(BaseAgent, ABC):

    """
    A base robot agent in a joint manipulation scenario.

    Attributes:
        model(mujoco_py.cymj.PyMjModel)
        num_id (int):   id of agent
        max_speed(None or float): speed magnitude constraint
        max_acc(None or float): accelerational magnitude constraint
        reward_range(tuple):    reward lmiits
        ctrl_joints(np.ndarray or None): list of joints to be controlled.

    Note:
        The joints given to ctrl_joints are set according to internal kinematic enumerations
        and not to the global numbering
    """

    def __init__(self, model, robot_name, num_id,
                 max_speed=np.inf, max_acc=np.inf,
                 sample_state=False,
                 state_limits=(-np.inf, np.inf),
                 reward_range=(-10.0, 0.0),
                 ctrl_joints=None, **kwargs):
        self.sim = None         # type: None or mujoco_py.MjSim
        self._id = num_id       # type: int
        self._name = robot_name
        self.world_base_tf = np.eye(4)
        name_filter = re.compile('{}_joint*'.format(robot_name))
        link_name_filter = re.compile('{}_.*link*'.format(robot_name))
        self._joint_filter = np.vectorize(lambda x: bool(name_filter.match(x)))
        self.joint_names = list(filter(lambda x: bool(name_filter.match(x)), model.joint_names))
        self.link_names = list(filter(lambda x: bool(link_name_filter.match(x)), model.body_names))
        self.ee_name = self.link_names[-1]
        self.joint_handles = np.where(self._joint_filter(model.joint_names))[0]
        self._q0 = model.qpos0[self.joint_handles]
        self.max_v = max_speed  # type: float
        self.max_a = max_acc    # type: float
        if ctrl_joints is None:
            self.controlled_joints = range(0, len(self.joint_handles))
        else:
            self.controlled_joints = ctrl_joints
        self.joint_limits = model.jnt_range[self.joint_handles]
        self.joint_speed_limits = np.ones(self.joint_limits.shape)
        self.joint_speed_limits[:, 0] *= -1
        ctrl_limits = (self.joint_limits[self.controlled_joints, 0],
                       self.joint_limits[self.controlled_joints, 1])
        assert reward_range[0] <= reward_range[1], "reward range needs to be a range (wow)"
        self.reward_range = reward_range  # type: tuple
        assert len(ctrl_limits) == 2 and isinstance(ctrl_limits, tuple),\
            "control limits have to be a tuple of length 2, provided: {} instead".format(ctrl_limits)
        assert len(state_limits) == 2 and isinstance(state_limits, tuple),\
            "control limits have to be a tuple of length 2, provided: {} instead".format(state_limits)
        assert ctrl_limits[0].shape == ctrl_limits[1].shape, "cannot set control limits " \
                                                             "with different dimensions: " \
                                                             "[{} -> {}]".format(ctrl_limits[0], ctrl_limits[1])
        self.action_space = _g_spaces.Box(low=ctrl_limits[0], high=ctrl_limits[1], dtype=np.float32)
        # observation space
        #if isinstance(state_limits[0], np.ndarray):
        #    assert state_limits[0].shape == state_limits[1].shape, "cannot set state limits " \
        #                                                         "with different dimensions: " \
        #                                                         "[{} -> {}]".format(state_limits[0], state_limits[1])
        #    self.state_space = _g_spaces.Box(low=state_limits[0], high=state_limits[1], dtype=np.float32)
        # else:
        self.x_pos = np.zeros(3)
        self.x_quat = np.array([0.0, 0.0, 0.0, 1.0])
        self.x_vel_p= np.zeros(3)
        self.x_vel_r = np.zeros(3)
        self.q = self._q0
        self.q_prev = self._q0.copy()
        self.q_dot = np.zeros(self._q0.shape)
        self.action = np.zeros(self.action_space.shape)
        self.state_space = None
        self._goals = []
        self._col_pos = []
        self.collision_with = []
        self.render_collisions = []
        self._agent_col = dict()
        self._incremental_pos_control = False
        self.sample_state = sample_state
        self.update_state_limits()

    @property
    def q0(self):
        if self.sample_state:
            return np.random.uniform(self.joint_limits[:,0],
                                     self.joint_limits[:,1])
        else:
            return self._q0.copy()

    @property
    def q_dot0(self):
        return np.zeros(self._q0.shape)

    @property
    def init_act(self):
        if self._incremental_pos_control:
            return np.zeros(len(self.controlled_joints))
        else:
            return self._q0[self.controlled_joints]

    @property
    def state(self):
        # return np.append(np.append(self.q self.q_dot), self.x_pos)
        return self.q[self.controlled_joints]

    def state_to_obs(self):
        self.obs[0:self.state_space.shape[0]] = self.state
        # self.obs[len(self.q): len(self.q) + len(self.q_dot)] = self.q_dot
        # self.obs[len(self.q) + len(self.q_dot): len(self.q) + len(self.q_dot) + len(self.x_pos)] = self.x_pos

    def update_state_limits(self):
        self.state_space = _g_spaces.Box(low=self.joint_limits[self.controlled_joints, 0],
                                       high=self.joint_limits[self.controlled_joints, 1])
        #_g_spaces.Box(low=np.append(np.append(self.joint_limits[:, 0], self.joint_speed_limits[:, 0]),
        #                           self.world_base_tf[3, 0:3].T - np.array([0.5, 0.5, 0.5])),
        #             high=np.append(np.append(self.joint_limits[:, 1], self.joint_speed_limits[:, 1]),
        #                            self.world_base_tf[3, 0:3].T + np.array([0.5, 0.5, 0.5])),
        #             dtype=np.float32)

    @property
    def name(self):
        # type: ()->str
        return self._name

    @property
    def type_name(self):
        return str(type(self))

    def ___str__(self):
        return self._name

    def __repr__(self):
        return self._name

    def set_action(self, action, time=None):
        # type: (np.ndarray, float or None)-> np.ndarray
        """
        Set action for agent given the current action space and limits

        Args:
            action(np.ndaray):  action of agent
            time(float or None):  time for which action will be applied. optional. Defaults to None

        Note:
            time not implemented atm.
        Returns:
             np.ndarray: action assigned
        """
        self.action = np.zeros(self.action_space.shape)
        if action.shape == self.action_space.shape:
            tmp_action = action
        else:
            tmp_action = action[self.controlled_joints]
        self.action = np.clip(tmp_action, self.action_space.low, self.action_space.high)
        if self._incremental_pos_control:
            tmp = self.q[self.controlled_joints] + self.action
            for q in self.controlled_joints:
                if self.action[q] == 0:
                    tmp[q] = self.sim.data.ctrl[self.joint_handles[q]]
            self.sim.data.ctrl[self.joint_handles[self.controlled_joints]] = \
                np.clip(tmp,
                       self.joint_limits[self.controlled_joints, 0],
                       self.joint_limits[self.controlled_joints, 1])
        else:
            self.sim.data.ctrl[self.joint_handles[self.controlled_joints]] = self.action
        return self.action

    def update(self, sim):
        # type: (mujoco_py.MjSim) -> None
        #        self.q = sim.data
        self.q_prev = self.q.copy()
        for ind, x in enumerate(self.joint_names):
            self.q[ind] = sim.get_state().qpos[sim.model.joint_name2id(x)]
            self.q_dot[ind] = sim.get_state().qvel[sim.model.joint_name2id(x)]
        self.x_pos = sim.data.get_body_xpos(self.link_names[-1])
        self.x_quat = sim.data.get_body_xquat(self.link_names[-1])
        self.x_vel_p = sim.data.get_body_xvelp(self.link_names[-1])
        self.x_vel_r = sim.data.get_body_xvelr(self.link_names[-1])

    def is_link_of(self, link):
        # type: (str) -> bool
        return link in self.link_names

    def end_step(self):
        self.render_collisions = [(l, p) for l, p in zip(self.collision_with, self._col_pos)]
        self._col_pos = []
        self.collision_with = []
        self._agent_col = dict()

    def add_collision(self, link_name, pos, agent=None):
        # type: (str, np.ndarray, int or str or None) -> bool
        if link_name in self.collision_with:
            return False
        self.collision_with.append(link_name)
        if len(self._col_pos) > 0:
            self._col_pos = np.append(self._col_pos,
                                      [pos], axis=0)
        else:
            self._col_pos = np.array([pos])
        if agent is not None:
            self._agent_col[link_name] = int(agent)
        return True

    def reset(self):
        self.end_step()
        self.q = self.q0
        self.q_dot = self.q_dot0
        self.action = self.init_act
        self.sim.data.ctrl[self.joint_handles[self.controlled_joints]] = self.q[self.controlled_joints]


