import os
from itertools import permutations
from typing import Dict, List, Tuple
import numpy as np
import scipy.optimize
import scipy.spatial.transform as scipy_tf
from gym import error

try:
    import mujoco_py
except (ImportError, ModuleNotFoundError) as e:
    raise error.DependencyNotInstalled("{}.".format(e) +
                                       "(HINT: you need to install mujoco_py, and also perform the setup instructions here:" \
                                       "https://github.com/openai/mujoco-py/.)")


class ContactMsr(object):

    def __init__(self):
        self.pos = np.zeros(3)
        self.quat = np.array([1.0, 0.0, 0.0, 0.0])
        self.f = np.zeros(3)
        self.tau = np.zeros(3)
        self.d = 0.0
        self.collision_with = None

    def __str__(self):
        return "Contact at p:{p} q:{q} with {o}.\nWith a 6D-force: {f}{t} and a displacement of {d}".format(
            p=self.pos, q=self.quat, o=self.collision_with, f=self.f, t=self.tau, d=self.d
        )


def get_contact(sim_handle: mujoco_py.MjSim, obj_name: str, debug: bool = False) -> List[ContactMsr]:
    r"""
    get contact measurement of body named 'obj_name'

    Args:
        sim_handle (mujoco_py.MjSim):  simulation handle
        obj_name(str):  name string of the selected object
        debug(bool): debug flag to check if object name is known to simulation
    Returns:
        ContactMsr or None: contact data between object

    Todo:
        compare with alternatives

        .. python:

            geom2_body = sim_handle.model.geom_bodyid[sim_handle.data.contact[i].geom2]
            print(' Contact force on geom2 body', sim_handle.data.cfrc_ext[geom2_body])
            print('norm', np.sqrt(np.sum(np.square(sim_handle.data.cfrc_ext[geom2_body]))))
    """
    if debug:
        assert obj_name in sim_handle.model.body_names, \
            "provide a feasible object name, {}. You can choose from{}".format(obj_name, sim_handle.model.body_names)
    all_contacts = []
    cur_id = sim_handle.model.geom_name2id(obj_name)
    for i in range(sim_handle.data.ncon):
        con = sim_handle.data.contact[i]
        if cur_id == con.geom1:
            found_contact = ContactMsr()
            found_contact.collision_with = sim_handle.model.geom_id2name(con.geom2)
        elif cur_id == con.geom2:
            found_contact = ContactMsr()
            found_contact.collision_with = sim_handle.model.geom_id2name(con.geom1)
        else:
            continue
        wrench_vector = np.zeros(6, dtype=np.float64)
        mujoco_py.functions.mj_contactForce(sim_handle.model, sim_handle.data, i, wrench_vector)
        found_contact.d = con.dist
        found_contact.f = wrench_vector[:3]
        found_contact.tau = wrench_vector[3:]
        found_contact.pos[0:3] = con.pos[0:3]
        all_contacts.append(found_contact)
    return all_contacts


def get_asset_path():
    # type: () -> str
    """
    Get path to mujoco assets.

    Returns:
        str: path to general assets as str
    Raises:
        RuntimeError: if asset path does not exist
    """
    return os.path.join(os.path.dirname(os.path.abspath(__file__)), "assets")


def load_model_helper(model_name, scenario="joint_recycle"):
    # type: (str, str) -> object
    """
    Load model  if it exists

    Args:
        model_name(str):    name of xml file
        scenario(str):      name of scenario / directory under asset path. Defaults to "joint_recycle".
    Returns:
        object: mujoco_py model. Adjust proper object type here
    """
    full_path = os.path.join(get_asset_path(), scenario)
    assert os.path.exists(full_path), "scenario {} is not listed in available environments.".format(scenario) + \
                                      "Asset path set to {}".format(get_asset_path())
    full_path = os.path.join(full_path, "{}.xml".format(model_name))
    assert os.path.exists(full_path), "Model {} does not exist".format(full_path)
    return mujoco_py.load_model_from_path(full_path)


def parse_reward_type(set_mode):
    """
    Parse reward type upon initialization.

    Args:
        set_mode(str):  desired reward choice
    Returns:
        str: legal identifier
    Raises:
         TypeError: if ``set_mode`` is set to an invalid string
         AssertionError: if non-string is provided
    """
    assert isinstance(set_mode, str), "please provide a string based reward type"
    if set_mode in ['sparse', 'dense']:
        return set_mode
    else:
        if set_mode in [''.join(p) for p in permutations('sparse')]:
            return 'sparse'
        if set_mode in [''.join(p) for p in permutations('dense')]:
            return 'dense'
    raise TypeError("reward mode {} is not supported".format(set_mode))


def get_homog_from_sim(sim, typ, name):
    # type: (mujoco_py.MjSim, str, str) -> np.ndarray
    out = np.zeros((4, 4))
    out[3, 3] = 1.0
    if "body" in typ.lower():
        out[0:3, 0:3] = sim.data.get_body_xmat(name)
        out[0:3, 3] = sim.data.get_body_xpos(name)
    elif "site" in typ.lower():
        out[0:3, 0:3] = sim.data.get_site_xmat(name)
        out[0:3, 3] = sim.data.get_site_xpos(name)
    elif "geom" in typ.lower():
        out[0:3, 0:3] = sim.data.get_geom_xmat(name)
        out[0:3, 3] = sim.data.get_geom_xpos(name)
    else:
        raise TypeError("Please provide a typ in the range of {}. Got {}".format(("body", "site", "geom"),
                                                                                 typ))
    return out


def inverse_tf_mat(tf_in):
    # type:(np.ndarray) -> np.ndarray
    assert tf_in.shape == (4, 4), "false matrix shape"
    assert tf_in[3, 3] != 0.0, "scaling needs to be >=0.0"
    tf_out: None = tf_in.copy()
    tf_out[0:3, 3] = -tf_in[0:3, 3]
    tf_out[0:3, 0:3] = tf_in[0:3, 0:3].T
    tf_out[3, 3] = 1.0 / tf_in[3, 3]
    return tf_out


def simple_pos_ik(agent, goal_point):
    """
    Most Simplest IK solver

    Args:
        agent:
        goal_point:

    Returns:

    """

    def _opt_func(q):
        tmp = agent.forward_kin(q)
        return np.linalg.norm(goal_point - tmp[0:3, 3])

    res = scipy.optimize.minimize(_opt_func, x0=agent.q)
    if res.fun < 1e-6:
        return True, res.x
    else:
        return False, res.x


def rotm2quat(rot_mat):
    # type: (np.ndarray or scipy_tf.Rotation) -> np.ndarray
    if not isinstance(rot_mat, scipy_tf.Rotation):
        rot_mat = scipy_tf.Rotation.from_dcm(rot_mat)
    return rot_mat.as_quat()


def _quat_as_Rotation(q):
    # type: (np.ndarray or scipy_tf.Rotation) -> scipy_tf.Rotation
    if not isinstance(q, scipy_tf.Rotation):
        r = scipy_tf.Rotation(q)
    else:
        r = q
    return r


def quat2rotm(quat):
    # type: (np.ndarray or scipy_tf.Rotation) -> np.ndarray
    return _quat_as_Rotation(quat).as_dcm()


def quat2eul(quat, seq):
    # type: (np.ndarray or scipy_tf.Rotation, str) -> np.ndarray
    return _quat_as_Rotation(quat).as_euler(seq=seq)


def get_roll_matrix(theta, sign=1):
    if sign >= 0:
        return sp.Matrix([[1, 0, 0],
                          [0, sp.cos(theta), -sp.sin(theta)],
                          [0, sp.sin(theta), sp.cos(theta)]
                          ])
    else:
        return sp.Matrix([[1, 0, 0],
                          [0, sp.cos(theta), sp.sin(theta)],
                          [0, -sp.sin(theta), sp.cos(theta)]
                          ])


def get_pitch_matrix(omega, sign=1):
    if sign >= 0:
        return sp.Matrix([[sp.cos(omega), 0, sp.sin(omega)],
                          [0, 1, 0],
                          [-sp.sin(omega), 0, sp.cos(omega)]])
    else:
        return sp.Matrix([[sp.cos(omega), 0, -sp.sin(omega)],
                          [0, 1, 0],
                          [sp.sin(omega), 0, sp.cos(omega)]])


def get_yaw_matrix(psi, sign=1):
    if sign >= 0:
        return sp.Matrix([[sp.cos(psi), -sp.sin(psi), 0],
                          [sp.sin(psi), sp.cos(psi), 0],
                          [0, 0, 1]])
    else:
        return sp.Matrix([[sp.cos(psi), sp.sin(psi), 0],
                          [-sp.sin(psi), sp.cos(psi), 0],
                          [0, 0, 1]])


def generate_homog_tf(q, d_p, axis=np.array((0, 0, 1))):
    if not isinstance(axis, np.ndarray):
        axis = np.array(axis)
    assert axis.shape != (3,) or np.sum(np.abs(axis)) == 1, "Illegal axis given: {} ".format(axis)
    if axis[0] != 0:
        transform = get_roll_matrix(q, axis[0]).col_insert(3, sp.Matrix(d_p))
    elif axis[1] != 0:
        transform = get_pitch_matrix(q, axis[1]).col_insert(3, sp.Matrix(d_p))
    elif axis[2] != 0:
        transform = get_yaw_matrix(q, axis[2]).col_insert(3, sp.Matrix(d_p))
    else:
        raise TypeError("Axis messed up: {} ".format(axis))
    return transform.row_insert(3, sp.Matrix([[0, 0, 0, 1]]))
