r"""
Multi Agent Gym Reaching Instances
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Actual instances available to the multi-agent-gym registry.
All the classes listed below inherit the base class from :py:mod:`~.reach_env`.
All environments are set up equally and simply differ by the amount of agents within these environments
As agents, COMAU Racer 5 0.80 robot manipulators are implemented, which provide a joint position interface with 6
degrees of freedom (DoFs). Due to the homogeneous team composition, all environment instances
take the same input variables, which are shortly outlined in here

* The ``ctrl_joints`` variable allows the environment to select, which joints are actuated per robot.
  The argument is provided as a np.ndarray or None. If None is provided, all joints are actuated, i.e. all 6 DoF in the
  case of the COMAU Racer 5 0.80.
* The ``reward_range`` sets the limit for each individual reward function. Due to the setup, it is not possible to set
  limits individually per agent for now.
* The ``num_goals`` argument sets the number of goals, which is for now clipped by the number of agents, but may be
  changed at a later stage. Nonetheless, the current method would require to add new sites if the number of goals
  exceeds the number of available sites from the loaded model-xml.
* The ``cart_dof`` argument is provided as an integer, which is expected to be in the interval of :math:`x \in [1, 6]`
  For the ease of use, the cartesian state space is further reduced to position only, as the reaching environment in
  free space is mainly focusing on learning reaching motions without explicitly taking orientations into account.

Further arguments for dedicated parent / base classes can be looked up at :py:mod:`~.reach_env`.

Note:
    * The current implementation does not allow to actuate agents differently, e.g. agent 1 with joint 1-3 and agent 2
      with joint 5 and 6.
    * Increasing the cartesian DoFs has not been tested.
    * The state limits for the cartesian state space may need improvements as they have chosen pretty arbitrarily...
"""
import numpy as np
import gym.spaces as _g_spaces
import multi_agent_gym.utils as _mag_utils
from ..utils import load_model_helper
from ..agents.reach_agents import Racer5
from .reach_env import BaseReach

lgr = _mag_utils.get_logger("single_reach")


class SingleReach(BaseReach):
    """
    Not really a Multi agent as number of agent is set to 1, but can serve as a comparison to single agent RL algorithms

    Loads model dyadic_reach.xml in the ``single_recycle`` asset-path.

    args:
        reward_range(tuple): defaults to -10.0, 0.0),
        num_goals(int):  defaults to 2,
        ctrl_joints(np.ndarray or none):  defaults to np.array([0,1,2,3]), i.e. actuating joints 1-4.
        cart_dof(int): defaults to 3.
    """
    nb_agents = 1

    def __init__(self, reward_range=(-10.0, 0.0), num_goals=2,
                 ctrl_joints=np.array([0, 1, 2, 3]), cart_dof=3,
                 **kwargs):
        model = load_model_helper("single_reach", scenario="joint_recycle")
        num_goals = min(num_goals, self.nb_agents)
        cart_dof = min(cart_dof, 3)
        agents = [Racer5(model=model, num_id=i, reward_range=reward_range,
                         ctrl_joints=ctrl_joints,
                         state_limits=(-np.inf * np.ones(cart_dof), np.inf * np.ones(cart_dof),))
                  for i in range(self.nb_agents)]
        self.world_state_space = _g_spaces.Box(low=-1.5, high=1.5,
                                               shape=(num_goals, cart_dof), dtype=np.float32)
        super(SingleReach, self).__init__(model, agents,
                                          num_goals=num_goals,
                                          n_substeps=10,
                                          **kwargs)


class DyadicReach(BaseReach):
    """
    Dyadic setup of two identical COMAU Racer 5 0.80 robots. The environment is registered with the default values.
    Loads model dyadic_reach.xml in the ``joint_recycle`` asset-path.

    Args:
        reward_range(tuple): defaults to -10.0, 0.0),
        num_goals(int):  defaults to 2,
        ctrl_joints(np.ndarray or none):  defaults to np.array([0,1,2,3]), i.e. actuating joints 1-4.
        cart_dof(int): defaults to 3.
    """
    nb_agents = 2

    def __init__(self, reward_range=(-10.0, 0.0), num_goals=2,
                 ctrl_joints=np.array([0, 1, 2, 3]), cart_dof=3, **kwargs):
        model = load_model_helper("dyadic_reach", scenario="joint_recycle")
        num_goals = min(num_goals, self.nb_agents)
        cart_dof = min(cart_dof, 3)
        agents = [Racer5(model=model, num_id=i, reward_range=reward_range,
                         ctrl_joints=ctrl_joints,
                         state_limits=(-np.inf * np.ones(cart_dof), np.inf * np.ones(cart_dof),)) for i in
                  range(self.nb_agents)]
        self.world_state_space = _g_spaces.Box(low=-1.5, high=1.5,
                                               shape=(num_goals, cart_dof), dtype=np.float32)
        super(DyadicReach, self).__init__(model, agents,
                                          num_goals=num_goals,
                                          n_substeps=10,
                                          **kwargs)


class TriadicReach(BaseReach):
    """
    Dyadic setup of two identical COMAU Racer 5 0.80 robots. The environment is registered with the default values.
    Loads model dyadic_reach.xml in the ``triadic_recycle`` asset-path.

    Args:
        reward_range(tuple): defaults to -10.0, 0.0),
        num_goals(int):  defaults to 5,
        ctrl_joints(np.ndarray or none):  defaults to np.array([0,1,2,3]), i.e. actuating joints 1-4.
        cart_dof(int): defaults to 3.
    """
    nb_agents = 3

    def __init__(self, reward_range=(-10.0, 0.0), num_goals=3,
                 ctrl_joints=np.array([0, 1, 2, 3]), cart_dof=3,
                 log=False, **kwargs):
        model = load_model_helper("triadic_reach", scenario="joint_recycle")
        num_goals = min(num_goals, self.nb_agents)
        cart_dof = min(cart_dof, 3)
        agents = [Racer5(model=model, num_id=i, reward_range=reward_range,
                         ctrl_joints=ctrl_joints,
                         state_limits=(-np.inf * np.ones(cart_dof), np.inf * np.ones(cart_dof),)
                         ) for i in range(self.nb_agents)]
        self.world_state_space = _g_spaces.Box(low=-1.5, high=1.5, shape=(num_goals, cart_dof), dtype=np.float32)
        super(TriadicReach, self).__init__(model, agents,
                                           num_goals=num_goals,
                                           n_substeps=10,
                                           **kwargs)
