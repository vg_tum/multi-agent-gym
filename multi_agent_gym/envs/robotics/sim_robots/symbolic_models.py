import os
import pickle
from typing import Callable, List
import numpy as np
import sympy as _sp
import scipy.optimize
from scipy.spatial.transform import Rotation
from .utils import calc_rot_error, is_ipython


def get_roll_matrix(theta, sign=1):
    if sign >= 0:
        return _sp.Matrix([[1, 0, 0],
                           [0, _sp.cos(theta), -_sp.sin(theta)],
                           [0, _sp.sin(theta), _sp.cos(theta)]
                           ])
    else:
        return _sp.Matrix([[1, 0, 0],
                           [0, _sp.cos(theta), _sp.sin(theta)],
                           [0, -_sp.sin(theta), _sp.cos(theta)]
                           ])


def get_pitch_matrix(omega, sign=1):
    if sign >= 0:
        return _sp.Matrix([[_sp.cos(omega), 0, _sp.sin(omega)],
                           [0, 1, 0],
                           [-_sp.sin(omega), 0, _sp.cos(omega)]])
    else:
        return _sp.Matrix([[_sp.cos(omega), 0, -_sp.sin(omega)],
                           [0, 1, 0],
                           [_sp.sin(omega), 0, _sp.cos(omega)]])


def get_yaw_matrix(psi, sign=1):
    if sign >= 0:
        return _sp.Matrix([[_sp.cos(psi), -_sp.sin(psi), 0],
                           [_sp.sin(psi), _sp.cos(psi), 0],
                           [0, 0, 1]])
    else:
        return _sp.Matrix([[_sp.cos(psi), _sp.sin(psi), 0],
                           [-_sp.sin(psi), _sp.cos(psi), 0],
                           [0, 0, 1]])


def generate_homogeneous_tf(q, d_p, axis=np.array((0, 0, 1))):
    if not isinstance(axis, np.ndarray):
        axis = np.array(axis)
    assert axis.shape != (3,) or np.sum(np.abs(axis)) == 1, "Illegal axis given: {} ".format(axis)
    if axis[0] != 0:
        transform = get_roll_matrix(q, axis[0]).col_insert(3, _sp.Matrix(d_p))
    elif axis[1] != 0:
        transform = get_pitch_matrix(q, axis[1]).col_insert(3, _sp.Matrix(d_p))
    elif axis[2] != 0:
        transform = get_yaw_matrix(q, axis[2]).col_insert(3, _sp.Matrix(d_p))
    else:
        raise TypeError("Axis messed up: {} ".format(axis))
    return transform.row_insert(3, _sp.Matrix([[0, 0, 0, 1]]))


class SymbolicKinematicRobot(object):
    """
    Robot kinematics handler.

    Serves as a base class that reads out kinematic chains from mujoco and generates
    symbolic forward kinematics and jacobian.
    """

    def __init__(self, n_dof: int, **kwargs):
        self.q_sym = [_sp.Symbol('q{}'.format(i), real=True) for i in range(n_dof)]
        self.joint_limits = None
        self.delta_links = None
        self._jnt_axes = np.array([])
        self._jacobian_chain = {}
        self._tf_chain = {}
        self._tfs = {}
        # ik-content
        self.ee_rotation = _sp.Matrix([[_sp.Symbol('r1{}'.format(i + 1), real=True) for i in range(3)],
                                       [_sp.Symbol('r2{}'.format(i + 1), real=True) for i in range(3)],
                                       [_sp.Symbol('r3{}'.format(i + 1), real=True) for i in range(3)]])
        self.ee_translation = _sp.Matrix([_sp.Symbol(x, real=True) for x in ['x', 'y', 'z']])

    @staticmethod
    def _print_helper(text, sym_var):

        if is_ipython():
            from IPython.display import Latex, display
            display((Latex("{}$${}$$.".format(text, _sp.latex(sym_var))),))
        else:
            print("{}$${}$$.".format(text, _sp.pprint(sym_var)))

    def initialize_kinematic(self, joint_limits, delta_links,
                             jnt_axes):
        """ 
        Sort of an initialization function that sets the kinematic tree components, i.e.:
        * link displacements
        * joint axes
        * joint limits
        * frame transformation 
        """
        if self.joint_limits is not None and self.delta_links is not None:
            return
        self.joint_limits = joint_limits
        self._jnt_axes = jnt_axes
        if delta_links.shape[0] == len(self._jnt_axes) + 1:
            delta_links = delta_links[1:, :]
        self.delta_links = delta_links
        for q, ax, d_p in zip(self.q_sym, self._jnt_axes, self.delta_links):
            i = self.q_sym.index(q)
            if i not in self._tfs.keys():
                self._tfs[i] = generate_homogeneous_tf(q, d_p, ax)

    def calc_transform(self, ref_frame_num: int = 0, target_frame_num=None):
        r"""
        Calculate transformation from one frame to the next using homogeneous coordinate transformations.
        Returns symbolic representaion and defaults to generating the homogeneous coordinates of
        the endeffector frame ( None -> end of chain) in the base frame (0).

        See below for some example use cases:

        .. code-block:: python

            q = np.zeros(len(kinematic_model.q_sym))
            kinematic_model.calc_transform(0, 3, False)

        would return the symbolic forward kinematics from link 0 -> link 3.

        Args:
            ref_frame_num(int): reference frame. Defaults to (0)
            target_frame_num(int or None): Target frame of which the transformation needs to be calculated.
            Defaults to None.

        Returns:
            object: symbolic transformation
        """
        assert ref_frame_num <= len(self._tfs), "reference frame is out of scope"
        if target_frame_num is None or target_frame_num >= len(self._tfs):
            target_frame_num = len(self._tfs)
        transform = self.transform(ref_frame_num)
        ref_frame_num += 1
        if ref_frame_num >= target_frame_num:
            return transform
        for t in range(ref_frame_num, target_frame_num):
            transform *= self.transform(t)
        return transform

    def calc_jac(self, base_frame: int = 0, target_frame: int or None = None):
        r"""
        Calculate symbolic Jacobian. It defaults to calculating the full body jacobian 

        .. code-block:: python:

            q = np.zeros(len(kinematic_model.q_sym))
            kinematic_model.calc_transform(0, 3)

        would return the dedicated symbolic matrix.

        Args:
            base_frame(int): base frame for dedicated kinematic chain. Defaults to (0)
            target_frame(int or None): Target frame of which the transformation needs to be calculated.
            Defaults to None.

        Returns:
            object: ymbolic jacobian
        """
        if target_frame is None:
            target_frame = len(self.q_sym)
        else:
            target_frame = min(target_frame, len(self.q_sym))
        assert 0 <= base_frame < target_frame, "cannot compute jacobian from {}->{}".format(base_frame, target_frame)
        transform = (self.calc_transform(base_frame, target_frame) * _sp.Matrix([0, 0, 0, 1]))[:, 0]
        cur_jac = []
        # calculate derivative of (x,y,z) wrt to each joint
        for i in range(len(self.q_sym)):
            d_x_dq = [transform[0].diff(self.q_sym[i]),  # dx/dq[i]
                      transform[1].diff(self.q_sym[i]),  # dy/dq[i]
                      transform[2].diff(self.q_sym[i])]  # dz/dq[i]
            if base_frame <= i < target_frame:
                d_x_dq = d_x_dq + list(self._jnt_axes[i, :])
            else:
                d_x_dq = d_x_dq + [0, 0, 0]
            cur_jac.append(d_x_dq)
        cur_jac = _sp.Matrix(cur_jac).T  # correct the orientation of J
        return cur_jac

    def jacobian(self, q, base=0, target=None):
        """
        Calculates the numeric jacobian matrix describing the relation of cartesian speed of frame ``target`` 
        in the ``base`` frame to the joint speeds of the dedicated kinematic chain.
        The frames are provided as integer numbers and the used to calculate the jacobian via the 
        `:pymeth:self.calc_jacobian` function. The usage is as shown below
        
        .. code-block:: python

            q = np.zeros(len(kinematic_model.q_sym))
            kinematic_model.calc_jac(q)

        for the default full chain jacobian. Or for a specific chain:

        .. code-block:: python

            q = np.zeros(len(kinematic_model.q_sym))
            kinematic_model.calc_jac(q, 0, 3)

        for the system from the base link to link3.

        Args:
            q(np.ndarray):  current joint vector
            base(int):  base frame number in the current kinematic chain. Defaults to 0
            target(int or None): target frame number in the current kinematic chain. Defaults to None, i.e. chain end.

        Returns:
            np.ndarray: jacobian matrix for current joint configuration.
        """
        if target is None:
            target = len(self._tfs)
        tree = tuple([base, target])
        if tree not in self._jacobian_chain.keys():
            self._jacobian_chain[tree] = _sp.lambdify(self.q_sym, self.calc_jac(base, target))
        return self._jacobian_chain[tree](*q).T

    def forward_kin(self, q, base=0, target=None):
        """
        Calculate the forward kinematic to receive the current pose of link `target` in the reference frame of
        link `base`, given the current joint vector `q`.
        To be used as:


        .. code-block:: python

            q = np.random.rand(len(kinematic_model.q_sym))
            kinematic_model.calc_transform(q)

        for the full forward kinematics of the robot. Whereas:

        .. code-block:: python

            q = np.random.rand(len(kinematic_model.q_sym))
            kinematic_model.calc_transform(q, 1, 3)

        would return the dedicated rotation and translation of link3 in the coordinates of the frame 1.

        Args:
            q(np.ndarray):  current joint vector
            base(int):  base frame number in the current kinematic chain. Defaults to 0
            target(int or None): target frame number in the current kinematic chain. Defaults to None, i.e. chain end.

        Returns:
            np.ndarray: full endeffector pose in base frame for current joint configuration q.
        """
        if target is None:
            target = len(self._tfs)
        tree = tuple([base, target])
        if tree not in self._tf_chain.keys():
            self._tf_chain[tree] = _sp.lambdify(self.q_sym, self.calc_transform(base, target))
        return self._tf_chain[tree](*q)

    def transform(self, frame_num):
        """
        Get the symbolic coordinate transformation from the the current to the next link.

        Args:
            frame_num(int):
        Returns:
            _sp.Matrix: symbolic link transformation
        """
        frame_num = max(0, min(frame_num, len(self._tfs)))
        return self._tfs[frame_num]


def numeric_pos_ik(q_cur: np.ndarray, goal_point: np.ndarray,
                   fk_handle: Callable[[np.ndarray, int, int or None], np.ndarray],
                   threshold: float = 1e-6, base_frame: int = 0, tip_frame: int or None = None):
    """
    Most Simplest IK solver using gradient descent optimization for position only goals

    Args:
        q_cur (np.ndarray): current joint position vector
        goal_point (np.ndarray) : 3d goal vector to reach
        fk_handle(function): forward kinematic of robot as a function handle. Defined as above.
        threshold(float): convergence limit to declare success
        base_frame(int): base frame for forward kinematic. Defaults to 0.
        tip_frame(int or None): tip frame for forward kinematics. Defaults to None
    Returns:
        (bool, np.ndarray): True, if optimization has converged. joint vector that solved optimization.
    """

    def _opt_func(q):
        return np.linalg.norm(goal_point - fk_handle(q, base_frame, tip_frame)[0:3, 3])

    res = scipy.optimize.minimize(_opt_func, x0=q_cur)
    if res.fun < threshold:
        return True, res.x
    else:
        return False, res.x


def numeric_pose_ik(q_cur: np.ndarray, goal_pose: np.ndarray,
                    fk_handle: Callable[[np.ndarray, int, int or None], np.ndarray],
                    threshold: float = 1e-6, base_frame: int = 0, tip_frame: int or None = None):
    """
    Simple cartesian pose IK solver via direct optimization

    Args:
        q_cur (np.ndarray): current joint position vector
        goal_pose (np.ndarray) : 3d pose matrix to reach
        fk_handle(function): forward kinematic of robot as a function handle. Defined as above.
        threshold(float): convergence limit to declare success
        base_frame(int): base frame for forward kinematic. Defaults to 0.
        tip_frame(int or None): tip frame for forward kinematics. Defaults to None
    Returns:
        (bool, np.ndarray): True, if optimization has converged. joint vector that solved optimization.
    """
    rot_mat = Rotation.from_dcm(goal_pose[0:3, 0:3]).as_dcm()

    def _opt_func(q):
        pose_q = fk_handle(q, base_frame, tip_frame)
        theta = calc_rot_error(rot_mat, pose_q[:3, :3])
        return np.linalg.norm(goal_pose[0:3, 3] - pose_q[0:3, 3]) + np.linalg.norm(np.rad2deg(theta))

    res = scipy.optimize.minimize(_opt_func, x0=q_cur)
    if res.fun < threshold:
        return True, res.x
    else:
        return False, res.x


class SymbolicRacerCobot(SymbolicKinematicRobot):
    """"
    Extension of the general kinematic Robot that furthermore obtains a symbolic inverse kinematic,
    exploiting the structural knowledge of the robot.
    """
    def __init__(self, **kwargs):
        super().__init__(6)
        self.pos_ik_sol = {i: list() for i in range(3)}
        self.rot_ik_sol = None
        self.calc_rot_mat = None
        self.q0_ik = [None, None]  # type: List[Callable[[np.ndarray, np.ndarray, np.ndarray], float] or None]
        self.q2_ik = [None for _ in
                      range(4)]  # type: List[Callable[[np.ndarray, np.ndarray, np.ndarray], float] or None]
        self.q1_ik = [None for _ in
                      range(8)]  # type: List[Callable[[np.ndarray, np.ndarray, np.ndarray], float] or None]
        self.rot_ik_fcn = None  # type: Callable[[np.ndarray, np.ndarray, np.ndarray], np.ndarray] or None

    def calc_transl_ik(self, verbose=0) -> None:
        """
        Calculate s
        """
        r_B_OW_cart = self.ee_translation - self.ee_rotation * _sp.Matrix(self.delta_links[-1])
        r_B_OW = self.calc_transform(0, 5)[0:3, 3]
        q0 = _sp.atan2(r_B_OW_cart[1], r_B_OW_cart[0])
        self.pos_ik_sol[0] = [q0, q0 + _sp.pi]
        if verbose > 0:
            self._print_helper("Obtained solution for q0:", self.pos_ik_sol[0][0])
            self._print_helper("Obtained solution for q0 + pi:", self.pos_ik_sol[0][1])
        # elbow configurations via shoulder - elbow - wrist triangle
        r_B_SW_n = (r_B_OW_cart - self.calc_transform(0, 2)[0:3, 3]).norm()
        l1_vec = self.delta_links[2]
        l2_vec = self.delta_links[3] + self.delta_links[4]
        l1 = np.linalg.norm(l1_vec)
        l2 = np.linalg.norm(l2_vec)
        # todo -> check where this drops from...
        black_magic = 0.0007092221775568675
        gamma = _sp.acos((l1 ** 2 + l2 ** 2 - r_B_SW_n ** 2) / (2 * l1 * l2))
        q2_neg = gamma + _sp.tan(l2_vec[0] / l2_vec[2]) + 2 * black_magic - _sp.pi
        q2_pos = _sp.pi - gamma + _sp.sin(l2_vec[0] / np.linalg.norm(l2_vec)) - black_magic
        self.pos_ik_sol[2] = [
            _sp.simplify(q2_neg.subs({self.q_sym[0]: self.pos_ik_sol[0][0]})),
            _sp.simplify(q2_pos.subs({self.q_sym[0]: self.pos_ik_sol[0][0]})),
            _sp.simplify(q2_neg.subs({self.q_sym[0]: self.pos_ik_sol[0][1]})),
            _sp.simplify(q2_pos.subs({self.q_sym[0]: self.pos_ik_sol[0][1]}))
        ]
        if verbose > 0:
            self._print_helper("Obtained solution for q2 (q0) 'elbow up':", self.pos_ik_sol[2][0])
            self._print_helper("Obtained solution for q2 (q0) 'elbow down':", self.pos_ik_sol[2][1])
            self._print_helper("Obtained solution for q2 (q0 + pi) 'elbow up':", self.pos_ik_sol[2][2])
            self._print_helper("Obtained solution for q2 (q0 + pi) 'elbow down':", self.pos_ik_sol[2][3])
        # calculate q1 from z component
        f = _sp.Eq(r_B_OW_cart[2], self.calc_transform(0, 5)[2, 3])
        q1 = _sp.simplify(_sp.solve(f, self.q_sym[1]))
        self.pos_ik_sol[1] = []
        for x in self.pos_ik_sol[2]:
            self.pos_ik_sol[1] += [q1[0].subs(self.q_sym[2], x),
                                   q1[1].subs(self.q_sym[2], x)]
        for k in self.pos_ik_sol[1]:
            # k = _sp.simplify(k)
            if verbose > 0:
                self._print_helper("Obtained solution for q1:", k)
        self.q0_ik = [_sp.lambdify([self.ee_translation, self.ee_rotation], x) for x in self.pos_ik_sol[0]]
        self.q2_ik = [_sp.lambdify([self.ee_translation, self.ee_rotation], x) for x in self.pos_ik_sol[2]]
        self.q1_ik = [_sp.lambdify([self.ee_translation, self.ee_rotation], x) for x in self.pos_ik_sol[1]]

    def _rot_ik_helper(self, q, p, r_flat):
        rot_mat = Rotation.from_dcm(self.calc_rot_mat(q, p, r_flat.flatten()))
        return -1 * rot_mat.as_euler('ZYZ')

    def _calc_symbolic_rot_ik(self):
        r"""
        Calculate s from wrist to EE rotation matrix:
        """
        assert all([x is not None for x in self.pos_ik_sol.values()]), "calculate translation solution at first!"
        # get rotation dependent term (q3-5) -> C_3E
        C_B3 = self.calc_transform(0, 3)[0:3, 0:3]
        C_3E = C_B3.T * self.ee_rotation
        C_3E = _sp.simplify(C_3E)
        self.rot_ik_sol = C_3E

    def calc_rotation_ik(self) -> None:
        """
        Generate  python functions from symbolic rotational IK
        """
        if self.rot_ik_sol is None:
            self._calc_symbolic_rot_ik()
        self.calc_rot_mat = _sp.lambdify([self.q_sym[0:3], self.ee_translation, self.ee_rotation], self.rot_ik_sol,
                                         "numpy")
        self.rot_ik_fcn = self._rot_ik_helper

    def calc_iks(self) -> None:
        """
        Calculate symbolic inverse kinematics and save solutions into dedicated member attribute.

        8 solution via binary indexing:

        * [ 1, 0 ] -> shoulder front / back, i.e. 1 -> front: q_0 in [-pi/2, pi/2]
        * [ 1, 0 ] -> elbow up / low, i.e. 1 -> down: q_2 > 0
        * [ 1, 0 ] -> wrist front / back i.e. 1 -> up: q_4 > 0

        such that (0,1,1) would result in id = 3 , i.e. return function value of ```self._ik_fcn[3]```

        Note:
            current implementation is limited to setting shoulder / elbow as rotation gets adjusted
            from translation solution.

        """
        self.calc_transl_ik()
        self.calc_rotation_ik()

    def save_symbolics(self, data_path):
        for k, v in self._tfs.items():
            filename = os.path.join(data_path, "FK_{}.dat".format(k))
            pickle.dump(v, open(filename, "wb"))
        for q_i in range(len(self.pos_ik_sol)):
            for n in range(len(self.pos_ik_sol[q_i])):
                filename = os.path.join(data_path, "IK_q_{}{}.dat".format(q_i, n))
                pickle.dump(self.pos_ik_sol[q_i][n], open(filename, "wb"))
        filename = os.path.join(data_path, "IK_q_rot.dat")
        pickle.dump(self.calc_rot_mat, open(filename, "wb"))

    def _get_ik_mode(self, x: np.ndarray, r_flat: np.ndarray, mode: np.ndarray = np.array([1, 1, 0])) -> np.ndarray:
        pos_index = 2 ** 1 * mode[0] + 2 ** 0 * mode[1]
        q_out = np.zeros(6)
        q_out[0] = self.q0_ik[mode[0]](x, r_flat)
        q_out[2] = self.q2_ik[pos_index](x, r_flat)
        q_out[1] = self.q1_ik[pos_index * 2 + mode[2]](x, r_flat)
        q_out[3:] = self.rot_ik_fcn(q_out[:3], x, r_flat)
        return q_out

    def _process_p_rot_mat(self, p: np.ndarray, rot_mat_flat: np.ndarray) -> np.ndarray:
        q_all = np.zeros((8, 6))
        cnt = 0
        for s in range(2):
            for e in range(2):
                for w in range(2):
                    q_all[cnt, :] = self._get_ik_mode(p, rot_mat_flat, np.array((s, e, w)))
                    cnt += 1
        return q_all

    def pos_quat_to_q(self, x: np.ndarray, q: np.ndarray) -> np.ndarray:
        return self._process_p_rot_mat(x, Rotation.from_quat(q).as_dcm().flatten())

    def pose_to_qs(self, transform: np.ndarray) -> np.ndarray:
        return self._process_p_rot_mat(transform[0:3, 3],
                                       transform[0:3, 0:3].flatten())

    def load_ik_from_file(self, data_path):
        self.pos_ik_sol[0] = [None for _ in range(2)]
        self.pos_ik_sol[2] = [None for _ in range(4)]
        self.pos_ik_sol[1] = [None for _ in range(8)]
        # q0
        for n in range(2):
            filename = os.path.join(data_path, "IK_q_0{}.dat".format(n))
            tmp_fun = pickle.load(open(filename, "rb"))
            self.pos_ik_sol[0][n] = tmp_fun
            self.q0_ik[n] = _sp.lambdify([self.ee_translation, self.ee_rotation],
                                                   tmp_fun, "numpy")

            # 2
        for n in range(4):
            filename = os.path.join(data_path, "IK_q_2{}.dat".format(n))
            tmp_fun = pickle.load(open(filename, "rb"))
            self.pos_ik_sol[2][n] = tmp_fun
            self.q2_ik[n] = _sp.lambdify([self.ee_translation, self.ee_rotation],
                                                   tmp_fun, "numpy")

            # q1
        for n in range(8):
            filename = os.path.join(data_path, "IK_q_1{}.dat".format(n))
            tmp_fun = pickle.load(open(filename, "rb"))
            self.pos_ik_sol[1][n] = tmp_fun
            self.q1_ik[n] = _sp.lambdify([self.ee_translation, self.ee_rotation],
                                                   tmp_fun, "numpy")

        filename = os.path.join(data_path, "IK_q_rot.dat")
        self.rot_ik_sol = pickle.load(open(filename, "rb"))
        self.calc_rotation_ik()
