from pathlib import Path
from typing import List
import numpy as np
try:
    from mujoco_py import load_model_from_path, MjSim, mjSim, cymj, functions as _mj_func
except ModuleNotFoundError:
    print("Mujoco not found on this client. Functionality limited")
    MjSim, mjSim, cymj = None, None, None


def load_model_helper(path: str, model_name: str):
    """
    Load model  if it exists

    Args:
        path(str):  path to xml file
        model_name(str):    name of xml file

    Returns:
        cymj.PyMjModel: simulation model to be used
    """
    path = Path(path)
    assert path.exists(), "path {} does not exist !.".format(path)
    path = path / model_name
    assert path.exists(), "Model {} does not exist in {} !.".format(model_name, path)
    return load_model_from_path(path.expanduser())


def get_mj_handles(path: str, model_name: str) -> mjSim:
    """

    Args:
        path(str):  path to xml file
        model_name(str):    name of xml file

    Returns:
       mjSim: simulation handle or None for missing mujoco setups
    """
    if MjSim is not None:
        return MjSim(load_model_helper(path, model_name))



class ContactMsr(object):

    def __init__(self):
        self.pos = np.zeros(3)
        self.quat = np.array([1.0, 0.0, 0.0, 0.0])
        self.f = np.zeros(3)
        self.tau = np.zeros(3)
        self.d = 0.0
        self.collision_with = None

    def __str__(self):
        return "Contact at p:{p} q:{q} with {o}.\nWith a 6D-force: {f}{t} and a displacement of {d}".format(
            p=self.pos, q=self.quat, o=self.collision_with, f=self.f, t=self.tau, d=self.d
        )


def get_contact(sim_handle:MjSim, obj_name: str, debug: bool = False) -> List[ContactMsr]:
    r"""
    get contact measurement of body named 'obj_name'

    Args:
        sim_handle (mujoco_py.MjSim):  simulation handle
        obj_name(str):  name string of the selected object
        debug(bool): debug flag to check if object name is known to simulation
    Returns:
        ContactMsr or None: contact data between object

    Todo:
        compare with alternatives

        .. python:

            geom2_body = sim_handle.model.geom_bodyid[sim_handle.data.contact[i].geom2]
            print(' Contact force on geom2 body', sim_handle.data.cfrc_ext[geom2_body])
            print('norm', np.sqrt(np.sum(np.square(sim_handle.data.cfrc_ext[geom2_body]))))
    """
    if debug:
        assert obj_name in sim_handle.model.body_names, \
            "provide a feasible object name, {}. You can choose from{}".format(obj_name, sim_handle.model.body_names)
    all_contacts = []
    cur_id = sim_handle.model.geom_name2id(obj_name)
    for i in range(sim_handle.data.ncon):
        con = sim_handle.data.contact[i]
        if cur_id == con.geom1:
            found_contact = ContactMsr()
            found_contact.collision_with = sim_handle.model.geom_id2name(con.geom2)
        elif cur_id == con.geom2:
            found_contact = ContactMsr()
            found_contact.collision_with = sim_handle.model.geom_id2name(con.geom1)
        else:
            continue
        wrench_vector = np.zeros(6, dtype=np.float64)
        _mj_func.mj_contactForce(sim_handle.model, sim_handle.data, i, wrench_vector)
        found_contact.d = con.dist
        found_contact.f = wrench_vector[:3]
        found_contact.tau = wrench_vector[3:]
        found_contact.pos[0:3] = con.pos[0:3]
        all_contacts.append(found_contact)
    return all_contacts
