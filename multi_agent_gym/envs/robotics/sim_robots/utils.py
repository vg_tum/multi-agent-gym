#!/usr/bin/env python
import scipy.spatial.transform as _sc_tf
import numpy as np
import quaternion


def is_ipython():
    """
    Short helper to check if current execution is called from an ipython instance

    Returns:
        bool: True if called via ipython
    """
    try:
        shell = get_ipython().__class__.__name__
        if shell == 'ZMQInteractiveShell':
            return True  # Jupyter notebook or qtconsole
        elif shell == 'TerminalInteractiveShell':
            return False  # Terminal running IPython
        else:
            return False  # Other type (?)
    except NameError:
        return False  # Probably standard Python interpreter


def calc_rot_error(R_from: np.ndarray, R_to: np.ndarray) -> float:
    """
    Calculate the angular error between two matrices

    Args:
        R_from (np.ndarray): rotation matrix
        R_to (np.ndarray):  rotation matrix

    Returns:
        float: angular error between
    """
    E = R_from.T * R_to[0:3, 0:3]
    axis = np.array([[E[1, 2] - E[2, 1]],
                     [E[2, 0] - E[0, 2]],
                     [E[0, 1] - E[1, 0]]])
    return np.arctan2(np.hypot(axis[0], np.hypot(axis[1], axis[2])),
                       E[0, 0] + E[1, 1] + E[2, 2])


def inverse_homog(T):
    T_ = np.eye(4)
    T_[:3, :3] = T[:3, :3].T
    T_[:3, 3] = -T[:3, :3].T * T[:3, 3]
    return T


def calc_rotation_error(x_des: _sc_tf.Rotation, q_cur: quaternion.quaternion):
    q_des = quaternion.quaternion(*x_des.as_quat())
    if np.dot(quaternion.as_float_array(q_des), quaternion.as_float_array(q_cur)):
        q_des = q_des.conjugate()
    quat_err = q_des * q_cur.inverse()
    return _sc_tf.Rotation.from_quat(quaternion.as_float_array(quat_err)).as_rotvec()
