#!/usr/bin/env python
import pybullet as _p
import pybullet_utils.bullet_client as _bc
import scipy.spatial.transform as _sc_tf
import numpy as np
import quaternion
from .symbolic_models import SymbolicKinematicRobot
from .sim_robot import SimRobot


class JointInfoIndex:
    """
    Refer to `the getJointInfo documentation
     <https://github.com/bulletphysics/bullet3/blob/master/docs/pybullet_quickstart_guide/PyBulletQuickstartGuide.md.html#getjointinfo>`_
    for further information.
    """   
    JOINT_NAME = 1
    JOINT_TYPE = 2
    DAMPING = 6
    FRICTION = 7
    Q_MIN = 8
    Q_MAX = 9
    TAU_MAX = 10
    V_MAX = 11
    LINK_NAME = 12
    AXIS = 13
    DISPLACEMENT = 14


class LinkInfoIndex:
    LINK_TRN = 0
    LINK_ROT = 1
    COM_TRN = 2
    COM_ROT = 3
    FRAME_POS = 4
    FRAME_ROT = 5
    LINK_VEL_TRN = 6
    LINK_VEL_ROT = 7


class PyBulRobot(SimRobot):

    def __init__(self, bullet_client: _bc.BulletClient, urdf_root_path: str, base_pos=np.zeros(3),
                 base_rot=np.array([0.0, 0.0, 0.0, 1.0]), dt=1.0 / 240.0):
        """
        Args:
            bullet_client:
            urdf_root_path:
        """
        self.dt = dt
        self._p = bullet_client
        self._base_pos = base_pos
        self._base_quat = base_rot
        self.robot_id = self._p.loadURDF(urdf_root_path, base_pos, base_rot, useFixedBase=True)
        self._n_joints = self._p.getNumJoints(self.robot_id)
        self._ctrl_joints = np.zeros(self._n_joints, dtype=np.bool)
        self._ee_state = None
        self.dof = 0
        joint_min = []
        joint_max = []
        joint_damp = []
        joint_v_max = []
        joint_tau_max = []
        joint_axes = []
        link_delta = []
        if len(self.link_names) == 0:
            for q in range(self._n_joints):
                q_info = self._p.getJointInfo(self.robot_id, q)
                self.joint_names.append(q_info[JointInfoIndex.JOINT_NAME])
                self.link_names.append(q_info[JointInfoIndex.LINK_NAME])
                self._ctrl_joints[q] = q_info[JointInfoIndex.JOINT_TYPE] != _p.JOINT_FIXED
                if self._ctrl_joints[q]:
                    joint_damp.append(q_info[JointInfoIndex.DAMPING])
                    joint_min.append(q_info[JointInfoIndex.Q_MIN])
                    joint_max.append(q_info[JointInfoIndex.Q_MAX])
                    joint_v_max.append(q_info[JointInfoIndex.V_MAX])
                    joint_tau_max.append(q_info[JointInfoIndex.TAU_MAX])
                    joint_axes.append(q_info[JointInfoIndex.AXIS])
                    link_delta.append(q_info[JointInfoIndex.DISPLACEMENT])
                    self.dof += 1
        self.joint_damping = np.array(joint_damp)
        self.joint_limits = np.hstack((np.array([joint_min]).T, np.array([joint_max]).T))
        self.joint_velocity_limits = np.array(joint_v_max)
        self.joint_force_limits = np.array(joint_tau_max)
        self.joint_axes = np.array(joint_axes)
        self.joint_indices = [x for x in filter(lambda x: self._ctrl_joints[x], range(len(self.joint_names)))]
        self.link_delta = np.array(link_delta)
        self._q = np.empty(self.dof)
        self._q_dot_prev = np.zeros(self.dof)
        self._q_dot = np.zeros(self.dof)
        self._tau_q = np.zeros(self.dof)
        self.update()

    def update(self):
        self._q_dot_prev = np.copy(self._q_dot)
        self.update_joint_state()
        self.update_link_state()

    def __del__(self):
        self._p = 0

    @property
    def base_to_world(self) -> np.ndarray:
        T = np.eye(4)
        T[0:3, 0:3] = self.x_rot_mat_base.T
        T[0:3, 3] = - self.x_p_base
        return T

    @property
    def x_p_base(self) -> np.ndarray:
        return self._base_pos

    @property
    def x_rot_mat_base(self) -> np.ndarray:
        return _p.getMatrixFromQuaternion(self._base_quat)

    @property
    def x_q_base(self) -> quaternion.quaternion:
        return np.quaternion(self._base_quat[3], *self._base_quat[0:3])

    def get_link_state(self, link_id=6):
        return self._p.getLinkState(self.robot_id, link_id,
                                    computeLinkVelocity=1,
                                    computeForwardKinematics=1
                                    )

    def get_joint_states(self):
        return self._p.getJointStates(self.robot_id, range(self._n_joints))

    def update_link_state(self):
        self._ee_state = self.get_link_state()

    def update_joint_state(self):
        joint_states = self.get_joint_states()
        q = 0
        for q_ind, jnt in enumerate(joint_states):
            if self._ctrl_joints[q_ind]:
                self._q[q] = jnt[0]
                self._q_dot[q] = jnt[1]
                self._tau_q[q] = jnt[3]
                q += 1

    @property
    def x_p_ee(self) -> np.ndarray:
        return np.array(self._ee_state[LinkInfoIndex.LINK_TRN])

    @property
    def x_q_ee(self) -> quaternion.quaternion:
        return np.quaternion(self._ee_state[LinkInfoIndex.LINK_ROT][3],
                             *self._ee_state[LinkInfoIndex.LINK_ROT][0:3])

    @property
    def x_rot_mat_ee(self) -> np.ndarray:
        return np.reshape(np.array(_p.getMatrixFromQuaternion(self._ee_state[LinkInfoIndex.LINK_ROT])), (3, 3))

    @property
    def x_rotation(self):
        return _sc_tf.Rotation()

    @property
    def T_ee(self):
        T = np.eye(4)
        T[:3, :3] = self.x_rot_mat_ee
        T[:3, 3] = self.x_p_ee
        return T

    @property
    def x_dot_ee(self) -> np.ndarray:
        return np.hstack((np.array(self._ee_state[LinkInfoIndex.LINK_VEL_TRN]),
                          np.array(self._ee_state[LinkInfoIndex.LINK_VEL_ROT])))

    @property
    def q(self) -> np.ndarray:
        return self._q

    @property
    def q_dot(self) -> np.ndarray:
        return self._q_dot

    @property
    def q_ddot(self) -> np.ndarray:
        return (self._q_dot - self._q_dot_prev) / self.dt

    @property
    def tau_q(self) -> np.ndarray:
        return self._tau_q

    @property
    def zero_q_vec(self):
        return [0.0] * self.dof

    @property
    def q_tau_dyn(self) -> np.ndarray:
        return self._p.calculateInverseDynamics(self.robot_id, self.q.tolist(), self.q_dot.tolist(), self.zero_q_vec)

    @property
    def jacobian(self) -> np.ndarray:
        out = np.zeros((6, self.dof))
        J_t, J_r = self._p.calculateJacobian(self.robot_id, self._n_joints - 1, self._ee_state[LinkInfoIndex.LINK_TRN],
                                             self.q.tolist(), self.zero_q_vec, self.zero_q_vec)
        for i in range(3):
            out[i, :] = J_t[i]
            out[3 + i, :] = J_r[i]
        return out

    @property
    def inertia(self) -> np.ndarray:
        return np.array(self._p.calculateMassMatrix(self.robot_id, self.q.tolist()))

    def generate_kinematic_model(self, class_handle=SymbolicKinematicRobot, **kwargs):
        kin_model = class_handle(**kwargs)
        dx_total = np.array([x[LinkInfoIndex.FRAME_POS] for x in self._p.getLinkStates(self.robot_id,
                                                                                       range(len(self.link_names)))])
        delta_links = np.diff(dx_total, axis=0)[:self.dof]
        kin_model.initialize_kinematic(joint_limits=self.joint_limits, jnt_axes=self.joint_axes,
                                       delta_links=delta_links)
        return kin_model
