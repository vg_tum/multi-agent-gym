#!/usr/bin/env python
import re
import numpy as np
import quaternion

from .symbolic_models import SymbolicKinematicRobot
from .sim_robot import SimRobot
try:
    from mujoco_py import MjSim, functions as _mj_func
except ModuleNotFoundError:
    print("Mujoco not found on this client. Do not use this file!")
    MjSim = None


class MjRobot(SimRobot):

    def __init__(self, sim_handle: MjSim, robot_name: str = "racer5"):
        name_filter = re.compile('{}_joint*'.format(robot_name))
        link_name_filter = re.compile('{}_.*link.*'.format(robot_name))
        self._sim = sim_handle
        joint_filter = np.vectorize(lambda x: bool(name_filter.match(x)))
        self.joint_names = list(filter(lambda x: bool(name_filter.match(x)), self._sim.model.joint_names))
        self.link_names = list(filter(lambda x: bool(link_name_filter.match(x)), self._sim.model.body_names))
        self.joint_handles = np.where(joint_filter(self._sim.model.joint_names))[0]
        self.joint_limits = self._sim.model.jnt_range[np.where(joint_filter(self._sim.model.joint_names))[0]]
        self.joint_ids = np.array([self._sim.model.joint_name2id(x) for x in self.joint_names])
        self._ee_name = self.link_names[-1]
        self._base_frame = self.link_names[0]
        self._cart_stiff = np.eye(6) * 1000
        self._cart_damp = np.eye(6) * 0.7
        self._jnt_stiff = np.eye(len(self.joint_names)) * 2000
        self._jnt_damp = np.eye(len(self.joint_names)) * 0.7

    @property
    def base_to_world(self) -> np.ndarray:
        T = np.eye(4)
        T[:3, :3] = self.x_rot_mat_base
        T[:3, 3] = self.x_p_base
        return T

    @property
    def x_p_base(self) -> np.ndarray:
        """ return cartesian position of end-effector in base frame coordinates"""
        return self._sim.data.get_body_xpos(self._base_frame)

    @property
    def x_q_base(self) -> quaternion.quaternion:
        """ return cartesian rotation of end-effector in base frame coordinates represented as a quaternion"""
        return quaternion.quaternion(*self._sim.data.get_body_xquat(self._base_frame))

    @property
    def x_rot_mat_base(self) -> np.ndarray:
        """ return cartesian rotation of end-effector in base frame coordinates represented as a quaternion"""
        return self._sim.data.get_body_xmat(self._base_frame)

    @property
    def endeffector_name(self):
        return self._ee_name

    @property
    def dof(self) -> int:
        """ return degrees of freedom"""
        return len(self.joint_handles)

    @property
    def x_p_ee(self) -> np.ndarray:
        """ return cartesian position of end-effector in base frame coordinates"""
        return self._sim.data.get_body_xpos(self._ee_name)

    @property
    def x_q_ee(self) -> quaternion.quaternion:
        """ return cartesian rotation of end-effector in base frame coordinates represented as a quaternion"""
        return quaternion.quaternion(*self._sim.data.get_body_xquat(self._ee_name))

    @property
    def x_rot_mat_ee(self) -> np.ndarray:
        """ return cartesian rotation of end-effector in base frame coordinates represented as a quaternion"""
        return self._sim.data.get_body_xmat(self._ee_name)

    @property
    def T_ee(self):
        """ return homogeneous transformation from base to endeffector frame """
        out = np.eye(4)
        out[0:3, 3] = self._sim.data.get_body_xpos(self._ee_name)
        out[:3, :3] = self._sim.data.get_body_xmat(self._ee_name)
        return out

    @property
    def x_dot_ee(self) -> np.ndarray:
        """ return cartesian endeffector velocity """
        out = np.zeros(6)
        out[0:3] = self._sim.data.get_body_xvelp(self._ee_name)
        out[3:] = self._sim.data.get_body_xvelr(self._ee_name)
        return out

    @property
    def q(self) -> np.ndarray:
        """ return joint position vector"""
        return np.array([self._sim.data.qpos[q] for q in self.joint_ids])

    @property
    def q_dot(self) -> np.ndarray:
        """ return joint velocity vector """
        return np.array([self._sim.data.qvel[q] for q in self.joint_ids])

    @property
    def q_ddot(self) -> np.ndarray:
        """return joint acceleration vector"""
        return np.array([self._sim.data.qacc[q] for q in self.joint_ids])

    @property
    def q_tau_dyn(self) -> np.ndarray:
        r"""
        calculate the current force at every joint as a result of the robot dynamics:

        .. math:
            M \cdot \ddot{q} + C(q,\dot{q}) + g(q)

        frc combines coriolis and gravity as seen here:
        http://www.mujoco.org/book/computation.html#General

        Return:
            np.ndarray: robot torque dynamics for each joint as a vector
        """
        c_g = np.array([self._sim.data.qfrc_bias[q] for q in self.joint_ids])
        return np.dot(self.inertia[self.joint_ids, self.joint_ids[0]:self.joint_ids[self.dof - 1] + 1],
                      self.q_ddot) + c_g

    @property
    def jacobian(self) -> np.ndarray:
        """
        calculate Jacobian of robot.

        Return:
            np.ndarray: Jacobian matrix of dimension of dimension 6xn_dof
        """
        out = np.zeros((6, self._sim.model.nv))
        out[0:3, :] = self._sim.data.get_body_jacp(self._ee_name).reshape((3, self._sim.model.nv))
        out[3:, :] = self._sim.data.get_body_jacr(self._ee_name).reshape((3, self._sim.model.nv))
        return out

    @property
    def inertia(self) -> np.ndarray:
        """
        Calculate inertia matrix.

        Returns:
            np.ndarray:
        """
        inertia_mat = np.zeros(self._sim.model.nv * self._sim.model.nv)
        _mj_func.mj_fullM(self._sim.model, inertia_mat, self._sim.data.qM)
        return inertia_mat.reshape(self._sim.model.nv, self._sim.model.nv)

    def cart_imp_ctrl(self, x_des, x_q_des, wrench_add) -> None:
        r"""
        Apply cartesian torque command on robot.

        Args:
            x_des (np.ndarray):    desired position  (3x1)
            x_q_des (np.ndarray):  desired orientation as a quaternion (4x1)
            wrench_add (np.ndarray): desired external wrench command (6x1). Defaults to zero vector.
        """
        self._sim.data.ctrl[self.joint_handles] = self.calc_cart_imp_ctrl_torque(x_des, x_q_des, wrench_add)

    def jnt_imp_ctrl(self, q_des, q_dot_des=None, q_tau_ext=None):
        """ Calculate joint impedance control command for each joint and set control command directly"""
        self._sim.data.ctrl[self.joint_handles] = self.calc_joint_imp_command(q_des, q_dot_des, q_tau_ext)

    def generate_kinematic_model(self, class_handle=SymbolicKinematicRobot, **kwargs):
        kin_model = class_handle(self.dof, **kwargs)
        kin_model.initialize_kinematic(joint_limits=self._sim.model.jnt_range[self.joint_handles],
                                       jnt_axes=self._sim.model.jnt_axis[self.joint_handles],
                                       delta_links=np.array([self._sim.model.body_pos[self._sim.model.body_name2id(x)]
                                                             for x in self.link_names]))
        return kin_model
