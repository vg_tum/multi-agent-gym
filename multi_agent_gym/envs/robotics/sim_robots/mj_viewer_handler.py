#!/usr/bin/env python
import enum
import os
import numpy as np
import pathlib
import matplotlib.pyplot as plt
import matplotlib.image as mp_img
from IPython.display import Image, display
try:
    from mujoco_py import MjSim, MjViewer,functions as _mj_func
except ModuleNotFoundError:
    print("Mujoco not found on this client. Do not use this file!")
    MjSim, MjViewer = None, None


class RenderMode(enum.Enum):
    default = 0
    jupyter = 1
    file = 2
    disabled = -1


class ViewerHandle(object):
    """
    Mujoco Viewer handler that allows to wrap visualization quickly to the specific needs of current project.
    """

    def __init__(self, sim_handle: MjSim,
                 tmp_file_dir=os.path.join(pathlib.Path().absolute(), "img"),
                 mode: RenderMode = RenderMode.default):
        self._mode = mode
        if mode != RenderMode.disabled:
            self._viewer = MjViewer(sim_handle)
        if not os.path.exists(tmp_file_dir):
            os.mkdir(tmp_file_dir)
        self._tmp_file_dir = tmp_file_dir
        self._img_cnt = 0

    def render(self):
        if self._mode == RenderMode.disabled:
            return
        self._viewer.render()
        if self._mode == RenderMode.jupyter:
            return self.jupyter_render(),
        elif self._mode == RenderMode.file:
            return self.file_render()
        del self._viewer._markers[:]

    def file_render(self, filename="render.png", incremental=False) -> str:
        import imageio
        if incremental:
            filename = "{}_{}.png".format(filename, self._img_cnt)
            self._img_cnt += 1
        imageio.imwrite(os.path.join(self._tmp_file_dir, filename), self._viewer._read_pixels_as_in_window())
        return filename

    def jupyter_render(self, filename="render.png", incremental: bool = False) -> Image:
        return Image(filename=self.file_render(filename, incremental))

    def image_plotter(self, filename="render.png", incremental: bool = False) -> None:
        plt.imshow(mp_img.imread(self.file_render(filename, incremental)))
        plt.show()

    def add_marker(self, pos: np.ndarray, size: np.ndarray, color: np.ndarray = np.ones(4) * 0.5, label: str = ""):
        self._viewer.add_marker(pos=pos, size=size, rgba=color, label=label)

    def add_text(self, title: str, value: str):
        from mujoco_py.generated import const
        self._viewer.add_overlay(const.GRID_BOTTOMLEFT, title, value)

