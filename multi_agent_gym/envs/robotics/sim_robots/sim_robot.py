from abc import ABC, abstractmethod
import scipy.spatial.transform as _sc_tf
import numpy as np
import quaternion
from .symbolic_models import SymbolicKinematicRobot
from .utils import calc_rotation_error


class SimRobot(ABC):
    """
    Meta class for a simulated robot

    """
    joint_names = []
    link_names = []
    _cart_stiff = np.eye(6) * 1000
    _cart_damp = np.eye(6) * 0.7
    _jnt_stiff = np.eye(6) * 2000  # to be overwritten in individual init functions
    _jnt_damp = np.eye(6) * 0.7

    @property
    def T_base_world(self) -> np.ndarray:
        T = np.eye(4)
        T[:3, :3] = self.x_rot_mat_base
        T[:3, 3] = self.x_p_base
        return T

    @property
    @abstractmethod
    def x_p_base(self) -> np.ndarray:
        """
        Cartesian translation of robot endeffector in base frame coordinate-system"""

    @property
    @abstractmethod
    def x_q_base(self) -> quaternion.quaternion:
        """ return cartesian rotation of end-effector in base frame coordinates represented as a quaternion"""

    @property
    @abstractmethod
    def x_rot_mat_base(self) -> np.ndarray:
        """ return cartesian rotation of end-effector in base frame coordinates represented as a quaternion"""

    @property
    @abstractmethod
    def x_p_ee(self) -> np.ndarray:
        """ return cartesian position of end-effector in world frame coordinate-system"""

    @property
    @abstractmethod
    def x_q_ee(self) -> quaternion.quaternion:
        """ return cartesian rotation of end-effector in world frame coordinates represented as a quaternion"""

    @property
    @abstractmethod
    def x_rot_mat_ee(self) -> np.ndarray:
        """ return cartesian rotation of end-effector in world frame coordinates represented as a quaternion"""

    @property
    def T_base_ee(self):
        """ return homogeneous transformation from base to endeffector frame """
        out = np.eye(4)
        out[0:3, 3] = self._sim.data.get_body_xpos(self._ee_name)
        out[:3, :3] = self._sim.data.get_body_xmat(self._ee_name)
        return out

    @property
    @abstractmethod
    def x_dot_ee(self) -> np.ndarray:
        """ return Cartesian velocity vector in SE(3)"""

    @property
    @abstractmethod
    def q(self) -> np.ndarray:
        """ return joint position vector"""

    @property
    @abstractmethod
    def q_dot(self) -> np.ndarray:
        """ return joint velocity vector """

    @property
    @abstractmethod
    def q_ddot(self) -> np.ndarray:
        """return joint acceleration vector"""

    @property
    @abstractmethod
    def q_tau_dyn(self) -> np.ndarray:
        r"""
        calculate the current force at every joint as a result of the robot dynamics:

        .. math::
            M \cdot \ddot{q} + C(q,\dot{q}) + g(q)


        Return:
            np.ndarray: robot torque dynamics for each joint as a vector
        """

    @property
    @abstractmethod
    def jacobian(self) -> np.ndarray:
        """
        calculate Jacobian of robot.

        Return:
            np.ndarray: Jacobian matrix of dimension of dimension 6xn_dof
        """

    @property
    @abstractmethod
    def inertia(self) -> np.ndarray:
        """
        Calculate inertia matrix.

        Returns:
            np.ndarray:
        """

    @abstractmethod
    def generate_kinematic_model(self, class_handle=SymbolicKinematicRobot, **kwargs) -> SymbolicKinematicRobot:
        """
        Generate symbolic kinematic robot from current simulation interface

        Args:
            class_handle(SymbolicKinematicRobot):  Symbolic Robot implementation to be iniitalized
            **kwargs: additional init arguments for symbolic model

        Returns:
            SymbolicKinematicRobot: Symbolic Kinematic Instance of currently simulated robot
        """

    def calc_cart_wrench_cmd(self, x_p_des: np.ndarray, x_rot_des: _sc_tf.Rotation,
                             x_dot_des: np.ndarray = np.zeros(6), wrench_add: np.ndarray = np.zeros(6)) -> np.ndarray:
        r"""
        Calculate cartesian wrench command for cartesian impedance controller

        .. math::
            \mathbf{F}_{\mathrm{cart}} =
                - \mathbf{K_s} \left(\mathbf{x}_\mathrm{cur} - \mathbf{x}_\mathrm{des}\right)
                - \mathbf{K_d} \left(\mathbf{J}\cdot \dot{q} \right)
                + \mathbf{f}_\mathrm{cart}

        Args:
            x_p_des (np.ndarray):    desired position  (3x1)
            x_rot_des (np.ndarray):  desired orientation as a scipy spatial rotation
            wrench_add (np.ndarray): desired external wrench command (6x1). Defaults to zero vector.

        Returns:
            np.ndarray: cartesian wrench
        """
        pos_error = np.zeros(6)
        vel_error = self.x_dot_ee - np.zeros(6)
        pos_error[0:3] = self.x_p_ee - x_p_des
        pos_error[3:] = calc_rotation_error(x_rot_des, self.x_q_ee)
        return wrench_add - np.dot(self._cart_stiff, pos_error) - np.dot(self._cart_damp, vel_error)

    def calc_cart_imp_ctrl_torque(self, x_des, x_q_des, wrench_add, j_inv=True) -> np.ndarray:
        r"""
        Calculate cartesian impedance control command for each joint and set control command

        .. math::
           \tau_{\mathrm{cmd}} = \mathbf{J}^{-1} \cdot \mathbf{F}_{\mathrm{cart}}  - \tau_{\mathrm{dyn}}

        Note:
            if `j_inv` is set to False, the transposed jacobian is taken instead
        Args:
            x_des (np.ndarray):    desired position  (3x1)
            x_q_des (np.ndarray):  desired orientation as a quaternion (4x1)
            wrench_add (np.ndarray): desired external wrench command (6x1). Defaults to zero vector.
            j_inv(bool): If true, jacobian inverse is used, otherwise jacobian transposed. Defaults to True
        Returns:
            np.ndarray: joint torque vector to be applied on robot
        """
        if j_inv:
            try:
                j_i = np.linalg.inv(self.jacobian)
            except np.linalg.LinAlgError:
                j_i = np.linalg.pinv(self.jacobian)
        else:
            j_i = self.jacobian.transpose()
        return np.dot(j_i, self.calc_cart_wrench_cmd(x_des, x_q_des, wrench_add)) - self.q_tau_dyn

    def calc_joint_imp_command(self, q_des, q_dot_des=None, q_tau_ext=None) -> np.ndarray:
        r""" Calculate joint impedance control command torque vector for each joint.

        .. math::
            \mathbf{\tau}_{\mathrm{q}} =
                - \mathbf{K_s} \left(\mathbf{q}_\mathrm{cur} - \mathbf{q}_\mathrm{des}\right)
                - \mathbf{K_d} \left(\dot{\mathbf{q}}_\mathrm{cur} - \dot{\mathbf{q}}_\mathrm{des}\right)
                + \mathbf{\tau}_\mathrm{q,ext} + \mathbf{\tau}_\mathrm{q,dyn}
                

        Args:
             q_des(np.ndarray): desired joint vector.
             q_dot_des(np.ndarray or None): optional desired joint velocity vector. Defaults to None
             q_tau_ext(np.ndarray or None): optional superimposed torque vector
        """
        if q_dot_des is None:
            q_dot_err = - self.q_dot
        else:
            q_dot_err = q_dot_des - self.q_dot
        if q_tau_ext is None:
            q_tau_ext = np.zeros(self.dof)
        return np.dot(self._jnt_stiff, (q_des - self.q)) + np.dot(self._jnt_damp, q_dot_err) \
               + q_tau_ext - self.q_tau_dyn
