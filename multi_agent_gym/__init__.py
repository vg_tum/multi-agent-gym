"""
Multi Agent Gym Module
------------------------

This section explains the base content of the multi-agent gym, while the :py:mod:`~envs` contain currently available
multi-agent domain from literature as well as newly introduced benchmark examples.

The content of this module is listed as

:py:mod:`~.core`
    outlines the actual core components of the multi-agent gym as well its analogy to gym ([OpenAI2016]_).
"""
from multi_agent_gym.core import *
from multi_agent_gym.commands import *

