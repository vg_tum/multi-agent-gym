## [0.5.7] - 2022-07-15

- corrected error in static coop-navigation/collection

## [0.5.6] - 2022-05-06

- corrected error in coop-navigation
- adjusted damping (0.9) and agent sizes


## [0.5.5] - 2022-05-03

- Added scripts to setup.py
- Added env to json generation to parse env-specs to sacred


## [0.5.4] - 2022-05-02

Added sparse rewards to registry. Eased registration / naming convention

## [0.5.3] - 2022-04-26

Added MLP-action spaces from Loewe et. al as practical action-space suffers 
from poor convergence properties.

Main changes:

- Now used batch_registry.
- Differentiate action-spaces / setting via dedicated suffices


## [0.5.2] - 2022-04-25

added new space types for observation


## [0.5.1] - 2022-04-21

corrected various issues in particles env

* ``dt`` not set correctly
* incorrect reward for coop-navigation
* set ``sparse_reward`` to False by default

### [0.5] - 2022-04-20

A last miserable trial

- reworked particles for utter base usage



### [0.4] - 2020-07-12

major environment upgrade

#### Added

- partivle environment wrapper 
- registered all non-communicative environments 
- removed previous particle world


### [0.3.2] - 2020-03-12

minor feature upgrades

#### Added

- dictionary based state spaces for multi agent settings
- timeout sensitive Timewrapper for multi agent environment that does not raise a done flag upon timeout but adjust a 
seperate flag instead

## [0.3] - 2020-02-12

Relational environments deprecated / incompatible

### Removed

* Relational environments due to incompatibility with relational engine module at the moment

### Added

* adversarial domains / reamining content from multi agent particles environment as stated by Loewe et al for fair comparisons

## [0.2] basic mujoco engine for dyadic / triadic robotic systems

Added mujoco robot models for joint reaching tasks.

## [0.1.1] - 2019-09-16

### Added

* State description for relational state ==> typed state to improve performance and reduce errors
* included relational state sampling (limits are not really incorporated so further checks shall be incorporated on top)
* Basic tests for relational domain

### Fixed

outsourced relational domain / modules to increase transparency which modules are actually needed and which are not.

### Open

* check how sampling is colliding with internal rules / conditions

## [0.1.0] - 2019-09-05 
## Initial log summary
With this being the first version report, this is held to a very superficial explanation.
Let's hope this log contains further notes next time.

### Added 
- basic robotics and particle environments
- assets for COMAU Racer 5 and simulation environments
- integration / compatibility with classic gym environment
- unit tests
- relational basic components (not finished)

### Fixed
#### Particle Environment
- broken physics (wall bouncing)
- reset problems, see issue #1, #4, #5 in terms of sampling legal goals and returning joint as well as individual observations
- goal sampling, see #2, #3, #5, #6 as various vases needed special treatment / were undetected in previous versions
- reward calculation fixes, such as wall collisions (#7, #9)
- reset problems (#5, #10)


