"""
Define all necessary information here
"""

PACKAGE_NAME = "multi_agent_gym"
PACKAGE_TITLE = "Multi Agent Simulation Environment (Extending OpenAi Gym)"
AUTHOR = "Volker Gabler"
REPO = 'https://gitlab.com/vgab/multi_agent_gym'
COPYRIGHT = "Copyright (C) 2019-2022 Volker Gabler"
LICENSE = "MIT License"
RELEASE = "0.5"
VERSION = "0.5.7"
STATUS = "development"
EMAIL = "v.gabler@tum.de"
MAINTAINER = "Volker Gabler"

